extends Node # Global variables of the game 

### Dev mode
var dev: bool = false # If true = OS recognition desactivated / dials with 2 players in local activated / reduce maker_question counter with help button activated / network is localhost
var version: float = 1.2 # Current version number 

### Buttons's management variables for Dials
var buttons_array: Array = [] # Array to access the .json (for the questions)
var buttons_dict: Dictionary = {} # Buttons's dictionary
var button_number_p: int = 0
const button_p: String = "button_%d"
var num = button_p % button_number_p # Variable controling buttons
var loading_button_number_p: int = 0
var loading_num = button_p % loading_button_number_p # Variable controling buttons (loading stage)
var answers_possible # Variable of the possible answers
var state_number: int = 0 # Variable used to id the question asked 
const questions_buttons: String = "button_%d"
var questions_states # = questions_buttons % state_number
var add_display_question: bool = false # Variable to trigger the display of a question
var add_display_answer: bool = false # Variable to trigger the display of an answer
var player_entry # Answers of the player
var activ_player: int = 0 # Temp variable to id the current player # will be deleted when networking is on
var p_buttons: Array # Array of buttons unlock by the player
var good_answers: bool = false # Variable for good answers
var buttons_loading: bool = false # Unlock buttons's loading activation variable 
var gen_array: int = 0 # Index of the buttons's array
var unlock_buttons # Variable for unlocking specific buttons
var hello: bool = false # True after click on the button_0 (hello) in dials

### Variables for clan and level management
var level: int = 1 # Current level of the player (4 level in total)
var p_clan: String = "Thinker" # Variable for player's faction. "Voider", "Builder", "Thinker".
var focus: int = 0 # Variable to grab focus on the player line_edit (ingame), return to 0 when the player quite a dial

### Network and server variables
var server: int = 0 # Server presence variable : 1 is for server, 0 or other numbers for client
var offline_mode: bool = true # Bool to made the game play offline
var players_searching: Array = [] # Array of players searching for dials
var players_ingame: Array = [] # Array of dials currently running (for management of disconnecting players)
var ingame: bool = false
var login_done: bool = false # Variable for disabling the save button from the connexion screen (conflicting with valid_player_entry on player_text)
var online_players: int = 0 # Var for the total number of players in currently in game
var client_connected: int = 0 # Var to check if clients are connected to the server

### Player's variables
var player_name: String
var player_pass: String
var time_dict = OS.get_time()
var current_time = [time_dict.hour,time_dict.minute,time_dict.second]
var player_sex: String
var player_OS
var player_planet
var player_color
var player_description
var logs_unlock: Array = [] # Logs unlock by the player
var current_scene # For sound management
var locale: String = "en" # Language management variable
var factions: Array # Array for unlocked factions (dials)
var avatar_links: Array = [] # Array of player's avatar parts, order is accessorie/body/eyes/hair/mouth
var delete: bool = false # If true admin will be deleted at the end of the game session

### Dials varibles
var player_question: String # Question's text loads when a button is hovered, then sent to the display
var question_ask: int = 0 # State 0 = no question, 1 = question asked, waiting for player answer, return to 0 when the player quite a dial
var player_number: int # "1" for the first player to play (asking questions), "2" for the second player (answering questions), player answering is the one using button_0 first
var other_id: int # Id of the other player in a dial
var other_name: String # Name of the other player in a dial
var other_clan # Clan of the other player in a dial
var other_level: int # Level of the other player in a dial
var other_color # Color of the other player in a dial
var game_ready:bool = false # Variable for players to know that they are ingame
var player_asking: int = 0 # Variable defining wich player is questionning, 0 is for no question, 1 is for player_1, 2 is for player_2, return to 0 when the player leave a dial
var other_terminated: bool = false
var count_ended: bool = false
var ingame_number: int = 0 # Position of the player's dial in the players_ingame array
var bot_button_dict: Dictionary # Button dictionary for the bot
var wait_player: bool = false # Var to check if the player is waiting for a dial (in loading)

### Logs variables
const log_name = "log_%d"
var log_selected: int = 0
var log_num = log_name % log_selected
var button_logs: int # Log number unlock by a button
var dials_logs # Variable for saving dials
var log_state: int # 1 for history, 2 for dials, 3 for makers

### Charts variables
var charts # Dictionnary {subject : [player_group : [ans], [ans, text], etc]}, also used as bool

### Maker Section
var maker_section: bool = false
var player_group: String # It's a string because we need to use it as a key in a dictionary
var player_id_in_group: Array # Array of players of one group's ids
var maker_entry
var current_subject: Array = ["null","null","null"] # Array identifying subject [Q number string, question string, type string]
var current_topic_time

### Sound variables
var logs_sound: bool = false # If true trigger a log display sound
var sound_buttons: int = 0 # State variable for buttons sound management : 0 = no sound, 
