extends VBoxContainer

const log_instance = preload("res://scenes/logs/logs_instance.tscn")
const dial_log = preload("res://scenes/logs/dials_log.tscn")
const maker_log = preload("res://scenes/logs/maker_logs.tscn")
var logs
var numlog
onready var dials_display = get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/dials_display_scrollcontainer/dials_display_margincontainer/logs_dials_display")
onready var makers_display = get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/makers_display_scrollcontainer/makers_display_margincontainer/logs_makers_display")

func _ready() -> void:
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/dials_display_scrollcontainer").visible = true
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/makers_display_scrollcontainer").visible = false


func _on_history_logs_pressed() -> void:
	var log_unlock_sorted: Array = []

	var numlog: int = global.logs_unlock.size()
	global.log_state = 1
	dials_display.hide()
	makers_display.hide()
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/history_logs").set_disabled(true)
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/dials_logs").set_disabled(false)
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/maker_logs").set_disabled(false)

	if global.logs_unlock.size() != 0 :
		var y = 1
		while y <= 20:
			if y in global.logs_unlock :
				log_unlock_sorted.append(y)
			else:
				pass
			y += 1

		numlog = 0
		for i in log_unlock_sorted :
			global.log_selected = log_unlock_sorted[numlog]
			logs = log_instance.instance()
			add_child(logs)
			numlog += 1


func _on_dials_logs_pressed() -> void:
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/dials_display_scrollcontainer").visible = true
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/makers_display_scrollcontainer").visible = false

	var player_dialogs = File.new()
	if player_dialogs.file_exists("user://data/dialogs_"+global.player_name+".dat"):
		### Load the file line by line and process into the dictionary to check datas
		player_dialogs.open("user://data/dialogs_"+global.player_name+".dat", File.READ)
		var current_line = parse_json(player_dialogs.get_as_text())
		player_dialogs.close()

		var counter_dials = current_line.keys().size()

		global.log_state = 2
		get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/dials_logs").set_disabled(true)
		get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/history_logs").set_disabled(false)
		get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/maker_logs").set_disabled(false)

		while counter_dials > 0 : # We instance one dials_log scene for each dials
			var dials = dial_log.instance()
			add_child(dials)
			dials.get_node("dials_choices")._get_text(current_line[str(counter_dials)])
			counter_dials -= 1


func _on_maker_logs_pressed() -> void:
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/dials_display_scrollcontainer").visible = false
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/makers_display_scrollcontainer").visible = true

	var player_dialogs = File.new()
	### Load the file line by line and process into the dictionary to check datas
	player_dialogs.open("user://data/dialogs_"+global.player_name+".dat", File.READ)
	var current_line = parse_json(player_dialogs.get_as_text())
	player_dialogs.close()

	var counter_makers = current_line.keys().size() # Total number of dialogs
	global.log_state = 3
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/dials_logs").set_disabled(false)
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/history_logs").set_disabled(false)
	get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/VBoxContainer/maker_logs").set_disabled(true)

	while counter_makers > 0 : # We instance one maker_log scene for each maker log
		var makers = maker_log.instance()
		add_child(makers)
		makers.get_node("maker_logs_display")._get_text(current_line[str(counter_makers)])
		counter_makers -= 1
