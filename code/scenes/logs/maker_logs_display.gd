extends Button

onready var dials_display = get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/makers_display_scrollcontainer/makers_display_margincontainer/logs_makers_display")
var entire_text
signal makerlogs_choice

func _ready() -> void:
	set_process(true)


func _process(delta) -> void:
	if global.log_state == 1 or global.log_state == 2:
		get_node("..").queue_free()


func _get_text(mak_log_text: String) -> void:
	entire_text = mak_log_text

	var troncated_text = entire_text.substr(0,(entire_text.find("\n"+"\n"))) # We extract the title of the dial from the saved file
	var array_troncated_text = troncated_text.split(' ', false) # We split the troncated text to look for makerz 

	if array_troncated_text[0].matchn("L0G_MAKERZ:"):
		get_node(".").set_text(troncated_text)
		get_node(".").set_clip_text(true)

		connect("pressed", self, "_on_pressed")
		connect("makerlogs_choice", get_node("../buttons_makerlogs"), "on_pressed")
	else:
		get_node("..").queue_free()


func _on_pressed() -> void:
	emit_signal("makerlogs_choice") # Sound

	# Show display if necessary
	if dials_display.is_visible_in_tree() == false :
		dials_display.show()

	# Add actual text
	dials_display.clear() # Empty the label
	dials_display.add_text(entire_text) # We add the dial text into the richtextlabel
