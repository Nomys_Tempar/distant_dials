extends RichTextLabel

var logs_file
var logs_dict: Dictionary

func _ready():
	set_process(true)

	### Loading of the json for the logs
	var logs_file = File.new()
	logs_file.open("res://json/logs_"+global.locale+".json", logs_file.READ)
	var logs_text = logs_file.get_as_text()
	logs_dict = parse_json(logs_text)
	logs_file.close()

	global.log_num = global.log_name % global.log_selected
	add_text(logs_dict[global.log_num].log_text)


func _process(delta):
	if global.log_state == 2 or global.log_state == 3:
		get_node("..").queue_free()
