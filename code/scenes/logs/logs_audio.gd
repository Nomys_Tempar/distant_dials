extends AudioStreamPlayer

var sound_dict: Dictionary

func _ready() -> void:
	if get_tree().is_network_server(): pass # The server bypass sounds
	else:
		var sound_dict_to_load = File.new()
		sound_dict_to_load.open("res://json/sfx.json", sound_dict_to_load.READ)
		var sound_dict_load = sound_dict_to_load.get_as_text()
		sound_dict = parse_json(sound_dict_load)
		sound_dict_to_load.close()
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("log_bus"), 0)


func _play_effect(sound_type) -> void: # 0 is for log_unlock, 1 for level and 2 for level_makerz
	if global.p_clan == "Voider" :
		get_node(".").stream = load(sound_dict.voiders_level[sound_type])
		get_node(".").play()
	elif global.p_clan == "Builder" :
		get_node(".").stream = load(sound_dict.builders_level[sound_type])
		get_node(".").play()
	elif global.p_clan == "Thinker" :
		get_node(".").stream = load(sound_dict.thinkers_level[sound_type])
		get_node(".").play()
