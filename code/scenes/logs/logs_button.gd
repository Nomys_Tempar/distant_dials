extends Button

func _ready() -> void:
	set_process(true)
	connect("pressed", self, "_on_pressed")


func _process(delta) -> void:
	if is_hovered() :
		get_node(".").owner.get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + tr("Logs about history of mankind and your own journey in dials.") + "[/center]\n")


func _on_pressed() -> void:
	get_node("/root/connection_back/main_menu")._load_logs()
