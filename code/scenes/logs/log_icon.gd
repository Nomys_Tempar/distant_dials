extends Node

var accum: float = 3

func _ready() -> void:
	set_process(true)


func _setup_icon(icon) -> void:
	var sprite
	if icon == "log": # Unlocking a log
		sprite = load("res://ui/icons/new-log.png")
		get_node("log_icon").set_texture(sprite)
		get_node("log_level_sound")._play_effect(0)
	elif icon == "level": # Gaining level
		sprite = load("res://ui/icons/level-up.png")
		get_node("log_icon").set_texture(sprite)
		get_node("log_level_sound")._play_effect(1)
	else: # Becoming Makerz
		sprite = load("res://ui/icons/you-are-maker.png")
		get_node("log_icon").set_texture(sprite)
		get_node("log_level_sound")._play_effect(2)


func _process(delta) -> void:
	accum -= delta

	if accum < 0 :
		get_node(".").queue_free()
