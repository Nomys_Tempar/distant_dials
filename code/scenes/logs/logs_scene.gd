extends MarginContainer

func _ready() -> void:
	global.current_scene = "logs"
	get_node("logs_buttons")._scene_change()

	if global.level == 4 and global.offline_mode == false:
		get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/maker_logs").visible = true
	else:
		get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/maker_logs").visible = false

	_theme_selection()


func _theme_selection() -> void: ### Theme selection
	var theme_log
	if global.level == 4 :
		if global.p_clan == "Voider":
			theme_log = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_log = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_log = load("res://ui/thinkers_maker_theme.tres")
	else :
		if global.p_clan == "Voider":
			theme_log = load("res://ui/voiders_theme.tres") # Theme...
		if global.p_clan == "Builder":
			theme_log = load("res://ui/builders_theme.tres") # Theme...
		if global.p_clan == "Thinker":
			theme_log = load("res://ui/thinkers_theme.tres") # Theme...
	get_node("Panel").set_theme(theme_log)
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/dials_logs").set_theme(theme_log)
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/history_logs").set_theme(theme_log)
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/maker_logs").set_theme(theme_log)
	get_node("Panel/title").set_theme(theme_log)
