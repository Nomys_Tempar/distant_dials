extends Button

var player_name: String

func _ready() -> void:
	set_process_input(true)
	set_action_mode(1)
	set_toggle_mode(true)
	connect("pressed", self, "_on_pressed")


### Press 'Enter' to send an answser
func _input(event) -> void:
	if global.current_scene == "connection":
		if event.is_action_pressed("Enter") :
			emit_signal("pressed")
		elif is_hovered() && event.is_action_pressed("Mouse") :
			emit_signal("toggled")


func _on_pressed() -> void:
	global.login_done == true
	global.player_pass = get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").get_text()

	if global.offline_mode == true :
		_load_save("offline", get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").get_text(), get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").get_text())
	else:
		rpc_id(1, "_load_save", get_tree().get_network_unique_id(), get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").get_text(), get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").get_text()) # On pressed, we ask the server to load the savegame and we send to server the player id


remote func _load_save(id, player_n: String, player_pass: String) -> void: # Server is loading player's datas & saving the player in the online_player_data file
	var online

	var save_game = File.new()
	player_name = "user://save/savegame_"+player_n+".dat"

	if not save_game.file_exists(player_name):
		if global.offline_mode == true :
			_no_save_to_load()
		else:
			rpc_id(id, "_no_save_to_load")
		return # Error!  We don't have a save to load.

	### Load the file line by line and process into the dictionary to check datas
	save_game.open_encrypted_with_pass(player_name, File.READ, player_pass)
	if save_game.is_open() != true : # Here we check if the player's pass matches the one saved

		# Here we check if the player's pass matches the one saved
		if global.offline_mode == true :
			_wrong_pass()
		else:
			rpc_id(id, "_wrong_pass")
		return # Error! Player entered the wrong password
		save_game.close()
	else : # If the password is the same...
		var current_line = parse_json(save_game.get_as_text())
		### ...we send the remaining variables to the player.
		if global.offline_mode == true :
			_loading_player_datas(current_line.player_name, current_line.player_sex, current_line.player_environment, current_line.player_planet, current_line.player_color, current_line.player_description, current_line.player_buttons, current_line.player_logs, current_line.level, current_line.player_group, current_line.current_topic, current_line.factions, current_line.avatar_links)
		else:
			rpc_id(id, "_loading_player_datas", current_line.player_name, current_line.player_sex, current_line.player_environment, current_line.player_planet, current_line.player_color, current_line.player_description, current_line.player_buttons, current_line.player_logs, current_line.level, current_line.player_group, current_line.current_topic, current_line.factions, current_line.avatar_links)

		### And we save the player in the online_player_data file
		var online_player = File.new()
		if online_player.file_exists("user://data/online_player.dat") == false:
			online_player.open("user://data/online_player.dat", File.WRITE)
			online = {
				str(id)+"_name" : current_line.player_name, 
				str(id)+"_group" : current_line.player_group
				}
		else :
			online_player.open("user://data/online_player.dat", File.READ_WRITE)
			var current_online = parse_json(online_player.get_as_text())
			current_online[str(id)+"_name"] = current_line.player_name
			current_online[str(id)+"_group"] = current_line.player_group
			online = current_online
		online_player.store_line(to_json(online))
		online_player.close()
		save_game.close()

		global.client_connected += 1


remote func _no_save_to_load() -> void:
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Error : no save to restore or invalid name"))


remote func _wrong_pass() -> void:
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Error : wrong password"))


### The player receive his datas...
remote func _loading_player_datas(_name: String, sex: String, environment, planet, player_color, player_description, buttons, logs, level: int, player_group, current_topic, factions, avatar_links) -> void: #dialogs
	### Opening option file to save name
	var options_file = File.new()
	options_file.open("user://data/options.dat", File.READ)
	var current_line = parse_json(options_file.get_as_text())
	if current_line.has("player_name") :
		current_line.erase("player_name")
		### Adding the new keys and values to the dict
		current_line["player_name"] = _name
	else:
		current_line["player_name"] = _name
	options_file.open("user://data/options.dat", File.WRITE)
	options_file.store_line(to_json(current_line))
	options_file.close()

	# Receiving variables
	global.player_name = _name
	global.player_sex = sex
	global.player_OS = environment
	global.player_planet = planet
	global.player_color = player_color
	global.player_description = player_description
	global.p_buttons = buttons
	global.logs_unlock = logs
	global.level = level
	global.player_group = player_group
	global.current_subject = current_topic
	global.factions = factions
	global.avatar_links = avatar_links

	if global.dev == true :
		_main_menu_launch()
	else:
		### OS recognition evolved
		if global.p_clan == "Voider":
			if global.player_OS == "Android" or global.player_OS == "Haiku" or global.player_OS == "HTML5" or global.player_OS == "X11":
				_main_menu_launch()
			else :
				if global.player_OS == "iOS" or global.player_OS == "OSX":
					global.p_clan = "Thinker"
				elif global.player_OS == "Windows" or global.player_OS == "UWP":
					global.p_clan = "Builder"
				_timed_loading()
		elif global.p_clan == "Builder":
			if global.player_OS == "Windows" or global.player_OS == "UWP":
				_main_menu_launch()
			else :
				if global.player_OS == "iOS" or global.player_OS == "OSX":
					global.p_clan = "Thinker"
				elif global.player_OS == "Android" or global.player_OS == "Haiku" or global.player_OS == "HTML5" or global.player_OS == "X11":
					global.p_clan = "Voider"
				_timed_loading()
		elif global.p_clan == "Thinker":
			if global.player_OS == "iOS" or global.player_OS == "OSX":
				_main_menu_launch()
			else :
				if global.player_OS == "Windows" or global.player_OS == "UWP":
					global.p_clan = "Builder"
				elif global.player_OS == "Android" or global.player_OS == "Haiku" or global.player_OS == "HTML5" or global.player_OS == "X11":
					global.p_clan = "Voider"
				_timed_loading()


func _timed_loading() -> void:
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("WARNING"))
	yield(get_tree().create_timer(1,2), "timeout")
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text(".")
	yield(get_tree().create_timer(1,2), "timeout")
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text(".")
	yield(get_tree().create_timer(1,2), "timeout")
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text(".")
	yield(get_tree().create_timer(1,2), "timeout")
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text(".")
	yield(get_tree().create_timer(1,2), "timeout")
	### OS recognition evolved
	_main_menu_launch()


func _main_menu_launch() -> void:
	### And we send the message to instance the main_menu scene
	get_node("/root/connection_back")._main_menu()
