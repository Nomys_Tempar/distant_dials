extends ScrollContainer

var in_dials: bool = true

func _ready() -> void:
	### Players are ready
	if global.offline_mode == false:
		if get_tree().is_network_server() : pass
		else :
			_theme_selection()
			if global.current_scene == "player_bot":
				_bot_ready()
			else :
				rpc_id(global.other_id, "_players_ready") # Send the ready signal to the other player
	else:
		_theme_selection()
		_bot_ready()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else: # Dials
		if global.level == 4 : # Maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_maker_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_maker_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_maker_theme.tres")
		else: # Non-maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_theme.tres")
		get_node(".").set_theme(theme_)


remote func _players_ready() -> void: # Other player receive the "ready" info
	global.game_ready = true


func _bot_ready() -> void: # Other player receive the "ready" info
	global.game_ready = true


func _exit_tree() -> void:
	in_dials = false
