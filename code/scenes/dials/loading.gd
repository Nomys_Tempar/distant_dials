extends Node

const bg_scene = preload("res://scenes/interfaces/bgControl.tscn")
const display = preload("res://scenes/dials/display.tscn")
const player = preload("res://scenes/dials/player1.tscn")
const bot = preload("res://scenes/dials/player_bot.tscn")
var duet: Array = [] # Array for the dialogue
var duet_count: int = 0 # Counter for player's matching
var groups_loaded
var no_players: bool = false
var player_pic: String # String with the path of the player's picture
var queue: int = 0
var temp_level # Var to control player level before and after a dial (for level 4 management)

func _ready() -> void:
	### Display loading
	var disp = display.instance()
	get_node("gameControl").add_child(disp)

	if global.offline_mode == false:
		if get_tree().is_network_server() :
			### launching player1 scene
			var load_player = player.instance()
			get_node("gameControl").add_child(load_player)
		else :
			_load_background()
			### Maker Section
			if global.maker_section == true:
				rpc_id(1, "_retrieve_player_maker", get_tree().get_network_unique_id(), global.player_name, global.player_group) # Sending player infos to the server
			### Dials
			else:
				temp_level = global.level
				rpc_id(1, "_retrieve_player_dials", get_tree().get_network_unique_id(), queue)
	else:
		_load_offline_dial()


func _load_background() -> void: # Background selection
	var bg = bg_scene.instance()
	add_child(bg) # We create the background scene
	move_child(bg, 0) # We move it to be at the top of the scene tree

	var bg_sprite

	if global.level == 4 :
		if global.p_clan == "Voider":
			bg_sprite = load("res://ui/bg/voiders-makerz-bg.jpg")
		if global.p_clan == "Builder":
			bg_sprite = load("res://ui/bg/builders-makerz-bg.jpg")
		if global.p_clan == "Thinker":
			bg_sprite = load("res://ui/bg/thinkers-makerz-bg.jpg")
	else :
		if global.p_clan == "Voider":
			bg_sprite = load("res://ui/bg/voiders-bg.jpg") 
		if global.p_clan == "Builder":
			bg_sprite = load("res://ui/bg/builders-bg.jpg") 
		if global.p_clan == "Thinker":
			bg_sprite = load("res://ui/bg/thinkers-bg.jpg") 
	get_node("bgControl/bg").set_texture(bg_sprite)
	get_node("bgControl")._window_resize()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else: # Dials
		if global.level == 4 : # Maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_maker_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_maker_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_maker_theme.tres")
		else: # Non-maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_theme.tres")

		get_node("gameControl/HBoxContainer/VBoxContainer/CenterContainer/avatar").set_texture(_load_png("user://data/"+global.player_name+"_avatar.png")) # We load the user avatar with the _load_png func and apply it to the TextureRect


func _load_png(file): # Loading user external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex


##### Dials Funcs

#### Offline
func _load_offline_dial():
		_load_background()
		temp_level = global.level
		_lauching_bot()


#### Online
### On the server, we retrieve player id and add it to the array of player searching to dial or joining a maker section
remote func _retrieve_player_dials(id, queue: int) -> void: # id = player_id / queue = check counter, 0 is for first check, 1 is for second
	if global.players_searching.size()%2 == 1:
		queue = 2

	global.players_searching.append(id)

	### We use the player entering event to trigger a check to clear empty arrays in the global.players_ingame array of the server
	if global.players_ingame.empty() == false:
		for a in global.players_ingame.size():
			if global.players_ingame[a].empty() == true:
				global.players_ingame.remove(a)
				a += 1
	### We use the player entering event to trigger a check to clear empty arrays in the global.players_ingame array of the server

	rpc_id(id, "_loading_first_check", global.players_searching.size()) 


remote func _loading_first_check(players_searching) -> void:
	if (players_searching%2) != 0: # Replica of _player_check function
		get_node("gameControl/display_scene/MarginContainer/display").add_text("\n"+str(players_searching)+tr(" players searching for a dial.")+"\n"+tr("No connection found.")+"\n"+tr("Please wait."))
		_no_players()
	else:
		get_node("gameControl/display_scene/MarginContainer/display").add_text("\n"+str(players_searching)+tr(" players searching for a dial.")+"\n"+tr("Connection found.")+"\n"+tr("Please wait.")+"\n")
		_search_av_player_dials(get_tree().get_network_unique_id())


func _no_players() -> void: # Not enough players to play, players wait
	var no_player_countdown: int = 5
	queue = 1
	_theme_selection()
	get_node("gameControl/display_scene/MarginContainer/counter").visible = false
	get_node("gameControl/HBoxContainer/VBoxContainer/return_button").visible = true
	get_node("gameControl/HBoxContainer/VBoxContainer/return_button")._set_text()
	global.logs_sound = true # Logs sound trigger

	get_node("gameControl/display_scene/MarginContainer/display").add_text("\n"+tr("Connection to a virtual host pending..."))
	get_node("gameControl/display_scene/MarginContainer/display").add_text("\n"+tr("Maker Warning: 5 messages to virtual host allowed")+"\n")

	global.wait_player = true
	### Countdown from 5 until bot launch
	while global.wait_player == true && no_player_countdown != 0:
		yield(get_tree().create_timer(1), "timeout")
		get_node("gameControl/display_scene/MarginContainer/display").add_text(str(no_player_countdown)+"\n")
		no_player_countdown -= 1

	rpc_id(1, "_player_check", get_tree().get_network_unique_id(), queue) # Notif server to check connections


remote func _player_check(id: int, queue: int) -> void: # Server checking the players_searching array // id = player_id / queue = check counter, 0 is for first check, 1 is for second
	if (global.players_searching.size()%2) != 0 && queue == 1: # Bot
		global.players_searching.erase(id)
		rpc_id(id, "_lauching_bot")
	else: # Multiplayer
		_search_av_player_dials(id) 


func _search_av_player_dials(id: int) -> void: # The server is matching 2 players...
	while global.players_searching.size() >= 2 && global.players_searching.size() > 0 :
		if duet_count < 2 : # Choosing players...
			var randomize_players = randi()%global.players_searching.size()
			duet.append(global.players_searching[randomize_players])
			if duet.size() > 1 && duet[0] == duet[1] : # Avoid to randomly choose the same player twice
				duet.remove(1)
			else: 
				duet_count += 1
		if duet_count >= 2 : # Launching the game for the 2 selected players, and sending infos about each other to players
			if global.players_searching.find(duet[0]) == -1: # Here we check if players are still looking for a dial
				global.players_searching.erase(duet[1])
				duet.remove(0)
				duet.remove(1)
				rpc_id(duet[1], "_lauching_bot")
			elif global.players_searching.find(duet[1]) == -1:
				global.players_searching.erase(duet[0])
				duet.remove(0)
				duet.remove(1)
				rpc_id(duet[0], "_lauching_bot")
			else :
				rpc_id(duet[0],"_creating_players", 1, duet[1])
				rpc_id(duet[1],"_creating_players", 2, duet[0])
				global.players_searching.erase(duet[0])
				global.players_searching.erase(duet[1])


remote func _lauching_bot() -> void: # Not enough players to play, player get the bot
	yield(get_tree().create_timer(1), "timeout")
	get_node("gameControl/display_scene/MarginContainer/display").add_text(tr("Entering...")+"\n")
	no_players = true
	### Launching bot
	var load_bot = bot.instance()
	get_node("gameControl").add_child(load_bot)

	global.wait_player = false
	get_node("gameControl/HBoxContainer/VBoxContainer/return_button").visible = false
	get_node("gameControl/HBoxContainer/VBoxContainer/CenterContainer/avatar").visible = false


remote func _creating_players(player_number: int, other_id: int) -> void: # Players are receiving infos about the other and then create there own instance of the game
	global.wait_player = false
	global.logs_sound = true # Logs sound trigger
	global.player_number = player_number
	global.other_id = other_id
	get_node("gameControl/display_scene/MarginContainer/display").add_text(tr("Entering...")+"\n")
	var load_player = player.instance()
	get_node("gameControl").add_child(load_player)
	get_node("gameControl/HBoxContainer/VBoxContainer/return_button").visible = false
	get_node("gameControl/HBoxContainer/VBoxContainer/return_button").set_disabled(true)
	get_node("gameControl/HBoxContainer/VBoxContainer/CenterContainer/avatar").visible = false
	rpc_id(1, "_reset_matching", get_tree().get_network_unique_id())


remote func _reset_matching(id: int) -> void: # Server reste matching array
	duet_count -= 1
	duet.erase(id)


func _exit_tree() -> void: # When players go back to the main menu
	global.ingame = false
	if global.current_scene == "player1" or global.current_scene == "player_bot" or global.current_scene == "makerz":
		if global.level == 4 and temp_level == 3:
			get_node("/root/connection_back/main_menu")._check_maker()

		if global.offline_mode == false : # If Online
			# Then we go on with the saving stage on the server
			if get_tree().get_network_unique_id() != 1 && global.maker_section == true : 
				rpc_id(1, "_save_game_datas_makers", global.player_name, global.player_pass, global.maker_section, global.current_subject) 
			elif get_tree().get_network_unique_id() != 1 && global.maker_section == false : # Exiting from dials
				rpc_id(1, "_save_game_datas_dials", global.player_name, global.player_pass, get_tree().get_network_unique_id(), global.p_buttons, global.other_name, global.logs_unlock, global.level, global.maker_section, global.factions, global.other_clan, global.avatar_links) #global.dialog_counter) # Saving unlock buttons and other things on the server (dials)
		else: # If Offline
			if  global.maker_section == true : 
				_save_game_datas_makers(global.player_name, global.player_pass, global.maker_section, global.current_subject) 
			elif  global.maker_section == false : # Exiting from dials
				_save_game_datas_dials(global.player_name, global.player_pass, "offline", global.p_buttons, global.other_name, global.logs_unlock, global.level, global.maker_section, global.factions, global.other_clan, global.avatar_links) #global.dialog_counter) # Saving unlock buttons and other things on the server (dials)
		### Reset of the dials and maker variables
		global.other_terminated = false
		global.question_ask = 0
		global.ingame = false
		global.count_ended = false
		global.hello = false
	else :
		if get_tree().get_network_unique_id() != 1 :
			rpc_id(1, "_quit_no_dial", get_tree().get_network_unique_id())

	### Saving Dialogs on exit...
	if global.dials_logs != null:
		var time = OS.get_time()
		var current_time = str(time.hour)+"."+str(time.minute)+"."+str(time.second)
		var date = OS.get_date()
		var current_date = str(date.month)+"."+str(date.day)

		var formated_dialog
		if global.offline_mode == false : # If Online
			if get_tree().get_network_unique_id() != 1 && global.maker_section == true : 
				formated_dialog = "L0G_MAKERZ: "+current_date+"//"+current_time+"\n"+"Room Topic: "+global.current_subject[1]+"\n"+"\n"+str(global.dials_logs)
			else:
				if global.factions.has(global.other_clan): 
					formated_dialog = "L0G : "+current_date+"//"+current_time+"_"+global.other_name+"\n"+"\n"+str(global.dials_logs)
				else:
					formated_dialog = "null"
		else: # If Offline
			if global.maker_section == true : 
				formated_dialog = "L0G_MAKERZ: "+current_date+"//"+current_time+"\n"+"Room Topic: "+global.current_subject[1]+"\n"+"\n"+str(global.dials_logs)
			else:
				if global.factions.has(global.other_clan): 
					formated_dialog = "L0G : "+current_date+"//"+current_time+"_"+global.other_name+"\n"+"\n"+str(global.dials_logs)
				else:
					formated_dialog = "null"

		var current_dialine
		### Loading of the dialfile
		var player_dialogs = File.new()
		if player_dialogs.file_exists("user://data/dialogs_"+global.player_name+".dat"):
			player_dialogs.open("user://data/dialogs_"+global.player_name+".dat", File.READ)
			current_dialine = parse_json(player_dialogs.get_as_text())

			current_dialine[(current_dialine.keys().size())+1] = formated_dialog

			### Saving new dialog in dialog file
			player_dialogs.open("user://data/dialogs_"+global.player_name+".dat", File.WRITE)
			player_dialogs.store_line(to_json(current_dialine))
			player_dialogs.close()

		else:
			current_dialine = {
				"1" : formated_dialog,
			}

		### Saving new dialog in dialog file
		player_dialogs.open("user://data/dialogs_"+global.player_name+".dat", File.WRITE)
		player_dialogs.store_line(to_json(current_dialine))
		player_dialogs.close()

	### Notifying main_menu: a maker is comming...
	if global.offline_mode == false :
		if get_tree().get_network_unique_id() != 1 && global.maker_section == true : 
			get_node("/root/connection_back/main_menu")._maker_section_exit() # Exiting from maker section, notifying main_menu
		else:
			get_node("/root/connection_back/main_menu")._dial_section_exit()
	else:
		if global.maker_section == true : 
			get_node("/root/connection_back/main_menu")._maker_section_exit() # Exiting from maker section, notifying main_menu
		else:
			get_node("/root/connection_back/main_menu")._dial_section_exit()
	global.dials_logs = null # We reset the dialog array we we leave


##### Server delete the player who's quiting before entering a dial
remote func _quit_no_dial(id:int) -> void:
	global.players_searching.erase(id)


##### Save Funcs
remote func _save_game_datas_dials(player_name: String, player_pass: String, player_id, player_buttons_new, other_name:String, player_logs, level: int, maker_section, factions, other_clan, avatar_links) -> void: # The server save dials
	### Hunting duplicates in player's unlock buttons
	var check: int = 0
	var erase
	var temp_player_but_new = player_buttons_new.duplicate()
	for i in temp_player_but_new.size():
		for x in temp_player_but_new.size():
			if temp_player_but_new[i] == temp_player_but_new[x]:
				check += 1
			else:
				pass
			if check == 2:
				erase = temp_player_but_new[i]
			x += 1
		check = 0
		i += 1
	player_buttons_new.erase(erase)
	### Hunting duplicates in player's unlock buttons

	### Loading of the savefile
	var save_game = File.new()
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.READ, player_pass)
	var current_saveline = parse_json(save_game.get_as_text())

	if maker_section == false : # Player quiting dials
		### We delete the old value from the dict
		current_saveline.erase("player_buttons") 
		current_saveline.erase("player_logs")
		current_saveline.erase("level")
		current_saveline.erase("factions")
		current_saveline.erase("avatar_links")
		### Adding the new keys and values to the dict
		current_saveline["player_buttons"] = player_buttons_new 
		current_saveline["player_logs"] = player_logs
		current_saveline["level"] = level
		current_saveline["factions"] = factions
		current_saveline["avatar_links"] = avatar_links

	### Writing new datas
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.WRITE, player_pass)
	save_game.store_line(to_json(current_saveline))
	save_game.close()
	if global.offline_mode == false:
		var time_ = OS.get_time()
		_server_data_save("Admin "+player_name+" saved at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))


remote func _save_game_datas_makers(player_name: String, player_pass: String, maker_section, current_topic) -> void: # dialog_count, player_group # The server save makers
	### Loading of the savefile
	var save_game = File.new()
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.READ, player_pass)
	var current_line = parse_json(save_game.get_as_text())
	if maker_section == true : # Player quiting makers
		### We delete the old value from the dict
		current_line.erase("current_topic")
		### Adding the new keys and values to the dict
		current_line["current_topic"] = current_topic

	### Writing new datas
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.WRITE, player_pass)
	save_game.store_line(to_json(current_line))
	save_game.close()
	if global.offline_mode == false:
		var time_ = OS.get_time()
		_server_data_save("Admin "+player_name+" saved at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))


##### Maker Funcs
remote func _retrieve_player_maker(player_id: int, player_name: String, player_group) -> void: # Server side
	if global.offline_mode == false:
		var time_ = OS.get_time()
		_server_data_save("Maker "+player_name+" arrived at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second)+" in Maker Section.")

	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/HBoxContainer/VBoxContainer/maker_list")._get_init_makers_data(player_id, player_name, player_group)
	rpc_id(player_id, "_player_join_group")


remote func _player_join_group() -> void: # Player join his group
	var load_player = player.instance()
	get_node("gameControl").add_child(load_player)


func _server_data_save(server_log_entry) -> void:
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)

		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry)

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry)
		data_server.close()
