extends Label # Counter for the game session

const count_end = preload("res://scenes/dials/countdown_ended.tscn") # Splashscreen for the counter reaching 0
const terminate = preload("res://scenes/dials/connection_terminated.tscn") # Splashscreen instance for the other player
var accum
var once = 0
var real_count: bool = true # Var to check if the counter ended or a player quit before
var sound_count: bool = true

func _ready() -> void:
	set_process(true)

	if global.offline_mode == false :
		if get_tree().is_network_server() :
			accum = 1
		else:
			### Setting the counter to a fit number
			accum = float(get_tree().get_network_unique_id())/2000000
			while accum >= 200 :
				accum /= 2
				if accum <= 25 :
					accum *= 2


func _set_bot_count() -> void: # Setting counter for player_bot
	accum = 5


func _process(delta) -> void:
	if global.ingame == true:
		if get_tree().is_network_server() : # If on the server = no countdown
			pass
		else :
			if global.other_terminated == true :
				pass
			else: 
				if once == 0 :
					if global.current_scene == "player_bot":
						pass
					else:
						accum -= delta
		set_text(str(int(accum)))
		if global.current_scene == "player_bot":
			if accum == 0:
				_count_ended(real_count)
		else:
			if int(accum) < 5 and accum > 4.98:
				get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/counter").counter()
			elif int(accum) < 4 and accum > 3.98:
				get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/counter").counter()
			elif int(accum) < 3 and accum > 2.98:
				get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/counter").counter()
			elif int(accum) < 2 and accum > 1.98:
				get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/counter").counter()
			elif accum <= 0: # If the counter reach 0 then we go back to the menu
				_count_ended(real_count)


func _bot_count() -> void: # Reduce count in player_bot
	accum -= 1


func _count_ended(real_count) -> void:
	global.count_ended = true
	if once == 0:
		if real_count == true && global.current_scene == "player1":
			rpc_id(global.other_id, "other_timing_ended") 
		else:
			pass
		var counting_ends = count_end.instance()
		get_parent().get_parent().get_parent().add_child(counting_ends)
	once = 1


remote func other_timing_ended() -> void:
	var terminated = terminate.instance() # Splash screen instance launch
	get_parent().get_parent().get_parent().add_child(terminated)
	global.other_terminated = true
