extends Button

onready var dials_display = get_node("/root/connection_back/main_menu/logs_background/Panel/MarginContainer/HBoxContainer/MarginContainer/HBoxContainer/dials_display_scrollcontainer/dials_display_margincontainer/logs_dials_display")
var entire_text
signal diallogs_choice

func _ready() -> void:
	set_process(true)


func _process(delta) -> void:
	if global.log_state == 1 or global.log_state == 3:
		get_node("..").queue_free()


func _get_text(log_text) -> void:
	entire_text = log_text

	var troncated_text = entire_text.substr(0,(entire_text.find("\n"))) # We extract the title of the dial from the saved file
	var array_troncated_text = troncated_text.split(' ', false) # We split the troncated text to look for makerz 

	if array_troncated_text[0].matchn("L0G_MAKERZ:"):
		get_node("..").queue_free()
	elif entire_text == "null":
		get_node("..").queue_free()
	else:
		get_node(".").set_text(troncated_text)
		connect("pressed", self, "_on_pressed")
		connect("diallogs_choice", get_node("../buttons_dialogs"), "on_pressed")


func _on_pressed() -> void:
	emit_signal("diallogs_choice") # Sound

	# Show display if necessary
	if dials_display.is_visible_in_tree() == false :
		dials_display.show()
	# Add actual text
	dials_display.clear() # Empty the label
	dials_display.add_text(entire_text.replace(("\n"+", "), "\n").replace(("["), "").replace(("]"), "")) # We add the dial text into the richtextlabel, replacing some part of the array with better text structure
