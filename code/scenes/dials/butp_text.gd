extends RichTextLabel

func _ready():
	get_node("..")._set_text()

	if get_total_character_count() > 15 :
		newline()
	_theme_selection()


func _theme_selection():
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else: # Dials
		if global.level == 4 : # Maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_maker_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_maker_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_maker_theme.tres")
		else: # Non-maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_theme.tres")
		get_node(".").set_theme(theme_)
