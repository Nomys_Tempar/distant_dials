extends Button

var active: bool

func _ready() -> void:
	set_process_input(true)
	set_action_mode(1)
	if global.current_scene != "makerz":
		get_node(".")._desactivated()
	else:
		_activated()
	set_text(tr("Enter"))


### Press 'Enter' to send an answser
func _input(event) -> void:
	if active == true :
		if event.is_action_pressed("Enter") || is_hovered() && event.is_action_pressed("Mouse") :
			global.logs_sound = true # Play log sound on display
			emit_signal("pressed")
			get_node("/root/connection_back/main_menu/loading/gameControl/dials")._key_pressed_on_dials(0) # 0 is holder_typing var in entry_text, removing all typing dots


func _activated() -> void:
	active = true
	get_node(".").set_disabled(false)


func _desactivated() -> void:
	active = false
	get_node(".").set_disabled(true)
