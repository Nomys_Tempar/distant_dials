extends AudioStreamPlayer

var sound_dict:Dictionary

func _ready() -> void:
	if get_tree().is_network_server() : pass # The server bypass sounds
	else:
		var sound_dict_to_load = File.new()
		sound_dict_to_load.open("res://json/sfx.json", sound_dict_to_load.READ)
		var sound_dict_load = sound_dict_to_load.get_as_text()
		sound_dict_to_load.close()
		sound_dict = parse_json(sound_dict_load)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("counter_bus"), 0)


func counter() -> void:
	if get_tree().is_network_server() : pass# The server bypass sounds
	else:
		if global.p_clan == "Voider" :
			get_node(".").stream = load(sound_dict.voiders_counter[0])
			get_node(".").play()
		if global.logs_sound == true && global.p_clan == "Builder" :
			get_node(".").stream = load(sound_dict.builders_counter[0])
			get_node(".").play()
		if global.logs_sound == true && global.p_clan == "Thinker" :
			get_node(".").stream = load(sound_dict.thinkers_counter[0])
			get_node(".").play()
