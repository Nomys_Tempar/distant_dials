extends Node

### This script was made for the display node, then reset to the root node of the scene
 
var display_text: Array = [] # Text array 
var see_text: String = "" # Text displayed
var valid_text: bool = false # Variable to limitate the non-relevant display on the display_text array
var display_range: int = 1
var first_question_asked: bool = false

var altered_text: Array = [] # Altered text array
var letters: String = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890" # Letters to randomize
var complete_random_text: String = "" # Concat of the content of altered_text array
var original_text_length: int # Length of the original text 
var typing: int = 0 # Dots counter for dots removal in _display_text

var text_dots

func _ready() -> void:
	if global.offline_mode == false :
		if get_tree().is_network_server() : pass
		else:
			_theme_selection()
	else:
		_theme_selection()

	randomize()
	set_process(true)
	get_node("MarginContainer/display").clear()
	get_node("MarginContainer/display").set_scroll_follow(true)

	get_node("HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").visible = false


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.level == 4 : # Maker in dials
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else: # Non-maker in dials
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_theme.tres")
	get_node("MarginContainer/display").set_theme(theme_)
	get_node("MarginContainer/counter").set_theme(theme_)


func _process(delta) -> void:
	if global.game_ready == true:
		global.game_ready = false
	
	### Here is the core mechanic of the dialogues display
	if global.add_display_question == true : # Display question is requiered, 
		if global.factions.has(global.other_clan) : # Clans are the same
			_questions()
		else: # Clans are different
			if global.player_asking == global.player_number: # Player questionning get the question...
				_questions()
			else : # ...Player receiving the question don't get it
				_crypt_question()
				if global.level == 1 : # If the player is still level 1
					if 8 in global.p_buttons: # If the button to access level 2 wasn't unlock...
						pass
					else:
						global.p_buttons.append(8) # We unlock it

	if global.add_display_answer == true: # Display answer is requiered
		if global.factions.has(global.other_clan) : # Clans are the same
			_answers()
		else : # Clans are different
			if global.player_asking != global.player_number: # Player answering get the answer...
				_answers()
			else : #...Player receiving the answer don't get it
				_crypt_answer()
				if global.level == 1 : # If the player is still level 1
					if 8 in global.p_buttons: # If the button to access level 2 wasn't unlock...
						pass
					else:
						global.p_buttons.append(8) # We unlock it
	### Here is the core mechanic of the dialogues display


### Dials Func
func _crypt_question() -> void: # Crypting questions if players doesn't understand each other
	altered_text.clear()
	complete_random_text = ""
	original_text_length = global.player_question.length()
	for i in original_text_length : # we create a random text in an array of the same length as the global.player_question
		altered_text.append(letters.substr((randi()%62),1))
	for i in range(0, altered_text.size()): # We concat the array to only have the text (not the hooks and commat)
		complete_random_text += altered_text[i]
	for i in range(display_range):
		display_text.append(global.other_name+": "+complete_random_text+"\n")
		display_range == display_range + 1
	global.add_display_question = false
	valid_text = true
	_display_text()


func _crypt_answer() -> void: # Crypting answers if players doesn't understand each other
	altered_text.clear()
	complete_random_text = ""
	original_text_length = global.player_entry.length()
	for i in original_text_length : # we create a random text in an array of the same length as the global.player_entry
		altered_text.append(letters.substr((randi()%62),1))
	for i in range(0, altered_text.size()): # We concat the array to only have the text (not the hooks and commas)
		complete_random_text += altered_text[i]
	for i in range(display_range):
		display_text.append(global.other_name+": "+complete_random_text+"\n")
		display_range == display_range + 1
	global.add_display_answer = false
	valid_text = true

	if global.current_scene == "player_bot": # Changing help status in dials with a bot
			get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Choose A Question"))
	_display_text()


func _questions() -> void: # Questions are entering the array
	for i in range(display_range):
		if global.state_number == 0 and global.current_scene == "player_bot": # When the bot is the same faction
			display_text.append(global.other_name+": "+global.player_question+"\n")
		elif global.state_number == 0 and global.current_scene != "player_bot":
			if global.player_asking == global.player_number:
				display_text.append(global.other_name+": "+global.player_question+"\n") # The question enters the display array
			else:
				display_text.append(global.player_name+": "+global.player_question+"\n") # The question enters the display array
		else:
			if global.player_asking == global.player_number :
				display_text.append(global.player_name+": "+global.player_question+"\n") # The question enters the display array
			else:
				display_text.append(global.other_name+": "+global.player_question+"\n") # The question enters the display array
		display_range == display_range + 1
		global.add_display_question = false
		valid_text = true

		if global.player_asking != global.player_number:
			global.logs_sound = true

	_display_text()


func _answers() -> void: # Answers are entering the array
	if global.current_scene == "makerz":
		pass
	else:
		for i in range(display_range):
			if global.player_asking == global.player_number :
				display_text.append(global.other_name+": "+global.player_entry+"\n") # The question enters the display array
			else:
				display_text.append(global.player_name+": "+global.player_entry+"\n") # The answer enters the display array
			display_range == display_range + 1
			global.add_display_answer = false
			valid_text = true

			if global.player_asking == global.player_number:
				global.logs_sound = true
				
			if global.current_scene == "player_bot": # Changing help status in dials with a bot
				get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Choose A Question"))

			_display_text()


func _display_text() -> void:
	### Extraction of the texts of the array
	for i in range(0,display_text.size()):
		 see_text = display_text[i]

	### Display of the answer
	if valid_text == true :
		if get_node("MarginContainer/display").get_total_character_count() == 21 and first_question_asked == false and global.maker_section == false :
			get_node("/MarginContainer/display").clear()
			first_question_asked = true
		else : 
			first_question_asked = true

		get_node("MarginContainer/display").add_text(see_text)
		valid_text = false

	global.dials_logs = display_text


func _display_dots(holder_typing) -> void: # Func to display temporary dots when the other player is typing
	typing = holder_typing 
	if holder_typing == 0: # Deleting the 3 dots
		text_dots = get_node("MarginContainer/display").get_text()
		text_dots.erase(text_dots.find_last("."), 1)
		text_dots.erase(text_dots.find_last("."), 1)
		text_dots.erase(text_dots.find_last("."), 1)
		get_node("MarginContainer/display").set_text(text_dots)
	else: # Adding the dots 
		get_node("MarginContainer/display").add_text(".")
