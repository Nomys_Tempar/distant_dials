extends Node

const questions_scene = preload("res://scenes/dials/questions_panel.tscn")
const player_text_entry = preload("res://scenes/dials/player_text.tscn")
const log_icon = preload("res://scenes/logs/log_icon.tscn")
const makerz = preload("res://scenes/maker/player_maker.tscn") # for server only

var count_buttons: int = 99
var ingame_temp_array: Array = [] # Temp array to store the players in a dial 
var groups = global.player_id_in_group
var temp_timer_other # Temp timer to let the player catch on when the question change without him in game 
var player_pic: String # String with the path of the player's picture
var help_temp


func _ready() -> void:
	global.current_scene = "player1"

### Maker Section
	if global.maker_section == true :
		get_node("MarginContainer/title").visible = false
		get_node("HBoxContainer/VBoxContainer").visible = false
		get_node("HBoxContainer/VBoxContainer/quit").set_disabled(true)
		get_node("HBoxContainer/dial_help").visible = false

		var player_makerz = makerz.instance()
		add_child(player_makerz)

### Dials
	else:
		get_node("../display_scene/MarginContainer/counter").visible = true
		get_node("HBoxContainer/VBoxContainer/make_maker").visible = false
		get_node("HBoxContainer/VBoxContainer/quit").visible = false
		get_node("HBoxContainer/VBoxContainer/quit").set_disabled(true)
		get_node("HBoxContainer/dial_help").visible = true
		get_node("HBoxContainer/dial_help").set_text(tr("Choose a question button"))

		get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").visible = true
		get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").color = Color(0,0,0,0)

		global.focus = 1
		global.ingame = true

		if get_tree().is_network_server() : # Server is also launching maker section 
			var player_makerz = makerz.instance()
			add_child(player_makerz)

		else:
			_theme_selection()
			rpc_id(global.other_id, "_send_name", global.player_name, global.p_clan, global.level, player_pic, global.avatar_links, global.player_color) # Players send their name and their clan to each other
			rpc_id(1, "_ingame_player", get_tree().get_network_unique_id()) # Sending player id to the Server

			var player_text = player_text_entry.instance()
			get_node("MarginContainer2/VBoxContainer").add_child(player_text)

			var questions_nodes = questions_scene.instance()
			get_node("MarginContainer2/VBoxContainer").add_child(questions_nodes)

			get_node("HBoxContainer/VBoxContainer/make_maker").connect("button_down", self, "_maker_pressed")
			get_node("HBoxContainer/VBoxContainer/make_maker").connect("mouse_entered", self, "_mouse_entered_maker")
			get_node("HBoxContainer/VBoxContainer/make_maker").connect("mouse_exited", self, "_mouse_exited_maker")

		get_node("buttons_player1")._scene_change() # Update audio button node


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.level == 4 : # Maker in dials
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else: # Non-maker in dials
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_theme.tres")

	get_node("HBoxContainer/VBoxContainer/quit").set_theme(theme_)
	get_node("MarginContainer/title").set_theme(theme_) 
	get_node("HBoxContainer/VBoxContainer/make_maker").set_theme(theme_)
	get_node("HBoxContainer/dial_help").set_theme(theme_)

	get_node("HBoxContainer/VBoxContainer/CenterContainer/avatar").set_texture(_load_png("user://data/"+global.player_name+"_avatar.png")) # We load the user avatar with the _load_png func and apply it to the TextureRect
	get_node("HBoxContainer/VBoxContainer/CenterContainer/avatar_border").color = Color(global.player_color[0], global.player_color[1], global.player_color[2], global.player_color[3])


func _load_png(file): # Loading user external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex


#### Dials Func
remote func _send_name(other_name: String, other_clan, other_level, other_pic, other_avatar_links, other_color) -> void: # Receiving other's infos and display of the title
	global.other_name = other_name
	global.other_clan = other_clan
	global.other_level = other_level
	global.other_color = other_color

	### Here player display other's avatar
	_other_avatar(other_avatar_links)
	yield(get_tree().create_timer(0.5), "timeout")

	if global.dev == false :
		get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other").set_texture(_load_other_png("user://data/other_avatar.png")) # We load the other avatar with the _load_png func and apply it to the TextureRect
	else:
		get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other").set_texture(_load_other_png("user://data/other_avatar_"+global.other_name+".png")) # Local testing only
	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").color = Color(global.other_color[0], global.other_color[1], global.other_color[2], global.other_color[3])
	### Here player display other's avatar

	if global.player_number == 1 :
		get_node("MarginContainer/title").set_text(tr("Admin dialing with")+global.other_name)

	if global.player_number == 2 :
		get_node("MarginContainer/title").set_text(tr("Admin dialing with")+global.other_name)

	if global.level == 4 :
		get_node("HBoxContainer/VBoxContainer/make_maker").visible = true
		if global.other_level == 3 :
			get_node("HBoxContainer/VBoxContainer/make_maker").set_disabled(false)
		else:
			get_node("HBoxContainer/VBoxContainer/make_maker").set_disabled(true)


### Other avatar handling
func _other_avatar(other_avatar_links) -> void:
	### Other Avatar Creation
		# Creating viewport
		var viewport = Viewport.new()
		viewport.size = Vector2(512, 512)
		viewport.render_target_update_mode =Viewport.UPDATE_ALWAYS
		add_child(viewport)
		viewport.set_vflip(true)

		# Create sprites
		var img_body = Sprite.new()
		viewport.add_child(img_body)
		img_body.set_centered(false)
		img_body.set_texture(load(other_avatar_links[0])) # Body

		var img_eyes = Sprite.new()
		viewport.add_child(img_eyes)
		img_eyes.set_centered(false)
		img_eyes.set_texture(load(other_avatar_links[1])) # Eyes

		var img_nose = Sprite.new()
		viewport.add_child(img_nose)
		img_nose.set_centered(false)
		img_nose.set_texture(load(other_avatar_links[2])) # Nose

		var img_mouth = Sprite.new()
		viewport.add_child(img_mouth)
		img_mouth.set_centered(false)
		img_mouth.set_texture(load(other_avatar_links[3])) # Mouth

		var img_hair = Sprite.new()
		viewport.add_child(img_hair)
		img_hair.set_centered(false)
		img_hair.set_texture(load(other_avatar_links[4])) # Hair

		var img_accessorie = Sprite.new()
		viewport.add_child(img_accessorie)
		img_accessorie.set_centered(false)
		img_accessorie.set_texture(load(other_avatar_links[5])) # Accessoirs

		# Wait for content
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")

		# Fetch viewport content
		var texture = viewport.get_texture()
		var other_avatar = texture.get_data()

		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")

		### Saving viewport content
		if global.dev == false:
			other_avatar.save_png("user://data/other_avatar.png")
		else:
			other_avatar.save_png("user://data/other_avatar_"+global.other_name+".png") # Local testing only
		### Other Avatar Creation

		viewport.free()


func _load_other_png(file): # Loading other external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex
### Other avatar handling


remote func _ingame_player(id: int) -> void: # Server receive ids of players in dials
	ingame_temp_array.append(id)
	_store_players(id)


func _store_players(id: int): # Server stores dial temp array in global.players_ingame
	if ingame_temp_array.size() == 2 :
		global.players_ingame.append(ingame_temp_array)
		var ingame_pos = global.players_ingame.size()
		ingame_pos -= 1
		rpc_id(id, "_dial_position", ingame_pos)
		var time_ = OS.get_time()
		_server_data_save("In dials at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second)+" = "+str(global.players_ingame))
		ingame_temp_array.clear()


remote func _dial_position(ingame_pos) -> void: # Clients receive the position of their own dial in the global.players_ingame array
	global.ingame_number = ingame_pos


func _key_pressed_on_dials(holder_typing) -> void: # Here we trigger the dots in the placeholder when a key is pressed
	rpc_id(global.other_id, "_other_typing", holder_typing)


remote func _other_typing(holder_typing) -> void: # Here we trigger the dots in the placeholder when a key is pressed
	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene")._display_dots(holder_typing)


remote func _dialer_exit(quitter_id: int, other_id: int, ingame_pos) -> void: # Server receiving data from a player quitting a dial before time ends
	var time_ = OS.get_time()
	_server_data_save("Client n°"+str(quitter_id)+"'s hard quit a dial at"+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))
	rpc_id(int(other_id), "_quitter")
	global.players_ingame.remove(ingame_pos)


remote func _quitter() -> void: # Function triggering quit screen for the remaining player when a player quit the dial before time ends
	var real_count = false
	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/MarginContainer/counter")._count_ended(real_count)


### Handling of the "Make Maker" button
func _maker_pressed() -> void: 
	rpc_id(global.other_id, "_maker_made")


func _mouse_entered_maker() -> void:
	help_temp = get_node("HBoxContainer/dial_help").get_text()
	if get_node("HBoxContainer/VBoxContainer/make_maker").is_disabled() == true:
		get_node("HBoxContainer/dial_help").set_text(global.other_name+tr(" isn't ready yet."))
	else:
		get_node("HBoxContainer/dial_help").set_text(global.other_name+tr(" can be turn."))


func _mouse_exited_maker() -> void:
	get_node("HBoxContainer/dial_help").set_text(help_temp)
### Handling of the "Make Maker" button


remote func _maker_made() -> void: # ... A new Maker is born
	global.level = 4
	_level_icon() # Activating leveling Icon

	### Player Avatar Upgrade to Maker
	# Loading the json
	var avatars_parts = File.new()
	avatars_parts.open("res://json/avatars.json", File.READ)
	var current_avatars = parse_json(avatars_parts.get_as_text())
	avatars_parts.close()
	# Randomizing to get the parts
	var rng = RandomNumberGenerator.new()
	var body_makerz = current_avatars[global.p_clan][global.player_sex]["body"].makerz[rng.randi_range(0,4)]

	global.avatar_links.remove(0) # removing old body from array
	global.avatar_links.insert(0, body_makerz)

	# Handling of the avatar parts
	var load_body = load(global.avatar_links[0])
	var load_eyes = load(global.avatar_links[1])
	var load_nose = load(global.avatar_links[2])
	var load_mouth = load(global.avatar_links[3])
	var load_hair = load(global.avatar_links[4])
	var load_accessorie = load(global.avatar_links[5])

	# Creating viewport
	var viewport = Viewport.new()
	viewport.size = Vector2(512, 512)
	viewport.render_target_update_mode =Viewport.UPDATE_ALWAYS
	add_child(viewport)
	viewport.set_vflip(true)

	# Create sprites
	var img_body = Sprite.new()
	viewport.add_child(img_body)
	img_body.set_centered(false)
	img_body.set_texture(load_body)

	var img_eyes = Sprite.new()
	viewport.add_child(img_eyes)
	img_eyes.set_centered(false)
	img_eyes.set_texture(load_eyes)

	var img_nose = Sprite.new()
	viewport.add_child(img_nose)
	img_nose.set_centered(false)
	img_nose.set_texture(load_nose)

	var img_mouth = Sprite.new()
	viewport.add_child(img_mouth)
	img_mouth.set_centered(false)
	img_mouth.set_texture(load_mouth)

	var img_hair = Sprite.new()
	viewport.add_child(img_hair)
	img_hair.set_centered(false)
	img_hair.set_texture(load_hair)

	var img_accessorie = Sprite.new()
	viewport.add_child(img_accessorie)
	img_accessorie.set_centered(false)
	img_accessorie.set_texture(load_accessorie)

	# Wait for content
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	# Fetch viewport content
	var texture = viewport.get_texture()
	var player_avatar = texture.get_data()

	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	### Saving viewport content
	player_avatar.save_png("user://data/"+global.player_name+"_avatar.png")
	### Player Avatar Upgrade to Maker


func _log_icon() -> void:
	var display_log = log_icon.instance()
	add_child(display_log)
	get_node("icon_control")._setup_icon("log")


func _level_icon() -> void:
	var display_level = log_icon.instance()
	add_child(display_level)
	if global.level != 4:
		get_node("icon_control")._setup_icon("level")
	else:
		get_node("icon_control")._setup_icon("makerz")


func _notification(quit) -> void:
	if quit == MainLoop.NOTIFICATION_WM_QUIT_REQUEST and get_tree().get_network_unique_id() != 1:
		if global.maker_section == false:
			rpc_id(1, "_dialer_exit", get_tree().get_network_unique_id(), global.other_name, global.ingame_number)
		else: pass
		get_tree().quit() # default behavior:


func _server_data_save(server_log_entry) -> void:
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)

		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry)

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry)
		data_server.close()
