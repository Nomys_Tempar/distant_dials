extends Node

const questions_scene = preload("res://scenes/dials/questions_panel.tscn")
const player_text_entry = preload("res://scenes/dials/player_text.tscn")
const log_icon = preload("res://scenes/logs/log_icon.tscn")

var count_buttons: int = 99
var ingame_temp_array: Array = [] # Temp array to store the players in a dial 
var groups = global.player_id_in_group
var temp_timer_other # Temp timer to let the player catch on when the question change without him in game 

func _ready() -> void:
	global.current_scene = "player_bot"

	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").visible = true
	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").color = Color(0,0,0,0)

	_theme_selection()

	### Loading of the button dictionary for the bot
	var buttons_file01 = File.new()
	buttons_file01.open("res://json/dialogues_"+global.locale+"_level01.json", buttons_file01.READ)
	var buttons_text1 = buttons_file01.get_as_text()
	buttons_file01.close()
	var level01 = parse_json(buttons_text1)

	var buttons_file02 = File.new()
	buttons_file02.open("res://json/dialogues_"+global.locale+"_level02.json", buttons_file02.READ)
	var buttons_text2 = buttons_file02.get_as_text()
	buttons_file02.close()
	var level02 = parse_json(buttons_text2)

	var buttons_file03 = File.new()
	buttons_file03.open("res://json/dialogues_"+global.locale+"_level03.json", buttons_file03.READ)
	var buttons_text3 = buttons_file03.get_as_text()
	buttons_file03.close()
	var level03 = parse_json(buttons_text3)

	var i: int = 0 # Number in the keys
#	var b = global.button_p % i # Text of the keys
	var buttons_temp_bot: Dictionary = {} # Temp dictionary to concat levels
	for b in level01 : # For a key in level01 dict
		buttons_temp_bot[global.button_p % i] = level01[b] # We add the key inside the temp dictionary (buttons_temp) 
		i += 1 # We increase the number to get the next key

	for b in level02 :
		buttons_temp_bot[global.button_p % i] = level02[b]
		i += 1

	for b in level03 :
		buttons_temp_bot[global.button_p % i] = level03[b]
		i += 1

	global.bot_button_dict = buttons_temp_bot # Finally the global.button_dict is set with the right keys and values

	get_node("../display_scene/MarginContainer/counter").visible = true
	get_node("HBoxContainer/VBoxContainer/make_maker").visible = false
	get_node("HBoxContainer/VBoxContainer/quit").visible = false
	get_node("HBoxContainer/VBoxContainer/quit").set_disabled(true)

	global.focus = 1
	global.ingame = true
	global.player_number = 1

	var player_text = player_text_entry.instance()
	get_node("MarginContainer2/VBoxContainer").add_child(player_text)

	var questions_nodes = questions_scene.instance()
	get_node("MarginContainer2/VBoxContainer").add_child(questions_nodes)

	_bot_setup()
	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/MarginContainer/counter")._set_bot_count()
	get_node("buttons_player1")._scene_change() # Update audio button node


func _theme_selection() -> void: ### Theme selection
	var theme_
	# Dials
	if global.level == 4 : # Maker in dials
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else: # Non-maker in dials
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_theme.tres")

	get_node("HBoxContainer/VBoxContainer/quit").set_theme(theme_)
	get_node("MarginContainer/title").set_theme(theme_) 
	get_node("HBoxContainer/VBoxContainer/make_maker").set_theme(theme_)
	get_node("HBoxContainer/dial_help").set_theme(theme_)

	get_node("HBoxContainer/VBoxContainer/CenterContainer/avatar").set_texture(_load_png("user://data/"+global.player_name+"_avatar.png")) # We load the user avatar with the _load_png func and apply it to the TextureRect
	get_node("HBoxContainer/VBoxContainer/CenterContainer/avatar_border").color = Color(global.player_color[0], global.player_color[1], global.player_color[2], global.player_color[3])


func _load_png(file): # Loading user external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex


#### Dials Bot Func
func _bot_setup() -> void: # Reciving other's infos and display of the title
	global.other_name = "Makerz_Construct"

	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var rand
	if global.level == 1: # 1 chance on 2 to understand if player is level 1
		rand = rng.randi_range(0,1) 
		if rand ==  0:
			global.other_clan = global.p_clan
		else:
			rng.randomize()
			rand = rng.randi_range(0,2)
			var factions = ["Voider", "Builder", "Thinker"]
			while factions[rand] == global.p_clan:
				rng.randomize()
				rand = rng.randi_range(0,2)
			global.other_clan = factions[rand]
	else: # 1 chance on 3 (normal)
		rand = rng.randi_range(0,2)
		var other_pic
		if rand == 0:
			global.other_clan = "Voider"
		elif rand == 1:
			global.other_clan = "Builder"
		elif rand == 2:
			global.other_clan = "Thinker"

	### Here the bot generate its avatar and display it
	_other_avatar() # Bot Avatar Creation
	yield(get_tree().create_timer(0.2), "timeout")
	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other").set_texture(_load_bot_png("user://data/other_avatar.png")) # We load the bot avatar with the _load_png func and apply it to the TextureRect
	### Here the bot generate its avatar and display it

	get_node("MarginContainer/title").set_text(tr("Admin dialing with")+global.other_name)

	get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/questions_panel/but_hboxcontainer")._bot_asking(1) # The bot is asking a question (1 is for the first time)


func _other_avatar() -> void:
	### Bot Avatar Creation
	var body_bot
	var sex_bot

	# Loading the json
	var other_parts = File.new()
	other_parts.open("res://json/avatars.json", File.READ)
	var current_other = parse_json(other_parts.get_as_text())
	other_parts.close()

	# Randomizing to get the parts
	var rng = RandomNumberGenerator.new()
	rng.randomize()

	var randi_sex = rng.randi_range(0,2)
	if randi_sex == 0:
		sex_bot = "female"
	elif randi_sex == 1:
		sex_bot = "male"
	elif randi_sex == 2:
		sex_bot = "other"

	rng.randomize()
	var randi_level = rng.randi_range(0,1)
	rng.randomize()
	if randi_level == 0: # Bot isn't maker
		body_bot = current_other[global.other_clan][sex_bot]["body"].normal[rng.randi_range(0,4)]
		global.other_level = 3 # So bot is level 3
	else: # Bot is maker
		body_bot = current_other[global.other_clan][sex_bot]["body"].makerz[rng.randi_range(0,4)]
		global.other_level = 4 # So bot is level 4 (makerz)
	rng.randomize()
	var eyes = current_other[global.other_clan][sex_bot].eyes[rng.randi_range(0,4)]
	rng.randomize()
	var nose = current_other[global.other_clan][sex_bot].nose[rng.randi_range(0,4)]
	rng.randomize()
	var mouth = current_other[global.other_clan][sex_bot].mouth[rng.randi_range(0,4)]
	rng.randomize()
	var hair = current_other[global.other_clan][sex_bot].hair[rng.randi_range(0,4)]
	rng.randomize()
	var accessorie = current_other[global.other_clan][sex_bot].accessorie[rng.randi_range(0,4)]

	### Setup color frame for bot's avatar
	if global.other_level == 3:
		if global.other_clan == "Voider":
			get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").color = Color(0.2, 0.682353, 0.803922, 0.7) # blue for voiders
		elif global.other_clan == "Builder":
			get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").color = Color(0.780392, 0.313726, 0.027451, 0.7) # red for builders
		elif global.other_clan == "Thinker":
			get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").color = Color(0.996078, 0.996078, 0.996078, 0.7) # almost white for thinkers
	else:
		get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/HBoxContainer/VBoxContainer/CenterContainer/avatar_other_border").color = Color(0.01, 0.77, 0.12, 0.7) # electric green for makerz

	# Creating viewport
	var viewport = Viewport.new()
	viewport.size = Vector2(512, 512)
	viewport.render_target_update_mode =Viewport.UPDATE_ALWAYS
	add_child(viewport)
	viewport.set_vflip(true)

	# Create sprites
	var img_body = Sprite.new()
	viewport.add_child(img_body)
	img_body.set_centered(false)
	img_body.set_texture(load (body_bot))

	var img_eyes = Sprite.new()
	viewport.add_child(img_eyes)
	img_eyes.set_centered(false)
	img_eyes.set_texture(load (eyes))

	var img_nose = Sprite.new()
	viewport.add_child(img_nose)
	img_nose.set_centered(false)
	img_nose.set_texture(load (nose))

	var img_mouth = Sprite.new()
	viewport.add_child(img_mouth)
	img_mouth.set_centered(false)
	img_mouth.set_texture(load (mouth))

	var img_hair = Sprite.new()
	viewport.add_child(img_hair)
	img_hair.set_centered(false)
	img_hair.set_texture(load (hair))

	var img_accessorie = Sprite.new()
	viewport.add_child(img_accessorie)
	img_accessorie.set_centered(false)
	img_accessorie.set_texture(load (accessorie))

	# Wait for content
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	# Fetch viewport content
	var texture = viewport.get_texture()
	var bot_avatar = texture.get_data()

	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	### Saving viewport content
	bot_avatar.save_png("user://data/other_avatar.png")
	### Player Avatar Creation

	viewport.free()


func _load_bot_png(file): # Loading bot external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex


func _level_icon() -> void:
	var display_level = log_icon.instance()
	add_child(display_level)
	if global.level != 4:
		get_node("./icon_control")._setup_icon("level")
	else:
		get_node("./icon_control")._setup_icon("makerz")


func _log_icon() -> void:
	var display_log = log_icon.instance()
	add_child(display_log)
	get_node("icon_control")._setup_icon("log")


func _bot_make_maker() -> void:
	global.level = 4
	_level_icon() # Activating leveling Icon

	### Player Avatar Upgrade to Maker
	# Loading the json
	var avatars_parts = File.new()
	avatars_parts.open("res://json/avatars.json", File.READ)
	var current_avatars = parse_json(avatars_parts.get_as_text())
	avatars_parts.close()
	# Randomizing to get the parts
	var rng = RandomNumberGenerator.new()
	var body_makerz = current_avatars[global.p_clan][global.player_sex]["body"].makerz[rng.randi_range(0,4)]

	global.avatar_links.remove(0) # removing old body from array
	global.avatar_links.insert(0, body_makerz)

	# Handling of the avatar parts
	var load_body = load(global.avatar_links[0])
	var load_eyes = load(global.avatar_links[1])
	var load_nose = load(global.avatar_links[2])
	var load_mouth = load(global.avatar_links[3])
	var load_hair = load(global.avatar_links[4])
	var load_accessorie = load(global.avatar_links[5])

	# Creating viewport
	var viewport = Viewport.new()
	viewport.size = Vector2(512, 512)
	viewport.render_target_update_mode =Viewport.UPDATE_ALWAYS
	add_child(viewport)
	viewport.set_vflip(true)

	# Create sprites
	var img_body = Sprite.new()
	viewport.add_child(img_body)
	img_body.set_centered(false)
	img_body.set_texture(load_body)

	var img_eyes = Sprite.new()
	viewport.add_child(img_eyes)
	img_eyes.set_centered(false)
	img_eyes.set_texture(load_eyes)

	var img_nose = Sprite.new()
	viewport.add_child(img_nose)
	img_nose.set_centered(false)
	img_nose.set_texture(load_nose)

	var img_mouth = Sprite.new()
	viewport.add_child(img_mouth)
	img_mouth.set_centered(false)
	img_mouth.set_texture(load_mouth)

	var img_hair = Sprite.new()
	viewport.add_child(img_hair)
	img_hair.set_centered(false)
	img_hair.set_texture(load_hair)

	var img_accessorie = Sprite.new()
	viewport.add_child(img_accessorie)
	img_accessorie.set_centered(false)
	img_accessorie.set_texture(load_accessorie)

	# Wait for content
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	# Fetch viewport content
	var texture = viewport.get_texture()
	var player_avatar = texture.get_data()

	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	### Saving viewport content
	player_avatar.save_png("user://data/"+global.player_name+"_avatar.png")
	### Player Avatar Upgrade to Maker
