extends HBoxContainer #### Retrieving and checking player's answers 

var ping_answers: int = 0 # Status of the answer

func _ready() -> void:
	get_node("valid_player_entry").connect("pressed", self, "_on_pressed")
	get_node("keys_entry")._key_change() # Update audio button node
	_theme_selection()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else: # Dials
		if global.level == 4 : # Maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_maker_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_maker_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_maker_theme.tres")
		else: # Non-maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_theme.tres")
		get_node("entry_text").set_theme(theme_)
		get_node("valid_player_entry").set_theme(theme_)


func _on_pressed() -> void: ### Retrieving player's answer
	var once: bool = false
	if get_node("entry_text").get_text() == "": # We check if the answer has text
		get_node("entry_text").set_placeholder(tr("Please, enter a reply...")) # If not we ask the player to reply
	else:
		### Testing the answer for banned words
		var array_word_ans = get_node("entry_text").get_text().split(' ', false) # We split the answer to words
		var banned_word_ar = ["zebrafucker", "whorefucker", "twerp", "twat", "turdball", "trolltard", "trollface", "tranny", "stinkhole", "stinker", "stink cunt", "son of a whore", "son of a bitch", "sluthead", "slutfucker", "slutbag", "skunkfucker", "skunk", "skullfucker", "sisterfucker", "sissy", "sibling fucker", "shitsack", "shitpiss", "shitlord", "shitizen", "shithead", "shitgoblin", "shitfucker", "shitfuck", "shit-for-brains", "shitface", "shit-eater", "shitdick", "shitcunt", "shitbreath", "shitbrain", "shitbird", "shitball", "shitbag", "shitass", "sheepshagger", "sheepfucker", "scumwad", "scumlord", "scumhead", "scumfuck", "scumface", "scumbutt", "scumbreath", "scumbag", "saggy fuck", "retard", "ratfucker", "rabbitfucker", "pussy pisser", "pukeball", "puke bag", "prickface", "prick", "poop hole", "poohead", "pooface", "Pissy bitch", "pisshead", "pissface", "pissbreath", "pissbrain", "pissbaby", "pigfucker", "Penis licker", "penis face", "pencil dick", "pedo", "pain in the ass", "numbskull", "numbnuts", "niece fucker", "mousefucker", "mental midget", "kidfucker", "horse's ass", "horsefucker", "headass", "handfucker", "grandmotherfucker", "gaytard", "fucktard", "fuckster", "fuckskull", "fuckshit", "fucklord", "fucking bitch", "fuckhead", "fuckface", "fucker", "fuckbait", "fuckass", "flag fucker", "fishwife", "fish fucker", "fat-tard", "fatherfucker", "fatfuck", "fatass", "fartface", "fagtard", "faggotface", "fag", "dumb fucker", "dumbass", "dumbarse", "duckfucker", "drunkard", "douchefag", "douche bag", "douche", "donkeyfucker", "dogfucker", "dirtbag", "dipshit", "dipfuck", "dicktard", "dick mouth", "dicklicker", "dickless", "dickhole", "dickhead", "dickfucker", "dickface", "dickcheese", "dickbreath", "dickbag", "dickass", "degenerate", "deadhead", "daughterfucker", "daddyfucker", "cuntsucker", "cuntlicker", "cuntfucker", "cunt fart", "cuntface", "cuntbreath", "cunt", "cumslut", "cum-licker", "cum dumpster", "cumbucket", "cuckold", "crook", "craphole", "crack whore", "crackhead", "cowfucker", "corpsefucker", "cockweasel", "cocksucker", "cockhead", "cockfucker", "clithead", "clitbag", "chump", "childfucker", "chickenfucker", "catfucker", "buttlicker", "butthead", "buttfucker", "buttbreath", "bitchface", "motherfucker", "birdfucker", "bimbo", "Beta cuck", "bastard", "Barbie-fucker", "ballsack", "Bag whore", "bag of dicks", "badgerfucker", "badgerfucker", "aunt fucker", "asstard", "ass sucker", "ass-nugget", "assmunch", "assmouth", "assmonkey", "ass-licker", "ass-kisser", "asshole", "asshat", "assfucker", "assfaggot", "assfag", "assface", "asscunt", "ass clown", "assbutt", "assbag", "assaholic", "arse-licker", "arsehole", "arseface", "arsecunt", "arsebreath", "apefucker", "shit", "bitch", "whore", "fuck", "slut", "merde", "pute", "con", "salop", "salope", "putain", "bordel", "pétasse", "catin", "fils de pute", "fille de pute", "connard", "conasse", "pd", "pédé", "pédale", "tapette", "tantouze", "fiotte", "tarlouze", "sac à foutre", "couille mole", "salopard", "biatch", "cagole", "shemale", "travelo", "gouine", "nègre", "negro", "nigga", "nigger", "chintoque", "bougnoul", "mongol", "mongolien", "triso", "taré", "abruti", "pochtron", "pochtronne", "clochard", "clocharde", "clodo", "pisseuse", "enflure", "enfoiré", "mange-merde", "raclure", "trou du cul", "puta", "cabron", "cyka", "blyat"] # Needed to be extend to other langages
#		var i = 0
		for i in array_word_ans.size(): # We test the words of the answer one by one
#			var y = 0
			for y in banned_word_ar.size(): # For eatch word in banned_word_ar array
				if (array_word_ans[i].matchn(banned_word_ar[y])):
					get_node("entry_text").clear()
					get_node("entry_text").set_placeholder(tr("Wrong answer"))
					return
		### Testing the answer for banned words

	if global.question_ask == 0:
		get_node("entry_text").clear()
	elif global.other_terminated == true: pass
	else:
		if global.player_asking == 1 and once == false : # Player_1 asking...
			once = true
			if global.player_number == 1 :
				get_node("entry_text").clear()
			if global.player_asking == 1 and global.player_number == 2 : # Player_2 can answer
				global.player_entry = get_node("entry_text").get_text()
				get_node("entry_text").clear()	
				global.add_display_answer = true # display answer
				if global.current_scene == "player1":
					rpc_id(global.other_id, "_sending_answer", global.player_entry) # Sending answer on the questionner
				else:
					_analyze_answer() # For player answering to unlock the button

		if global.player_asking == 2 and once == false : # Player_2 asking...
			once = true
			if global.player_number == 2 : 
				get_node("entry_text").clear()
			if global.player_asking == 2 and global.player_number == 1 : # Player_1 can answer
				global.player_entry = get_node("entry_text").get_text()
				get_node("entry_text").clear()
				global.add_display_answer = true # display answer
				if global.current_scene == "player1":
					rpc_id(global.other_id, "_sending_answer", global.player_entry) # Sending answer on the questionner
				else:
					_analyze_answer() # For player answering to unlock the button
	get_node("./valid_player_entry")._desactivated()
	get_node("./entry_text")._desactivated()
	get_node("./entry_text").clear()
	get_node("./entry_text").set_placeholder("")
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Wait For A Question"))


remote func _sending_answer(player_ans: String) -> void: # Questionner reveiving answer
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Choose A Question"))
	global.player_entry = player_ans
	global.add_display_answer = true # display answer
	_analyze_answer() # For player who unlock the button
	global.logs_sound = true # Logs sound trigger


### Analizing of the answer by the questionner and processing to display
func _analyze_answer() -> void:
	var one_word_ans: bool = true # Variable to trigger the search for a long answer

	global.questions_states = global.questions_buttons % global.state_number

	var array_word_ans = global.player_entry.split(' ', false) # We split the answer to words

	if global.player_entry == tr("I am one of them... And you are one too now."): # Tweak for the bot's special answer
		array_word_ans = ["crafters"]

	for i in array_word_ans.size(): # We test the words of the answer one by one
		if (array_word_ans[i].matchn(global.buttons_dict[global.questions_states].question_answers[0]))or(array_word_ans[i].matchn(global.buttons_dict[global.questions_states].question_answers[1]))or(array_word_ans[i].matchn(global.buttons_dict[global.questions_states].question_answers[2]))or(array_word_ans[i].matchn(global.buttons_dict[global.questions_states].question_answers[3]))or(array_word_ans[i].matchn(global.buttons_dict[global.questions_states].question_answers[4]))or(array_word_ans[i].matchn(global.buttons_dict[global.questions_states].question_answers[5])):
#			var x = 0
			for x in global.buttons_dict[global.questions_states].unlock_buttons.size():
				global.button_number_p = global.buttons_dict[global.questions_states].unlock_buttons[x]
				global.num = global.button_p % global.button_number_p # Updating the global.num variable to unlock the button
				if global.current_scene == "player_bot" and global.factions.has(global.other_clan) == false : pass# If player doesn't understand the bot, answers doesn't unlock buttons
				else:
					global.good_answers = true
					if global.buttons_dict[global.questions_states].question_state == 9 : # If the question just answered is the leveling one 
						if global.level == 1 : # We check the current level of the player
							global.level = 2

							var factions = ["Voider","Thinker", "Builder"]
							var p_faction = [global.p_clan]
							var new_f
							for y in factions.size():
								p_faction.push_front(factions[y])
								if p_faction[0] == global.p_clan:
									p_faction.remove(0)
									y += 1
								else:
									new_f = factions[y]
									break
							global.factions.append(new_f)

							get_node("/root/connection_back/main_menu/loading/gameControl/dials")._level_icon() # Activating leveling Icon

					if global.buttons_dict[global.questions_states].question_state == 29 : # If the question just answered is the leveling one 
						if global.level == 2 : # We check the current level of the player
							global.level = 3
							get_node("/root/connection_back/main_menu/loading/gameControl/dials")._level_icon() # Activating leveling Icon
							global.factions = ["Voider","Thinker", "Builder"]
				ping_answers = 1
				if global.current_scene == "player1": # Checking if answer comes from a player or the bot
					rpc_id(global.other_id,"_ping_ans", 1)
		else :
			ping_answers = 1
			if global.current_scene == "player1": # Checking if answer comes from a player or the bot
				rpc_id(global.other_id,"_ping_ans", 1)
			one_word_ans = false
		if ping_answers != 0 : # Reset questions for the player analyzing the answer
			global.question_ask = 0
			ping_answers = 0
		i += 1
	if one_word_ans == false :
		if (global.player_entry.matchn(global.buttons_dict[global.questions_states].question_answers[0]))or(global.player_entry.matchn(global.buttons_dict[global.questions_states].question_answers[1]))or(global.player_entry.matchn(global.buttons_dict[global.questions_states].question_answers[2]))or(global.player_entry.matchn(global.buttons_dict[global.questions_states].question_answers[3]))or(global.player_entry.matchn(global.buttons_dict[global.questions_states].question_answers[4]))or(global.player_entry.matchn(global.buttons_dict[global.questions_states].question_answers[5])):
			for x in global.buttons_dict[global.questions_states].unlock_buttons.size():
				global.button_number_p = global.buttons_dict[global.questions_states].unlock_buttons[x]
				global.num = global.button_p % global.button_number_p # Updating the global.num variable to unlock the button
				if global.current_scene == "player_bot" and global.factions.has(global.other_clan) == false : pass# If player doesn't understand the bot, answers doesn't unlock buttons
				else:
					global.good_answers = true

					if global.buttons_dict[global.questions_states].question_state == 9 : # If the question just answered is the leveling one
						if global.level == 1 : # We check the current level of the player
							global.level = 2

							var factions = ["Voider","Thinker", "Builder"]
							var p_faction = [global.p_clan]
							var new_f
							for y in factions.size():
								p_faction.push_front(factions[y])
								if p_faction[0] == global.p_clan:
									p_faction.remove(0)
									y += 1
								else:
									new_f = factions[y]
									break
							global.factions.append(new_f)

							get_node("/root/connection_back/main_menu/loading/gameControl/dials")._level_icon() # Activating leveling Icon

					if global.buttons_dict[global.questions_states].question_state == 29 : # If the question just answered is the leveling one 
						if global.level == 2 : # We check the current level of the player
							global.level = 3
							get_node("/root/connection_back/main_menu/loading/gameControl/dials")._level_icon() # Activating leveling Icon
							global.factions = ["Voider","Thinker", "Builder"]

					if global.buttons_dict[global.questions_states].question_state == 29 : # If the question just answered is the leveling one 
						if global.level == 2 : # We check the current level of the player
							global.level = 3
							get_node("/root/connection_back/main_menu/loading/gameControl/dials")._level_icon() # Activating leveling Icon
							global.factions = ["Voider","Thinker", "Builder"]
				ping_answers = 1
				if global.current_scene == "player1": # Checking if answer comes from a player or the bot
					rpc_id(global.other_id,"_ping_ans", 1)
		else :
			ping_answers = 1
			if global.current_scene == "player1": # Checking if answer comes from a player or the bot
				rpc_id(global.other_id,"_ping_ans", 1)
		if ping_answers != 0 : # Reset questions for the player analyzing the answer
			global.question_ask = 0
			ping_answers = 0

	if global.current_scene == "player_bot": # In player_bot counter reduce when a question is processed
		get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/MarginContainer/counter")._bot_count()


remote func _ping_ans(ping) -> void: # Reset questions for the questionner only !
	ping_answers = ping
	if ping_answers != 0 :
		global.question_ask = 0
		ping_answers = 0


func _bot_managing_question() -> void:
	yield(get_tree().create_timer(1), "timeout")
	if global.count_ended == true: # If the countdown ended before a answer is displayed
		return
	else:
		var question_button = global.questions_buttons % global.state_number
		if question_button == "button_9": # Bot managing question 9
			if global.p_clan == "Voider":
				global.player_entry = global.buttons_dict[question_button].question_answers[2]
			elif global.p_clan == "Builder":
				global.player_entry = global.buttons_dict[question_button].question_answers[1]
			elif global.p_clan == "Thinker":
				global.player_entry = global.buttons_dict[question_button].question_answers[0]
		elif question_button == "button_49": # Bot managing question 49
			if global.level == 4 and global.other_level == 4:
				global.player_entry = tr("I am one of them... And you are one too now.") # Custom answer when player is maker
			else:
				global.player_entry = global.buttons_dict[question_button].question_answers[0] # Bot is answering
		else:
			global.player_entry = global.buttons_dict[question_button].question_answers[0] # Bot is answering
			
		if global.count_ended == true: # If the countdown ended before a answer is displayed
			return

		global.add_display_answer = true # display answer
		_analyze_answer()
