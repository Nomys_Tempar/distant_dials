extends Label

func _ready() -> void:
	_theme_selection()
	set_text(tr("CONNECTION TERMINATED"))


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.level == 4:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else:
		if global.p_clan == "Voider":	
			theme_ = load("res://ui/voiders_theme.tres") # Theme...
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_theme.tres") # Theme...
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_theme.tres") # Theme...
	get_node(".").set_theme(theme_)
	get_node("../back_menu").set_theme(theme_)
	get_node("../../../../Panel").set_theme(theme_)
