extends Button

var state_number
var p_buttons_size = global.p_buttons.size() # Number of buttons unlock by player 1
var unlock_buts # Buttons unlock by the button
var button_logs # Log unlock by the button
var level01: Dictionary # Dictionary of the level 1
var level02: Dictionary # Dictionary of the level 2
var level03: Dictionary # Dictionary of the level 3
var state_text: int # 0 for gen, 1 for good answers/unlocking buttons

func _ready() -> void:
	set_process(true)

	### Loading of jsons for the level's questions
	var buttons_file01 = File.new()
	buttons_file01.open("res://json/dialogues_"+global.locale+"_level01.json", buttons_file01.READ)
	var buttons_text1 = buttons_file01.get_as_text()
	buttons_file01.close()
	level01 = parse_json(buttons_text1)
	if global.level >= 2:
		var buttons_file02 = File.new()
		buttons_file02.open("res://json/dialogues_"+global.locale+"_level02.json", buttons_file02.READ)
		var buttons_text2 = buttons_file02.get_as_text()
		buttons_file02.close()
		level02 = parse_json(buttons_text2)
	if global.level >= 3:
		var buttons_file03 = File.new()
		buttons_file03.open("res://json/dialogues_"+global.locale+"_level03.json", buttons_file03.READ)
		var buttons_text3 = buttons_file03.get_as_text()
		buttons_file03.close()
		level03 = parse_json(buttons_text3)

	_concat_levels() # Function to concat jsons inside one dict

	if global.buttons_loading == true : # Settings of buttons for player 1 (loading stage)
		global.loading_num = global.button_p % global.p_buttons[global.gen_array]
		set_name(global.buttons_dict[global.loading_num].name)
		state_text = 0
		global.gen_array += 1
		global.loading_button_number_p += 1 

		state_number = global.buttons_dict[global.loading_num].question_state
		unlock_buts = global.buttons_dict[global.loading_num].unlock_buttons
		button_logs = global.buttons_dict[global.loading_num].logs

		if global.loading_button_number_p >= p_buttons_size :
			global.buttons_loading = false

	else : # Settings of buttons for player 1 when an answer is right
		set_name(global.buttons_dict[global.num].name)
		state_text = 1
		state_number = global.buttons_dict[global.num].question_state
		unlock_buts = global.buttons_dict[global.num].unlock_buttons
		button_logs = global.buttons_dict[global.num].logs
	if get_tree().is_network_server() == false :
		_theme_selection() # Set theme of the button
		_set_text() # Set text of the button

	set_tooltip(get_node(".").get_text())
	connect("button_down", self, "_is_down")


func _theme_selection() -> void:
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			pass
		if global.p_clan == "Builder":
			pass
		if global.p_clan == "Thinker":
			pass
	else: # Dials
		if global.level == 4 : # Maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_maker_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_maker_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_maker_theme.tres")
		else: # Non-maker in dials
			if global.p_clan == "Voider":
				theme_ = load("res://ui/voiders_theme.tres")
			if global.p_clan == "Builder":
				theme_ = load("res://ui/builders_theme.tres")
			if global.p_clan == "Thinker":
				theme_ = load("res://ui/thinkers_theme.tres")
		get_node(".").set_theme(theme_)


### Retriving the question of the button be pressed
func _is_down() -> void:
	if not get_tree().is_network_server() and global.question_ask == 0 :
			get_node("button_audio").on_pressed()
			global.player_question = get_text()
			global.state_number = state_number
			global.unlock_buttons = unlock_buts
			global.button_logs = button_logs


### Activation/desactivation of the buttons
func _process(delta) -> void:
	if global.other_terminated == true or global.count_ended == true : # Countdown related check
		set_disabled(true)
	else:
		if global.current_scene == "player_bot": # If the bot is on
			if get_node(".").name == "hello":
				set_disabled(true)
				get_node(".").visible = false
			elif get_node(".").name != "hello":
				if global.question_ask == 1:
					set_disabled(true)
				else:
					set_disabled(false)
		else: # If we're in "player1" scene (not bot)
			if global.hello == false: # If "hello" hasn't been said
				if get_node(".").name == "hello":
					set_disabled(false)
				else:
					set_disabled(true)
			if global.hello == true:
				if get_node(".").name == "hello":
					set_disabled(true)
				else:
					if global.question_ask == 0 && global.player_asking != global.player_number:
						set_disabled(true)
					elif global.question_ask == 1:
						set_disabled(true)
					else:
						set_disabled(false)


### We concat level's jsons for level management 
func _concat_levels() -> void: 
	var buttons_temp: Dictionary = {} # Temp dictionary to concat levels
	for y in level01.size() : # For a key in level01 dict
		var level_buttons: Array = level01.keys().duplicate(true)
		buttons_temp[level01.keys()[y]] = level01.get(level01.keys()[y]) # We add the key inside the temp dictionary (buttons_temp) 

	if global.level >= 2 : # Same but for the level 2 (if it's unlock)
		for y in level02.size() :
			buttons_temp[level02.keys()[y]] = level02.get(level02.keys()[y])

	if global.level >= 3 : # Same but for the level 3 (if it's unlock)
		for y in level03.size() :
			buttons_temp[level03.keys()[y]] = level03.get(level03.keys()[y])

	global.buttons_dict = buttons_temp # Finally the global.button_dict is set with the right keys and values


func _set_text() -> void: # Settings of the buttons
	if state_text == 0 :
		get_node(".").set_text(global.buttons_dict[global.loading_num].player_question)
	else :
		get_node(".").set_text(global.buttons_dict[global.num].player_question)
