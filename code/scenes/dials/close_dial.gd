extends AudioStreamPlayer

var sound_dict: Dictionary

func _ready() -> void:
	### Opening the json
	var sound_dict_to_load = File.new()
	sound_dict_to_load.open("res://json/sfx.json", sound_dict_to_load.READ)
	var sound_dict_load = sound_dict_to_load.get_as_text()
	sound_dict = parse_json(sound_dict_load)
	sound_dict_to_load.close()
	
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("counter_bus"), 0)

	if global.p_clan == "Voider" :
		get_node(".").stream = load(sound_dict.voiders_close_dial[0])
		get_node(".").play()
		
	elif global.p_clan == "Builder" :
		get_node(".").stream = load(sound_dict.builders_close_dial[0])
		get_node(".").play()
	
	elif global.p_clan == "Thinker" :
		get_node(".").stream = load(sound_dict.thinkers_close_dial[0])
		get_node(".").play()
