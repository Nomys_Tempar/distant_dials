extends Control

const quest_ions_p = preload("res://scenes/dials/buttons_p1.tscn")

var p_buttons_size = global.p_buttons.size() # Number of buttons unlock by player 1
var butt_ons_p # Variable for creation of buttons
var text_butp # Variable for creation of rich text label node to display button's text
var ar_buttons: Array = [] # Array of visible buttons
var more_but # State variable to create more buttons

func _ready() -> void:
	set_process(true)
	### Calling the function to generate unlock buttons
	if global.ingame == true  :
		global.buttons_loading = true
		_bouton_gen()


func _process(delta) -> void:
	### Triggerring the display of buttons when an answer is right
	if global.good_answers == true :
		_create_buttons()


func _create_buttons() -> void: # Here we create the needed buttons
	more_but = 0
	var temp_unlock_buttons_size: int = global.unlock_buttons.size() # Temp var to control the number of buttons unlock
	var create
	var i: int = 0
	while i < global.unlock_buttons.size() and i < temp_unlock_buttons_size :
		global.button_number_p = global.unlock_buttons[more_but]
		global.num = global.button_p % global.button_number_p
		if global.unlock_buttons[more_but] in global.p_buttons : # We check for buttons already unlocked
			pass
		else:
			### button_9 unlock buttons, we handle it by hand
			if global.unlock_buttons[0] == 20:
				if global.p_clan == "Voider":
					if global.unlock_buttons[more_but] == 20:
						create = false
					else:
						create = true
						global.p_buttons.append(global.unlock_buttons[more_but])
				elif global.p_clan == "Builder":
					if global.unlock_buttons[more_but] == 21:
						create = false
					else:
						create = true
						global.p_buttons.append(global.unlock_buttons[more_but])
				elif global.p_clan == "Thinker":
					if global.unlock_buttons[more_but] == 22:
						create = false
					else:
						create = true
						global.p_buttons.append(global.unlock_buttons[more_but])
			elif global.unlock_buttons[0] == 50 and global.level != 4: # Unlock endgame buttons only if level 4
				temp_unlock_buttons_size -= 5
				create = false
			else: # Normal path
				create = true
				global.p_buttons.append(global.unlock_buttons[more_but]) # Before creating the button, we had it to the unlock buttons array
			if create == true:
				butt_ons_p = quest_ions_p.instance() # Creation of a new instance for every buttons
				add_child(butt_ons_p, true)
				move_child(butt_ons_p, 0)
				ar_buttons.append(butt_ons_p)
				butt_ons_p.connect("button_up", self, "_is_pressed")

			### We unlock the log matching the button of the questinoning player
			if global.current_scene == "player1" :
				rpc_id(global.other_id, "_unlocking_log")
			elif global.offline_mode == true :
				 _unlocking_log()

		more_but += 1
		### We increment the path to buttons to get more buttons using the temp size variable
		if more_but < temp_unlock_buttons_size:
			global.button_number_p = global.unlock_buttons[more_but]
			global.num = global.button_p % global.button_number_p # Updating the global.num variable
		i += 1
	global.good_answers = false 


### Generation of unlock buttons (loading savegame)
func _bouton_gen() -> void:
	global.gen_array = 0
	if global.current_scene == "player1" or global.current_scene == "player_bot":
		global.loading_button_number_p = 0
		var hello
		for i in p_buttons_size :
			butt_ons_p = quest_ions_p.instance() # Creation of a new instance for every buttons
			add_child(butt_ons_p, true)
			move_child(butt_ons_p, 0)
			ar_buttons.append(butt_ons_p)
			butt_ons_p.connect("button_up", self, "_is_pressed")
			if butt_ons_p.get_text() == "Hello, what can I do for you?":
				hello = butt_ons_p
				move_child(hello, 0)
			elif butt_ons_p.get_text() == "": # If a bug create an empty button we delete it
				butt_ons_p.free()


### A question button is pressed...
func _is_pressed() -> void:
	### Who is pressing the button ?
	if global.player_number == 1:
		global.player_asking = 1
	if global.player_number == 2:
		global.player_asking = 2

	if global.current_scene == "player_bot":
		global.question_ask = 1 # Activation of the status : a question is waiting for answer
		global.add_display_question = true
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Wait Please"))
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text")._bot_managing_question()
	else:	
		rpc_id(global.other_id, "_question_ask_check", global.player_asking, global.player_question, global.state_number, global.unlock_buttons, global.button_logs) # Sending question infos to the answerer

		if global.state_number == 0: # Hello question = no answer, but other player is questionning
			global.hello = true
			if global.player_asking == 1 :
				global.player_asking = 2
			else:
				global.player_asking = 1
			get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Wait For A Question"))
		else:
			global.question_ask = 1 # Activation of the status : a question is waiting for answer
			get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Wait For The Answer"))

	global.add_display_question = true


remote func _question_ask_check(player_asking, player_question, question_states ,unlock_buts, button_logs) -> void: # Here we check if the player is in the player_1 scene (loading countdown) must be ended
	global.wait_player = false
	_question_ask(player_asking, player_question, question_states ,unlock_buts, button_logs)


func _question_ask(player_asking, player_question, question_states ,unlock_buts, button_logs) -> void:
	if question_states == 0 && global.hello == false : # Hello question = no answer, but other player is questionning
		global.hello = true
		global.add_display_question = true
		global.player_question = player_question
		global.player_asking = global.player_number
	else:
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text/valid_player_entry")._activated()
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text/entry_text")._activated()
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text/entry_text").clear()
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text/entry_text").set_placeholder("")
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Answer The Question"))
		global.player_question = player_question
		global.question_ask = 1 # Activation of the status : a question is waiting for answer
		global.player_asking = player_asking
		global.state_number = question_states
		global.unlock_buttons = unlock_buts
		global.button_logs = button_logs
		global.add_display_question = true

	global.logs_sound = true # Logs sound trigger


remote func _unlocking_log() -> void:
	if global.logs_unlock.has(global.button_logs) == false: # We check for logs already unlocked
			global.logs_unlock.append(global.button_logs)
			get_node("/root/connection_back/main_menu/loading/gameControl/dials")._log_icon()


func _bot_asking(enter) -> void:
	if enter == 0: # 1 is for the first time, 0 for the reste of the dial
		yield(get_tree().create_timer(1), "timeout")

		### Here we selecte the next question the player doesn't have
		var count = -1
		for i in global.p_buttons.size():
			if count <= global.p_buttons[i]:
				count = global.p_buttons[i]
			elif global.p_buttons[i] == 20:
				count = randi() % 20
			i += 1
		var max_player_button = count+1
		global.player_question = global.bot_button_dict[global.button_p % max_player_button].player_question
		global.player_asking = 2

		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text/valid_player_entry")._activated()
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text/entry_text")._activated()
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/MarginContainer2/VBoxContainer/player_text/entry_text").clear()
	else:
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/HBoxContainer/dial_help").set_text(tr("Choose A Question"))
		global.hello = true
		global.player_asking = 2
		### Here we trigger the first question (hello)
		global.player_question = global.bot_button_dict[global.button_p % 0].player_question
		
		# Bot is turning the player to maker if the right conditions are met
		if global.current_scene == "player_bot":
			if global.level == 3 and global.other_level == 4:
				if 49 in global.p_buttons :
					get_node("/root/connection_back/main_menu/loading/gameControl/dials/")._bot_make_maker()

	global.add_display_question = true
