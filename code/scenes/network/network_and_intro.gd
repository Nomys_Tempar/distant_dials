extends Node

const bg_scene = preload("res://scenes/interfaces/bgControl.tscn")
const no_internet = preload("res://scenes/network/no_internet.tscn")
const connection_scene = "res://scenes/interfaces/connection_screen.tscn"
const offline_choice_scene = "res://scenes/interfaces/offline_choice_scene.tscn"
var cursor_builders = load("res://ui/cursors/cursor-builders.png")
var cursor_builders_I = load("res://ui/cursors/cursor-builders-I.png")
var cursor_thinkers = load("res://ui/cursors/cursor-thinkers.png")
var cursor_thinkers_I = load("res://ui/cursors/cursor-thinkers-I.png")
var cursor_voiders = load("res://ui/cursors/cursor-voiders.png")
var cursor_voiders_I = load("res://ui/cursors/cursor-voiders-I.png")
var first_connection: bool

func _ready() -> void:
	### OS recognition
	if global.dev == false :
		var os = OS.get_name()
		if os == "X11" or os == "Android" or os == "Haiku" or os == "HTML5" :
			global.p_clan = "Voider"
		if os == "Windows" or os == "UWP" :
			global.p_clan = "Builder"
		if os == "iOS" or os == "OSX" :
			global.p_clan = "Thinker"
	### OS recognition

	if global.server == 1:
		_internet_check()
		global.offline_mode = false
	else: 
		_cursor()
		if global.offline_mode == false :
			_internet_check()
		else:
			_setup_clan_and_go()
	_option_load()


func _option_load() -> void: # Here we check the data folder and we load/create the options file
	### Checking if the data directory exists
	var dir = Directory.new()
	if dir.dir_exists("user://data"): pass
	else:
		dir.make_dir("user://data")
	### Checking if the data directory exists

	### Creating options file if it doesn't exists
	var options_file = File.new() 
	if options_file.file_exists("user://data/options.dat"):
		options_file.open("user://data/options.dat", File.READ)
		var current_line = parse_json(options_file.get_as_text())
		### Loading options
		if current_line.has("master_volume") or current_line.has("language") or current_line.has("music_volume"):
			### We apply option file to the buttons and scrollers
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), current_line.master_volume)
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("amb_bus"), current_line.music_volume)
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music_bus"), current_line.music_volume)
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("effect_bus"), current_line.effect_volume)
			global.locale = current_line.language

		TranslationServer.set_locale(global.locale)
		first_connection == current_line.first_connection
		options_file.close()
	else:
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), 2.6)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("amb_bus"), -10)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music_bus"), -10)
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("effect_bus"), 0)
		var first_connection_save = {
		"first_connection" : true
		}
		options_file.open("user://data/options.dat", File.WRITE)
		options_file.store_line(to_json(first_connection_save))
		options_file.close()

		first_connection = true


func _cursor() -> void:
	### Cursor theming
	if global.p_clan == "Voider":
		Input.set_custom_mouse_cursor(cursor_voiders)
		Input.set_custom_mouse_cursor(cursor_voiders_I, Input.CURSOR_IBEAM)
	if global.p_clan == "Builder":
		Input.set_custom_mouse_cursor(cursor_builders)
		Input.set_custom_mouse_cursor(cursor_builders_I, Input.CURSOR_IBEAM)
	if global.p_clan == "Thinker":
		Input.set_custom_mouse_cursor(cursor_thinkers)
		Input.set_custom_mouse_cursor(cursor_thinkers_I, Input.CURSOR_IBEAM)
	### Cursor theming


func _internet_check() -> void: # Here we check if the player has a working internet connection
	var err = 0
	var http = HTTPClient.new() # Create the Client
	err = http.connect_to_host("http://perdu.com", 80) # Connect to host/port

	# Wait until resolved and connected
	while http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING:
		http.poll()
		OS.delay_msec(500)

	if http.get_status() == HTTPClient.STATUS_CONNECTED : # Could connect
		if global.server == 1 :
			_server_data_save("-- Server Connected --")
			get_tree().change_scene(connection_scene)
		else:
			_setup_clan_and_go()
	else : # Could not connect to network
		var noint = no_internet.instance() # Instancing no_internet scene
		add_child(noint)
		var bg = bg_scene.instance() # Adding background on no_internet scene
		get_node("./no_internet_back").add_child(bg)
		get_node("./no_internet_back").move_child(bg, 0)

		### Skinning no_internet scene
		if global.p_clan == "Voider": 
			var bg_sprite = load("res://ui/bg/voiders-bg.jpg") # Background selection
			get_node("./no_internet_back/bgControl/bg").set_texture(bg_sprite)
			get_node("./no_internet_back/bgControl")._window_resize()

			var theme_ = load("res://ui/voiders_theme.tres") # Theme...
			get_node("./no_internet_back/no_internet_text").set_theme(theme_)

		if global.p_clan == "Builder":
			var bg_sprite = load("res://ui/bg/builders-bg.jpg") # Background selection
			get_node("./no_internet_back/bgControl/bg").set_texture(bg_sprite)
			get_node("./no_internet_back/bgControl")._window_resize()

			var theme_ = load("res://ui/builders_theme.tres") # Theme...
			get_node("./no_internet_back/no_internet_text").set_theme(theme_)

		if global.p_clan == "Thinker":
			var bg_sprite = load("res://ui/bg/thinkers-bg.jpg") # Background selection
			get_node("./no_internet_back/bgControl/bg").set_texture(bg_sprite)
			get_node("./no_internet_back/bgControl")._window_resize()

			var theme_ = load("res://ui/thinkers_theme.tres") # Theme...
			get_node("./no_internet_back/no_internet_text").set_theme(theme_)


func _setup_clan_and_go():
	if first_connection == true : # if it's a first connection, play intro
		if global.p_clan == "Builder":
			get_tree().change_scene("intro_builders.tscn")
		if global.p_clan == "Thinker":
			get_tree().change_scene("intro_thinkers.tscn")
		if global.p_clan == "Voider":
			get_tree().change_scene("intro_voiders.tscn")
	else : # if it's not a first connection, skip intro
		get_tree().change_scene(offline_choice_scene)


func _server_data_save(server_log_entry) -> void:
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)

		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry)

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry)
		data_server.close()
