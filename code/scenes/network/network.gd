extends Control

var SERVER_PORT: int = 8996
var MAX_PLAYERS: int = 4000
var SERVER_IP: String
var client_name: String
var client_group

func _ready() -> void:
	if global.offline_mode == false :
		if global.dev == true :
			SERVER_IP = "localhost" # "localhost"= home client for testing
		else:
			SERVER_IP = "localhost" # Prod IP adress to change for it to match your server domain

		if global.server == 1 :
			var peer = NetworkedMultiplayerENet.new()
			peer.create_server(SERVER_PORT, MAX_PLAYERS)
			get_tree().set_network_peer(peer)

		if global.server != 1 :
			var peer = NetworkedMultiplayerENet.new()
			peer.create_client(SERVER_IP, SERVER_PORT)
			get_tree().set_network_peer(peer)
			get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text(tr("Client started. Awaiting connection..."))

		get_tree().connect("connected_to_server", self, "_connected_ok")
		get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
		get_tree().connect("connection_failed", self, "_connected_fail")


func _connected_ok() -> void:
	rpc_id(1,"register_player", get_tree().get_network_unique_id(), global.version)


remote func register_player(id: int, p_version) -> void: # If I'm the server, let the new guy know about existing players
	_server_data_save("Client n°"+str(id)+" connected...", 1)
	if p_version == global.version : # Server checking if player's game version is the same
		rpc_id(id, "_welcome", id)
	else:
		rpc_id(id, "_wrong_version", global.version)


remote func _welcome(id: int) -> void:
	yield(get_tree().create_timer(2,2), "timeout")
	get_node("/root/connection_back/enter_connection")._trigger()
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("///Server Connected///")+"\n"+tr("Welcome, you are ")+str(id))
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/replay_intro").set_disabled(false)
	_option_load()


remote func _wrong_version(current_version) -> void:
	get_node("/root/connection_back/gameControl/MarginContainer2/").set_visible(true)
	get_node("/root/connection_back/gameControl/MarginContainer2/version_panel/version_text").set_text(tr("Wrong Maker Version.")+"\n"+"\n"+tr("Your version: ")+str(global.version)+"\n"+tr("Current version: ")+str(current_version)+"\n"+"\n"+tr("Please, update your Maker."))


func _connected_fail() -> void:
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("///Server Offline///"))
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+"\n"+tr("Please come back later..."))
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/replay_intro").set_disabled(false)


func _player_disconnected(id: int) -> void:
	if get_tree().is_network_server() :
		if global.client_connected > 0:
			### If the client is quiting while he's searching for a dial, then we remove him from the search list
			if global.players_searching.has(id):
				global.players_searching.erase(id)

			### We delete the player from the online_player file
			var online_player = File.new()
			online_player.open("user://data/online_player.dat", File.READ)
			var current_online = parse_json(online_player.get_as_text())

			client_name = current_online[str(id)+"_name"] # Grabbing player_name
			client_group = current_online[str(id)+"_group"] # Grabbing player_group
			
			current_online.erase(str(id)+"_name") # Deleting values
			current_online.erase(str(id)+"_group")

			var online = current_online
			online_player.open("user://data/online_player.dat", File.WRITE)
			online_player.store_line(to_json(online))
			online_player.close()

			### If the client is quiting while he's in the maker section
			var load_temp_groups = File.new()
			if load_temp_groups.file_exists("user://data/temp_groups.dat") == true :
				load_temp_groups.open("user://data/temp_groups.dat", File.READ)
				var current_line = parse_json(load_temp_groups.get_as_text())

				if client_group in current_line :
					var temp_array = current_line[client_group]
#					var i = 0
					var temp_array_size = temp_array.size()
					for i in range (temp_array_size) :
						if temp_array.has(i) && temp_array[i] == id :
							temp_array.remove(i)
							current_line[client_group] = temp_array
							load_temp_groups.open("user://data/temp_groups.dat", File.WRITE)
							load_temp_groups.store_line(to_json(current_line))
							load_temp_groups.close()
						else :
							i += 1
						if current_line[client_group] == null:
							current_line.erase(client_group)

		_server_data_save("Client n°"+str(id)+" disconnected...", 0)


func _option_load() -> void: # Here we check the data folder and we load/create the options file
	### Opening option file
	var options_file = File.new()
	options_file.open("user://data/options.dat", File.READ)
	var current_line = parse_json(options_file.get_as_text())
	### Loading player_name
	if current_line.has("player_name"):
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").set_text(current_line.player_name)
		options_file.close()
	else:
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Please create a new Admin and login..."))


func _server_data_save(server_log_entry, online_players) -> void:
	if online_players == 1:
		global.online_players += 1
	else:
		global.online_players -= 1
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)

		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry+"\n"+"Total players online = "+str(global.online_players))

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry+"\n"+"Total players online = "+str(global.online_players))
		data_server.close()
