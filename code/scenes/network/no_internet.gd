extends Node

func _ready() -> void:
	_theme_selection()
	get_node("no_internet_text").set_text(tr("No internet"))


func _theme_selection() -> void:
	var theme_
	if global.p_clan == "Voider":
		theme_ = load("res://ui/voiders_theme.tres")
	if global.p_clan == "Builder":
		theme_ = load("res://ui/builders_theme.tres")
	if global.p_clan == "Thinker":
		theme_ = load("res://ui/thinkers_theme.tres")
	get_node("no_internet_text").set_theme(theme_)
