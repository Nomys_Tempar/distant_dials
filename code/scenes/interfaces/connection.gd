extends Button

var save_dict: Dictionary
var creation_possible: bool = false

func _ready() -> void:
	connect("pressed", self, "_on_pressed")
	get_node(".").set_text(tr("Create New Admin"))


func _save()-> Dictionary:
	save_dict = {
	"player_name" : global.player_name,
	"player_sex" : global.player_sex,
	"player_environment" : global.player_OS,
	"player_planet" : global.player_planet,
	"player_color" : global.player_color,
	"player_description" : tr("Enter your description"),
	"player_buttons" : global.p_buttons,
	"player_logs" : global.logs_unlock,
	"level" : 1,
	"player_group" : "0",
	"current_topic" : ["null","null","null"],
	"factions" : global.factions,
	"avatar_links" : global.avatar_links
	}
	return save_dict


func _on_pressed() -> void:
	if creation_possible == true :
		_save()
		if global.offline_mode == true :
			_save_game(global.player_name, get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").get_text(), save_dict)
		else:
			rpc_id(1, "_save_game", global.player_name, get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").get_text(), save_dict)

		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").clear()
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").clear()
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+"\n"+tr("Admin accepted.")+"\n"+tr("You can connect now..."))
		get_node("/root/connection_back")._bypass_connection(false)


func _creation_activation() -> void:
	creation_possible = true
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Admin creation status: Available"))
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+"\n"+tr("Please Confirm Admin Creation."))
	get_node(".").set_text(tr("Confirm New Admin"))
	get_node("/root/connection_back/newadmin_verify")._bypass_pressed(true) # Bypass verifying because it's done
	get_node("/root/connection_back")._bypass_connection(true) # Bypass Connection until creation is validate


func _creation_reset() -> void: # Reset admin creation button if the player change text before validating
	creation_possible = false
	get_node(".").set_text(tr("Create New Admin"))


remote func _save_game(player_name, player_pass, save_dict) -> void: # Savegame on the Server
	### Checking if the save directory exist
	var dir = Directory.new()
	if dir.dir_exists("user://save"): pass
	else:
		dir.make_dir("user://save")
	### Checking if the save directory exist

	if global.offline_mode == false:
		var time_ = OS.get_time()
		_server_data_save("Admin "+player_name+" saved at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))
	var save_game = File.new()
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.WRITE, player_pass)
	var node_data:Dictionary = save_dict
	save_game.store_line(to_json(node_data))
	save_game.close()


func _server_data_save(server_log_entry) -> void:
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)
		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry)

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry)
		data_server.close()
