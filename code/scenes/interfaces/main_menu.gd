extends Node

const bg_scene = preload("res://scenes/interfaces/bgControl.tscn")
const dials = preload("res://scenes/dials/loading.tscn")
const logs = preload("res://scenes/logs/logs_scene.tscn")
const admin = preload("res://scenes/interfaces/admin_scene.tscn")
const credits = preload("res://scenes/interfaces/credits.tscn")
const intro_voi = preload("res://scenes/intro/intro_voiders.tscn")
const intro_bui = preload("res://scenes/intro/intro_builders.tscn")
const intro_thi = preload("res://scenes/intro/intro_thinkers.tscn")
const charts = preload("res://scenes/maker/makerz_chart_scene.tscn")

func _ready() -> void:
	global.login_done = true
	if get_tree().is_network_server() : # The server autoconnect to the game...
		_load_dial()
	else: # Else player load the right theme 

		if global.level != 4 or global.offline_mode == true:
			get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/makerz_charts").visible = false
			get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/maker_section").visible = false
		_theme_selection()

		global.current_scene = "main_menu"
		set_process(true)
		get_node("buttons_mainmenu")._scene_change()

		get_node("gameControl/MarginContainer/VBoxContainer2/replay_intro").connect("pressed", self, "_intro")
		get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/credits").connect("pressed", self, "_credits")
		get_node("gameControl/MarginContainer/VBoxContainer2/disconnect").connect("pressed", self, "_disconnect")
		get_node("gameControl/MarginContainer/VBoxContainer2/quit").connect("pressed", self, "_quit")

		get_node("gameControl/MarginContainer/CenterContainer/VBoxContainer/deletion_label").set_text(tr("Warning - Admin will be deleted next shutdown"))
		get_node("gameControl/MarginContainer/CenterContainer/VBoxContainer/deletion_label").visible = false


func _process(delta) -> void:
	if get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/credits").is_hovered():
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + tr("Who made Distant Dials and how.") + "[/center]\n")
	elif get_node("gameControl/MarginContainer/VBoxContainer2/settings").is_hovered(): 
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + tr("Sound and other settings.") + "[/center]\n")
	elif get_node("gameControl/MarginContainer/VBoxContainer2/replay_intro").is_hovered(): 
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + tr("Replay Distant Dials's intro.") + "[/center]\n")
	elif get_node("gameControl/MarginContainer/VBoxContainer2/disconnect").is_hovered(): 
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + tr("Disconnect from ")+global.player_name+tr("'s session.") + "[/center]\n")
	elif get_node("gameControl/MarginContainer/VBoxContainer2/quit").is_hovered(): 
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + tr("Quit Distant Dials.") + "[/center]\n")
	elif global.level == 4 and global.offline_mode == true :
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + (tr("Congrats! You are a Maker now. You have reach the end of the offline mode of Distant Dials. You can keep dialing to get all logs. Thank You for playing!")))
	else:
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()


func _theme_selection() -> void: ### Theme selection
	var bg = bg_scene.instance()
	add_child(bg) # We create the background scene
	move_child(bg, 0) # We move it to be at the top of the scene tree
	var bg_sprite
	var title_sprite
	var theme_

	if global.p_clan == "Voider":
		bg_sprite = load("res://ui/bg/voiders-landscape.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()

		if global.level == 4 :
			theme_ = load("res://ui/voiders_maker_theme.tres") # Theme...
			title_sprite = load("res://ui/titles/makerz-title.png") # Title selection
		else:
			theme_ = load("res://ui/voiders_theme.tres") # Theme...
			title_sprite = load("res://ui/titles/voiders-title.png") # Title selection

		get_node("gameControl/MarginContainer/HBoxContainer/distant_dials_title").set_texture(title_sprite)

	if global.p_clan == "Builder":
		bg_sprite = load("res://ui/bg/builders-landscape.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()
		get_node("gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").add_color_override("default_color", Color(0.89,0.89,0.56,1))

		if global.level == 4 :
			theme_ = load("res://ui/builders_maker_theme.tres") # Theme...
			title_sprite = load("res://ui/titles/makerz-title.png") # Title selection
		else:
			theme_ = load("res://ui/builders_theme.tres") # Theme...
			title_sprite = load("res://ui/titles/builders-title.png") # Title selection

		get_node("gameControl/MarginContainer/HBoxContainer/distant_dials_title").set_texture(title_sprite)

	if global.p_clan == "Thinker":
		bg_sprite = load("res://ui/bg/thinkers-landscape.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()

		if global.level == 4 :
			theme_ = load("res://ui/thinkers_maker_theme.tres") # Theme...
			title_sprite = load("res://ui/titles/makerz-title.png") # Title selection
		else:
			theme_ = load("res://ui/thinkers_theme.tres") # Theme...
			title_sprite = load("res://ui/titles/thinkers-title.png") # Title selection

		get_node("gameControl/MarginContainer/HBoxContainer/distant_dials_title").set_texture(title_sprite)

	get_node("gameControl/MarginContainer/CenterContainer/VBoxContainer/deletion_label").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/dials").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/maker_section").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/admin").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/logs").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/makerz_charts").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer2/settings").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer2/replay_intro").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/credits").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer2/disconnect").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer2/quit").set_theme(theme_)


func _maker_section_exit() -> void:
	global.maker_section = false
	global.current_scene = "main_menu"


func _dial_section_exit() -> void:
	global.current_scene = "main_menu"


func _check_maker() -> void: # Changing the menu if player is now maker
	if global.offline_mode == false :
		get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/maker_section").visible = true
		get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/makerz_charts").visible = true
		rpc_id(1, "_maker_group", get_tree().get_network_unique_id(), global.player_group, global.player_name, global.player_pass)
	else:
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").clear()
		get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + (tr("Congrats! You are a Maker now. You have reach the end of the offline mode of Distant Dials. You can keep dialing to get all logs. Thank You for playing!")))


remote func _maker_group(id: int, player_group, player_name: String, player_pass: String) -> void: # Server give a valid group to the player
	var time_ = OS.get_time()
	var groups_loaded
	var load_groups = File.new()
	if load_groups.file_exists("user://data/maker_groups.dat") == false : pass
	else :
		load_groups.open("user://data/maker_groups.dat", File.READ)
		groups_loaded = parse_json(load_groups.get_as_text())

	if player_group == "0" && load_groups.file_exists("user://data/maker_groups.dat") == false : # If player doesn't have a group and maker_group file doesn't exist...
		groups_loaded = { # Group management
			"1" : [player_name]
			}
		load_groups.open("user://data/maker_groups.dat", File.WRITE)
		load_groups.store_line(to_json(groups_loaded))
		load_groups.close()
		player_group = str(groups_loaded.size())

		var mak_quest = File.new() # Makerz_subject management
		var group_mak = {# Creating a new group with question 1 and time 0
			str(groups_loaded.size()) : [1, 86400] # 1209600 for 2 weeks, 86400 for one day, 20 for testing
			} 
		mak_quest.open("user://data/maker_group_questions.dat", File.WRITE)
		mak_quest.store_line(to_json(group_mak))
		mak_quest.close()

		rpc_id(id, "_joining_group", player_group) # The server send group number to the player

	elif player_group == "0" : # If the player doesn't have a group...
		var maker_data_size = str(groups_loaded.size())
		var maker_string = groups_loaded[maker_data_size]

		if maker_string.size() >= 10: # And the last group is full

			var mak_quest = File.new() # Makerz_subject management
			var group_mak = {# Creating a new group with question 1 and time 0
				str(groups_loaded.size()+1) : [1, 86400] # 1209600 for 2 weeks, 86400 for one day, 20 for testing
				} 
			mak_quest.open("user://data/maker_group_questions.dat", File.WRITE)
			mak_quest.store_line(to_json(group_mak))
			mak_quest.close()

			### Group management
			groups_loaded[str(groups_loaded.size()+1)] = [player_name] # Creating a new group
			load_groups.open("user://data/maker_groups.dat", File.WRITE)
			load_groups.store_line(to_json(groups_loaded))
			load_groups.close()
			player_group = groups_loaded.size()+1

			rpc_id(id, "_joining_group", player_group) # The server send group number to the player

		else: # And the last group is available

			maker_string.append(player_name) # Group management
			groups_loaded[str(groups_loaded.size())] = maker_string

			load_groups.open("user://data/maker_groups.dat", File.WRITE)
			load_groups.store_line(to_json(groups_loaded))
			load_groups.close()
			player_group = groups_loaded.size()

			rpc_id(id, "_joining_group", player_group) # The server send group number to the player

		_server_data_save("Admin group n°"+str(player_group)+" saved at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))

	elif player_group != "0" && groups_loaded[player_group].find(player_name) == -1 :
		print("player fantom in group")
		var maker_data_size = str(groups_loaded.size())
		var maker_string = groups_loaded[maker_data_size]
		load_groups.open("user://data/maker_groups.dat", File.WRITE)
		load_groups.store_line(to_json(groups_loaded))
		load_groups.close()

		maker_string.append(player_name) # Group management
		groups_loaded[str(groups_loaded.size())] = maker_string

		rpc_id(id, "_joining_group", player_group) # The server send group number to the player
	else : pass# If the player have a group...

	### Loading of the savefile
	var save_game = File.new()
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.READ, player_pass)
	var current_line = parse_json(save_game.get_as_text())
	current_line["player_group"] = str(groups_loaded.size())
	### Writing new datas
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.WRITE, player_pass)
	save_game.store_line(to_json(current_line))
	save_game.close()
	_server_data_save("Admin group n°"+str(player_group)+" saved at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))


remote func _joining_group(player_group_number: int) -> void: # The player receive his group number
	global.player_group = str(player_group_number)


### Menu loading functions...
func _load_dial() -> void: # Instancing the loading scene of the dials...
	var loading = dials.instance()
	add_child(loading) 
	if get_tree().is_network_server() == false :
		get_node("bgControl").queue_free()


func _load_admin() -> void: # Player data screen
	var admin_datas = admin.instance()
	add_child(admin_datas)
	get_node("bgControl").queue_free()


func _admin_screen_quit() -> void:
	if global.offline_mode == true:
		_admin_description_save(global.player_name, global.player_description, global.player_pass)
	else:
		rpc_id(1, "_admin_description_save", global.player_name, global.player_description, global.player_pass)


remote func _admin_description_save(player_name: String, new_description, player_pass: String) -> void:
	var save_game = File.new()
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.READ, player_pass)
	var current_line = parse_json(save_game.get_as_text())

	### We delete the old value from the dict
	current_line.erase("player_description") 
	### Adding the new keys and values to the dict
	current_line["player_description"] = new_description

	### Writing new datas
	save_game.open_encrypted_with_pass("user://save/savegame_"+player_name+".dat", File.WRITE, player_pass)
	save_game.store_line(to_json(current_line))
	save_game.close()


func _load_logs() -> void: # Player logs screen
	var player_logs = logs.instance()
	add_child(player_logs)


func _load_charts() -> void:
	rpc_id(1,"_get_charts", get_tree().get_network_unique_id()) # Player recover makerz charts


remote func _get_charts(id: int) -> void: # Server fetching bbcofor the player
	var data_store = File.new()
	if data_store.file_exists("user://data/data_makerz_compilation_file.dat") == false  : # No charts to load
		rpc_id(id, "_receive_charts", "no_charts")
	else:
		data_store.open("user://data/data_makerz_compilation_file.dat", File.READ)
		var charts = parse_json(data_store.get_as_text())
		rpc_id(id, "_receive_charts", charts)


remote func _receive_charts(charts_received) -> void: # Player receive charts
	if typeof(charts_received) != TYPE_DICTIONARY:
		global.charts = false
	else:
		global.charts = charts_received
	var makerz_charts = charts.instance()
	add_child(makerz_charts)


func _intro() -> void:
	var intro_scene
	if global.p_clan == "Voider":
		intro_scene = intro_voi.instance()
	if global.p_clan == "Builder":
		intro_scene = intro_bui.instance()
	if global.p_clan == "Thinker":
		intro_scene = intro_thi.instance()
	add_child(intro_scene)


func _credits() -> void:
	var credits_scene = credits.instance()
	add_child(credits_scene)


func _disconnect() -> void:
	if global.delete == true :
		if global.offline_mode == true:
			_delete_admin(global.player_name)
		else:
			rpc_id(1, "_delete_admin", global.player_name)

		var dir = Directory.new() # Deleting admin avatar
		dir.remove("user://data/"+global.player_name+"_avatar.png")

		get_node("/root/connection_back")._deletion_notice(global.player_name)

		global.delete = false

	yield(get_tree().create_timer(0.2), "timeout")
	get_node("/root/connection_back")._theme_selection()
	global.current_scene = "connection"
	get_node(".").queue_free()


func _quit() -> void:
	if global.delete == true :
		if global.offline_mode == true:
			_delete_admin(global.player_name)
		else:
			rpc_id(1, "_delete_admin", global.player_name)
		global.delete = false

		var dir = Directory.new() # Deleting admin avatar
		dir.remove("user://data/"+global.player_name+"_avatar.png")
		global.delete = false
	else: pass

	yield(get_tree().create_timer(0.2), "timeout")
	get_tree().quit()


func _back_from_option() -> void:
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/dials").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/maker_section").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/admin").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/logs").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/makerz_charts").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer2/settings").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer2/replay_intro").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/credits").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer2/disconnect").set_disabled(false)
	get_node("gameControl/MarginContainer/VBoxContainer2/quit").set_disabled(false)


### Admin Deletion Process
func _delete_admin_verify(admin_pass: String) -> void: # From admin_scene, contacting server to verify password (deleting admin process)
	if global.offline_mode == true:
			_pass_verif_for_deletion(global.player_name, admin_pass, "offline")
	else:
		rpc_id(1, "_pass_verif_for_deletion", global.player_name, admin_pass, get_tree().get_network_unique_id())


remote func _pass_verif_for_deletion(player_name:String, player_pass: String, player_id) -> void: # Server side (password verif)
	var save_game = File.new()
	var player_save = "user://save/savegame_"+player_name+".dat"

	### Load the file line by line and process into the dictionary to check datas
	save_game.open_encrypted_with_pass(player_save, File.READ, player_pass)
	if save_game.is_open() != true :# Here we check if the player's pass matches the one saved
		if player_id is String : # If player_id == "offline"
			_deletion_status(0)
		else:
			rpc_id(player_id, "_deletion_status", 0)
	else : # If the password is the same...
		### ...we send the confirmation to the player.
		if player_id is String : # If player_id == "offline"
			_deletion_status(1)
		else:
			rpc_id(player_id, "_deletion_status", 1)

	save_game.close()


remote func _deletion_status(deletion_bool) -> void: # Player side, deletion_bool = 0 for wrong pass, 1 for deletion confirmed
	get_node("./admin_scene/gameControl/MarginContainer/HBoxContainer/VBoxContainer3/delete_admin")._deletion_status(deletion_bool)
	if deletion_bool == 1:
		get_node("gameControl/MarginContainer/CenterContainer/VBoxContainer/deletion_label").visible = true


remote func _delete_admin(player_name) -> void: # Server delete player's admin
	var dir = Directory.new()
	dir.remove("user://save/savegame_"+player_name+".dat")
	if global.offline_mode == true :
		var player_dialogs = File.new()
		if player_dialogs.file_exists("user://data/dialogs_"+global.player_name+".dat"):
			dir.remove("user://data/dialogs_"+global.player_name+".dat")
### Admin Deletion Process


func _exit_tree():
	global.player_name = ""
	global.player_sex = ""
	global.player_OS = ""
	global.player_planet = ""
	global.player_color = ""
	global.player_description = ""
	global.p_buttons = []
	global.logs_unlock = []
	global.level = 1
	global.factions = []
	global.avatar_links = []


func _server_data_save(server_log_entry) -> void:
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)

		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry)

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry)
		data_server.close()
