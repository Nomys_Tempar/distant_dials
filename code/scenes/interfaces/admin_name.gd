extends Node

const bg_scene = preload("res://scenes/interfaces/bgControl.tscn")


func _ready() -> void:
	_theme_selection()

	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/id_data").add_text(global.player_name)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/sex_data").add_text(tr(global.player_sex))
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/environment_data").add_text(global.player_OS)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/planet_data").add_text(global.player_planet)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/clearance_data").add_text(str(global.level))
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/description_data").set_text(global.player_description)

	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/description_data").set_context_menu_enabled(false)


remote func _receive_description(player_description) -> void:
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/description_data").set_text(player_description)


func _theme_selection() -> void: ### Theme selection
	var bg = bg_scene.instance()
	add_child(bg) # We create the background scene
	move_child(bg, 0) # We move it to be at the top of the scene tree

	var bg_sprite
	var theme_
	if global.p_clan == "Voider":
		bg_sprite = load("res://ui/bg/voiders-bg.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()
		theme_ = load("res://ui/voiders_theme.tres") # Theme...	
	if global.p_clan == "Builder":
		bg_sprite = load("res://ui/bg/builders-bg.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()
		theme_ = load("res://ui/builders_theme.tres") # Theme...	
	if global.p_clan == "Thinker":
		bg_sprite = load("res://ui/bg/thinkers-bg.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()
		theme_ = load("res://ui/thinkers_theme.tres") # Theme...	

	if global.level == 4:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")

	get_node("gameControl/MarginContainer/HBoxContainer/CenterContainer/avatar").set_texture(_load_png("user://data/"+global.player_name+"_avatar.png")) # We load the user avatar with the _load_png func and apply it to the TextureRect
	get_node("gameControl/MarginContainer/HBoxContainer/CenterContainer/avatar_border").color = Color(global.player_color[0], global.player_color[1], global.player_color[2], global.player_color[3])

	get_node("MarginContainer/title").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer2/id").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer2/sex").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer2/environment").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer2/planet").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer2/clearance").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer2/description").set_theme(theme_)

	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/id_data").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/sex_data").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/environment_data").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/planet_data").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/clearance_data").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/description_data").set_theme(theme_)

	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/delete_admin").set_theme(theme_)


func _load_png(file) -> Image: # Loading user external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex


func _exit_tree() -> void:
	global.player_description = get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer3/description_data").get_text()
	get_node("/root/connection_back/main_menu")._admin_screen_quit()
