extends OptionButton

var main_lang

func _ready() -> void:
	if get_node(".").get_name() == "lang_but":
		connect("item_selected", self, "_item_selected_lang")

		### Adding languages into the button 
		get_node(".").add_item("English")
		get_node(".").add_item("Français")

		if global.current_scene == "connection":
			get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/settings/options_screen/options_background")._language_setter()
		else:
			get_node("/root/connection_back/main_menu/gameControl/MarginContainer/VBoxContainer2/settings/options_screen/options_background")._language_setter()

	elif get_node(".").get_name() == "music_but":
		connect("item_selected", self, "_item_selected_music")


func _item_selected_lang(lang_index: int):
	if lang_index == 0:
		global.locale = "en"
	elif lang_index == 1:
		global.locale = "fr"
	TranslationServer.set_locale(global.locale)
