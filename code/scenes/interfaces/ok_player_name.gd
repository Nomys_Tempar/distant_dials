extends Control # Button validates the player registration

var time_zone = OS.get_time_zone_info()
var zone_mod: String # Text replacing the original time_zone.name one
var langage = OS.get_locale()
var langage_mod: String # Text replacing the original langage one
var save_already_exist: bool = false # Server variable to tell if a player already exist or not
var bypass: bool = false
var step1: int
var step2: int
var step3: int

var letters: String = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN1234567890" # Letters to randomize

func _ready() -> void:
	randomize()
	get_node("../gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/admin_creation").connect("pressed", self, "_is_pressed")

	### Color mod
	if global.offline_mode == false :
		step1 = int(str(get_tree().get_network_unique_id()).substr(0,1))*0.1
		step2 = int(str(get_tree().get_network_unique_id()).substr(3,1))*0.1
		step3 = int(str(get_tree().get_network_unique_id()).substr(6,1))*0.1
	else:
		var rng = RandomNumberGenerator.new()
		step1 = rng.randi_range(0,255)
		step2 = rng.randi_range(0,255)
		step3 = rng.randi_range(0,255)

	### Mod the langage text
	langage_mod = langage.replace(langage, (str(langage.hash()).substr(0,3)))+"."+(str(randi()%999))

	### Mod the time_zone.name text
	zone_mod = time_zone.name.insert(1, "-").insert(2, letters.substr((randi()%62),1)).insert(5, letters.substr((randi()%62),1))


func _is_pressed() -> void:
	if bypass == false :
		if get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").get_text() == "" or get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").get_text() == "" :
			get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Name or password missing !"))
			get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Admin creation status: Unavailable"))
		else : # We verify if a player with that name already exist
			if global.offline_mode == false :
				rpc_id(1, "_verif_savename", get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").get_text(), get_tree().get_network_unique_id())
			else:
				_verif_savename(get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").get_text(), "offline")


func _bypass_pressed(by: bool) -> void: # Setup checking admin bypass (true or false)
	bypass = by


remote func _verif_savename(new_player_name, new_player_id):
	### Checking if the save directory exist
	var dir = Directory.new()
	if dir.dir_exists("user://save"):
		pass
	else:
		dir.make_dir("user://save")
	### Checking if the save directory exist

	var save_game = File.new()
	if save_game.file_exists("user://save/savegame_"+new_player_name+".dat"):
		save_already_exist = true
		if new_player_id is String : # If new_player_id == "offline"
			_save_newplayer(save_already_exist)
		else:
			rpc_id(new_player_id, "_save_newplayer", save_already_exist)
	else :
		save_already_exist = false
		if new_player_id is String : # If new_player_id == "offline"
			_save_newplayer(save_already_exist)
		else:
			rpc_id(new_player_id, "_save_newplayer", save_already_exist)


remote func _save_newplayer(already_exist: bool) -> void: # Player receive the info about his name available or not
	save_already_exist = already_exist
	if save_already_exist == true :
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+"\n"+tr("Sorry, an Admin with this name already exist, please enter another name."))
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Admin creation status: Unavailable"))
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").clear()
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").clear()
	else :
		global.login_done = true
		_admin_rpg_sheet_creation()


func _admin_rpg_sheet_creation() -> void: # Player generating is own character datas
	if global.current_time[0] < 12 : # Sex
		global.player_sex = "male"
	elif global.current_time[0] > 12 :
		global.player_sex = "female"
	elif global.current_time[0] == 12 or global.current_time[0] == 0 or global.current_time[0] == 6 or global.current_time[0] == 18:
		global.player_sex = "other"

	global.player_name = get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").get_text() 
	global.player_OS = OS.get_name()
	global.player_planet = zone_mod+str(time_zone.bias)+"-"+langage_mod+" "+(letters.substr((randi()%10),1))
	global.player_color = [step1, step2, step3, 0.7]
	global.player_description = tr("Enter your description")
	global.p_buttons = [0, 1]
	global.level = 1
	if global.p_clan == "Voider":
		global.factions = ["Voider"]
	if global.p_clan == "Builder":
		global.factions = ["Builder"]
	if global.p_clan == "Thinker":
		global.factions = ["Thinker"]

	### Player Avatar Creation
	# Loading the json
	var avatars_parts = File.new()
	avatars_parts.open("res://json/avatars.json", File.READ)
	var current_avatars = parse_json(avatars_parts.get_as_text())
	avatars_parts.close()
	# Randomizing to get the parts
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var body = current_avatars[global.p_clan][global.player_sex]["body"].normal[rng.randi_range(0,4)]
	rng.randomize()
	var eyes = current_avatars[global.p_clan][global.player_sex].eyes[rng.randi_range(0,4)]
	rng.randomize()
	var nose = current_avatars[global.p_clan][global.player_sex].nose[rng.randi_range(0,4)]
	rng.randomize()
	var mouth = current_avatars[global.p_clan][global.player_sex].mouth[rng.randi_range(0,4)]
	rng.randomize()
	var hair = current_avatars[global.p_clan][global.player_sex].hair[rng.randi_range(0,4)]
	rng.randomize()
	var accessorie = current_avatars[global.p_clan][global.player_sex].accessorie[rng.randi_range(0,4)]

	global.avatar_links = [body, eyes, nose, mouth, hair, accessorie]
	# Handling of the avatar parts
	var load_body = load(global.avatar_links[0])
	var load_eyes = load(global.avatar_links[1])
	var load_nose = load(global.avatar_links[2])
	var load_mouth = load(global.avatar_links[3])
	var load_hair = load(global.avatar_links[4])
	var load_accessorie = load(global.avatar_links[5])

	# Creating viewport
	var viewport = Viewport.new()
	viewport.size = Vector2(512, 512)
	viewport.render_target_update_mode =Viewport.UPDATE_ALWAYS
	add_child(viewport)
	viewport.set_vflip(true)
	
	# Create sprites
	var img_body = Sprite.new()
	viewport.add_child(img_body)
	img_body.set_centered(false)
	img_body.set_texture(load_body)
	
	var img_eyes = Sprite.new()
	viewport.add_child(img_eyes)
	img_eyes.set_centered(false)
	img_eyes.set_texture(load_eyes)
	
	var img_nose = Sprite.new()
	viewport.add_child(img_nose)
	img_nose.set_centered(false)
	img_nose.set_texture(load_nose)
	
	var img_mouth = Sprite.new()
	viewport.add_child(img_mouth)
	img_mouth.set_centered(false)
	img_mouth.set_texture(load_mouth)
	
	var img_hair = Sprite.new()
	viewport.add_child(img_hair)
	img_hair.set_centered(false)
	img_hair.set_texture(load_hair)
	
	var img_accessorie = Sprite.new()
	viewport.add_child(img_accessorie)
	img_accessorie.set_centered(false)
	img_accessorie.set_texture(load_accessorie)
	
	# Wait for content
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	
	# Fetch viewport content
	var texture = viewport.get_texture()
	var player_avatar = texture.get_data()
	
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	
	### Saving viewport content
	player_avatar.save_png("user://data/"+global.player_name+"_avatar.png")
	### Player Avatar Creation

	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+"\n"+tr("Name")+global.player_name)
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Time")+str(global.current_time))
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Sex")+global.player_sex)
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Environment")+global.player_OS)
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Planet")+global.player_planet)
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Access Clearance Level")+str(global.level))
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+tr("Color Code")+str(global.player_color))

	get_node("../gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/admin_creation")._creation_activation()
