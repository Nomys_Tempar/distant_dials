extends AudioStreamPlayer

var sound_dict: Dictionary

func _ready() -> void:
	if get_tree().is_network_server(): pass
	else:
		### Opening the json
		var sound_dict_to_load = File.new()
		sound_dict_to_load.open("res://json/sfx.json", sound_dict_to_load.READ)
		var sound_dict_load = sound_dict_to_load.get_as_text()
		sound_dict = parse_json(sound_dict_load)
		sound_dict_to_load.close()


func _enter() -> void:
	if global.p_clan == "Voider" :
		get_node(".").stream = load(sound_dict.voiders_enter[0])
		get_node(".").play()
	elif global.p_clan == "Builder" :
		get_node(".").stream = load(sound_dict.builders_enter[0])
		get_node(".").play()
	elif global.p_clan == "Thinker" :
		get_node(".").stream = load(sound_dict.thinkers_enter[0])
		get_node(".").play()


func _trigger() -> void:
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	if global.p_clan == "Voider" :
		get_node(".").stream = load(sound_dict.voiders_connection[0])
		get_node(".").play()
	elif global.p_clan == "Builder" :
		get_node(".").stream = load(sound_dict.builders_connection[0])
		get_node(".").play()
	elif global.p_clan == "Thinker" :
		get_node(".").stream = load(sound_dict.thinkers_connection[0])
		get_node(".").play()
	elif global.p_clan == "Maker" :
		get_node(".").stream = load(sound_dict.buttontest[rng.randi() % sound_dict.buttontest.size()])
		get_node(".").play()
