extends ReferenceRect

var p_group
var all_charts
var current_question
var size_selected

var unlock # Dials var
var not_unlock
var points_arc_unlock
var color_unlock
var points_arc_else
var color_else

var total_vote #Makerz var
var yes
var no
var complicated

var points_arc_yes
var color_yes

var points_arc_no
var color_no

var points_arc_comp
var color_comp

var center = Vector2(100,100)
var radius = 100

var chart_array # Display var

func _ready() -> void:
	get_node("./yes").visible = false
	get_node("./no").visible = false
	get_node("./complicated").visible = false
	get_node("./purcent").visible = false

	_theme_selection()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.level == 4:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_theme.tres") # Theme...
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_theme.tres") # Theme...
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_theme.tres") # Theme...
	get_node(".").set_theme(theme_)
	get_node("./yes").set_theme(theme_)
	get_node("./no").set_theme(theme_)
	get_node("./complicated").set_theme(theme_)
	get_node("./purcent").set_theme(theme_)


### Dials Charts
func _recover_dials(dials_chart, type: int) -> void: # Type is 0 for buttons unlock and 1 for history logs
	get_node("./yes").visible = false
	get_node("./no").visible = false
	get_node("./complicated").visible = false
	get_node("./purcent").visible = true

	var how_many_unlock = dials_chart.size()
	if type == 0:
		total_vote = 43
	else :
		total_vote = 20
	unlock = (how_many_unlock*100)/total_vote # Calculating percentages
	not_unlock = 100-unlock

	get_node("./purcent").set_text(str(unlock)+"%")
	_draw_dials(unlock)


func _draw_dials(unlock) -> void:
	var nb_points = 32
	points_arc_unlock = PoolVector2Array()
	points_arc_unlock.push_back(Vector2(100, 100))
	color_unlock = PoolColorArray([Color(0.35, 1.0, 0.0)])
	var from_unlock = 0 # Start point of the chart 

	### Calculate angle
	var angle_end_unlock = (unlock*360)/100
	### Calculate angle

	for i in range(nb_points + 1):
		var angle_point = deg2rad(from_unlock + i * (angle_end_unlock - from_unlock) / nb_points - 90)
		points_arc_unlock.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	### Here we place the label near the middle of the arc of the color
	var angle_end = (angle_end_unlock*PI)/180 # Degree to radian
	var from = (from_unlock*PI)/180 # Degree to radian
	var x_from_center = cos(angle_end-((angle_end-from)/2))*radius
	var y_from_center = sin(angle_end-((angle_end-from)/2))*radius
	get_node("./purcent").set_position(Vector2((100+y_from_center),(100+x_from_center)))
	### Here we place the label near the middle of the arc of the color

	_draw_arc_else(angle_end_unlock)


func _draw_arc_else(from_else) -> void:
	var nb_points = 32
	points_arc_else = PoolVector2Array()
	points_arc_else.push_back(Vector2(100, 100))
	color_else = PoolColorArray([Color(0.0, 0.0, 0.0)])

	### Calculate angle
	var angle_end_else = ((not_unlock*360)/100)+from_else
	### Calculate angle

	for i in range(nb_points + 1):
		var angle_point = deg2rad(from_else + i * (angle_end_else - from_else) / nb_points - 90)
		points_arc_else.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	_set_chart_array("dials")


### Makerz Charts
func _recover_values(player_group, charts, question, group_size) -> void:
	get_node("./yes").visible = true
	get_node("./no").visible = true
	get_node("./complicated").visible = true
	get_node("./purcent").visible = false

	### Reset var
	total_vote = 0
	yes = 0
	no = 0
	complicated = 0

	### Datas Acquisition
	p_group = player_group
	all_charts = charts
	current_question = question
	size_selected = group_size

	### Extracting datas from all_charts var
#	var i = 0
	if size_selected == 0: # All
#		var y = 0
		for y in all_charts[current_question].size():
			var cur_quest_ans = all_charts[current_question]
			for i in cur_quest_ans[str(y+1)].size():
				var cur_group = cur_quest_ans[str(y+1)]
#				var u = 0
				for u in cur_group[i].size(): # This for is because answers are arrays
					if cur_group[i][0] == "yes":
						yes += 1
					elif cur_group[i][0] == "no":
						no += 1
					else: # Complicated
						complicated += 1
					u += 1
					total_vote += 1
				i += 1
			y += 1
	else: # Player_group 
		var quest_and_group = all_charts[current_question][p_group]
		for i in quest_and_group.size():
			if quest_and_group[i][0] == "yes":
				yes += 1
			elif quest_and_group[i][0] == "no":
				no += 1
			else: # Complicated
				complicated += 1
			total_vote += 1
			i += 1
	
	if yes == 0:
		get_node("./yes").visible = false
	if no == 0:
		get_node("./no").visible = false
	if complicated == 0:
		get_node("./complicated").visible = false

	_ready_to_display()


func _ready_to_display() -> void:
	_draw_arc_yes() # First portion of the chart


func _draw_arc_yes() -> void: # Prepare drawing yes
	var circ_yes = (yes*100)/total_vote # Calculating percentages

	var nb_points = 32
	points_arc_yes = PoolVector2Array()
	points_arc_yes.push_back(Vector2(100, 100))
	color_yes = PoolColorArray([Color(1.0, 0.0, 0.0)])
	var from_yes = 0 # Start point of the chart 

	### Calculate angle
	var angle_end_yes = (circ_yes*360)/100
	### Calculate angle

	for i in range(nb_points + 1):
		var angle_point = deg2rad(from_yes + i * (angle_end_yes - from_yes) / nb_points - 90)
		points_arc_yes.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	### Here we place the label near the middle of the arc of the color
	var angle_end = (angle_end_yes*PI)/180 # Degree to radian
	var from = (from_yes*PI)/180 # Degree to radian
	var x_from_center = cos(angle_end-((angle_end-from)/2))*radius
	var y_from_center = sin(angle_end-((angle_end-from)/2))*radius
	get_node("./yes").set_position(Vector2((100+y_from_center),(100+x_from_center)))
	### Here we place the label near the middle of the arc of the color

	_draw_arc_no(angle_end_yes)


func _draw_arc_no(from_no) -> void: # Prepare drawing no
	var circ_no = (no*100)/total_vote # Calculating percentages

	var nb_points = 32
	points_arc_no = PoolVector2Array()
	points_arc_no.push_back(Vector2(100, 100))
	color_no = PoolColorArray([Color(0.0, 1.0, 0.0)])

	### Calculate angle
	var angle_end_no = ((circ_no*360)/100)+from_no
	### Calculate angle

	for i in range(nb_points + 1):
		var angle_point = deg2rad(from_no + i * (angle_end_no - from_no) / nb_points - 90)
		points_arc_no.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	### Here we place the label near the middle of the arc of the color
	var angle_end = (angle_end_no*PI)/180 # Degree to radian
	var from = (from_no*PI)/180 # Degree to radian
	var x_from_center = cos(angle_end-((angle_end-from)/2))*radius
	var y_from_center = sin(angle_end-((angle_end-from)/2))*radius
	get_node("./no").set_position(Vector2((100+y_from_center),(100+x_from_center)))
	### Here we place the label near the middle of the arc of the color

	_draw_arc_comp(angle_end_no)


func _draw_arc_comp(from_comp) -> void: # Prepare drawing complicated
	var circ_comp = (complicated*100)/total_vote # Calculating percentages

	var nb_points = 32
	points_arc_comp = PoolVector2Array()
	points_arc_comp.push_back(Vector2(100, 100))
	color_comp = PoolColorArray([Color(0.0, 0.0, 1.0)])

	### Calculate angle
	var angle_end_comp = ((circ_comp*360)/100)+from_comp
	### Calculate angle

	for i in range(nb_points + 1):
		var angle_point = deg2rad(from_comp + i * (angle_end_comp - from_comp) / nb_points - 90)
		points_arc_comp.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

	### Here we place the label near the middle of the arc of the color
	var angle_end = (angle_end_comp*PI)/180 # Degree to radian
	var from = (from_comp*PI)/180 # Degree to radian
	var x_from_center = cos(angle_end-((angle_end-from)/2))*radius
	var y_from_center = sin(angle_end-((angle_end-from)/2))*radius
	get_node("./complicated").set_position(Vector2((100+y_from_center),(100+x_from_center)))
	### Here we place the label near the middle of the arc of the color

	_set_chart_array("makerz")


### Display Charts
func _set_chart_array(type_chart) -> void: # type-chart is "makerz" for topics and "dials" for unlocks
	if type_chart == "dials":
		chart_array = [
			[points_arc_unlock, color_unlock],
			[points_arc_else, color_else],
		]
	else:
		chart_array = [
			[points_arc_yes, color_yes],
			[points_arc_no, color_no],
			[points_arc_comp, color_comp],
		]
	_draw()


func _draw() -> void:
	for i in chart_array:
		draw_polygon(i[0],i[1])
