extends Panel

var hover: String

func _ready() -> void:
	_theme_selection()

	get_node("margin/grid/master_volume_hscrollbar").connect("value_changed", self, "_master")
	get_node("margin/grid/music_volume_hscrollbar").connect("value_changed", self, "_music")
	get_node("margin/grid/ambiance_volume_hscrollbar").connect("value_changed", self, "_amb")
	get_node("margin/grid/effect_volume_hscrollbar").connect("value_changed", self, "_effect")
	get_node("margin/grid/master_volume_hscrollbar").connect("mouse_entered", self, "_master_hovered")
	get_node("margin/grid/music_volume_hscrollbar").connect("mouse_entered", self, "_music_hovered")
	get_node("margin/grid/ambiance_volume_hscrollbar").connect("mouse_entered", self, "_amb_hovered")
	get_node("margin/grid/effect_volume_hscrollbar").connect("mouse_entered", self, "_effect_hovered")

	### We load the option file if it exists
	var options_file = File.new() 
	if options_file.file_exists("user://data/options.dat"):
		options_file.open("user://data/options.dat", File.READ)
		var current_line = parse_json(options_file.get_as_text())

		if current_line.has("master_volume") == false :
			### We delete the old value
			var temp_co = current_line.first_connection
			current_line.erase("first_connection") 

			### And add the new ones
			current_line["first_connection"] = temp_co
			current_line["master_volume"] = get_node("margin/grid/master_volume_hscrollbar").get_value()
			current_line["amb_volume"] = get_node("margin/grid/ambiance_volume_hscrollbar").get_value()
			current_line["music_volume"] = get_node("margin/grid/music_volume_hscrollbar").get_value()
			current_line["effect_volume"] = get_node("margin/grid/effect_volume_hscrollbar").get_value()
			current_line["language"] = global.locale

			options_file.open("user://data/options.dat", File.WRITE)
			options_file.store_line(to_json(current_line))
		else :
			### We apply option file to the buttons and scrollers
			get_node("margin/grid/master_volume_hscrollbar").set_value(current_line.master_volume)
			get_node("margin/grid/ambiance_volume_hscrollbar").set_value(current_line.amb_volume)
			get_node("margin/grid/music_volume_hscrollbar").set_value(current_line.music_volume)
			get_node("margin/grid/effect_volume_hscrollbar").set_value(current_line.effect_volume)
	else : # If the file doesn't exist we create it
		var new_options: Dictionary = {
		"master_volume" : get_node("margin/grid/master_volume_hscrollbar").get_value(),
		"amb_volume" : get_node("margin/grid/ambiance_volume_hscrollbar").get_value(),
		"music_volume" : get_node("margin/grid/music_volume_hscrollbar").get_value(),
		"effect_volume" : get_node("margin/grid/effect_volume_hscrollbar").get_value(),
		"language" : global.locale
		}
		options_file.open("user://data/options.dat", File.WRITE)
		options_file.store_line(to_json(new_options))


func _language_setter() -> void:
	if global.locale == "en":
		get_node("margin/grid/lang_but").select(0)
	elif global.locale == "fr":
		get_node("margin/grid/lang_but").select(1)


func _theme_selection() -> void: ### Theme selection
	var theme_opt
	if global.level == 4:
		if global.p_clan == "Voider":
			theme_opt = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_opt = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_opt = load("res://ui/thinkers_maker_theme.tres")
	else:
		if global.p_clan == "Voider":
			theme_opt = load("res://ui/voiders_theme.tres") # Theme...
		if global.p_clan == "Builder":
			theme_opt = load("res://ui/builders_theme.tres") # Theme...
		if global.p_clan == "Thinker":
			theme_opt = load("res://ui/thinkers_theme.tres") # Theme...
	get_node(".").set_theme(theme_opt)
	get_node("./margin/margin/return_button").set_theme(theme_opt)
	get_node("./margin/grid/master_volume").set_theme(theme_opt)
	get_node("./margin/grid/master_volume_hscrollbar").set_theme(theme_opt)
	get_node("./margin/grid/ambiance_volume").set_theme(theme_opt)
	get_node("./margin/grid/ambiance_volume_hscrollbar").set_theme(theme_opt)
	get_node("./margin/grid/music_volume").set_theme(theme_opt)
	get_node("./margin/grid/music_volume_hscrollbar").set_theme(theme_opt)
	get_node("./margin/grid/effect_volume").set_theme(theme_opt)
	get_node("./margin/grid/effect_volume_hscrollbar").set_theme(theme_opt)
	get_node("./margin/grid/language").set_theme(theme_opt)
	get_node("./margin/grid/lang_but").set_theme(theme_opt)


func _master(value) -> void:
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), value)


func _music(value) -> void:
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music_bus"), value)


func _amb(value) -> void:
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("amb_bus"), value)


func _effect(value) -> void:
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("effect_bus"), value)


### ScrollBars hovered
func _master_hovered() -> void:
	hover = "master"


func _music_hovered() -> void:
	hover = "music"


func _amb_hovered() -> void:
	hover = "amb"


func _effect_hovered() -> void:
	hover = "effect"


func _input(event) -> void: # Mouse wheel & click handling
	if event is InputEventMouseButton && event.is_pressed() :
		if hover == "master":
			if event.button_index == BUTTON_WHEEL_UP:
				get_node("./margin/grid/master_volume_hscrollbar").set_value(get_node("./margin/grid/master_volume_hscrollbar").get_value()+1)
			if event.button_index == BUTTON_WHEEL_DOWN:
				get_node("./margin/grid/master_volume_hscrollbar").set_value(get_node("./margin/grid/master_volume_hscrollbar").get_value()-1)
		elif hover == "music":
			if event.button_index == BUTTON_WHEEL_UP:
				get_node("./margin/grid/music_volume_hscrollbar").set_value(get_node("./margin/grid/music_volume_hscrollbar").get_value()+1)
			if event.button_index == BUTTON_WHEEL_DOWN:
				get_node("./margin/grid/music_volume_hscrollbar").set_value(get_node("./margin/grid/music_volume_hscrollbar").get_value()-1)
		elif hover == "amb":
			if event.button_index == BUTTON_WHEEL_UP:
				get_node("./margin/grid/ambiance_volume_hscrollbar").set_value(get_node("./margin/grid/ambiance_volume_hscrollbar").get_value()+1)
			if event.button_index == BUTTON_WHEEL_DOWN:
				get_node("./margin/grid/ambiance_volume_hscrollbar").set_value(get_node("./margin/grid/ambiance_volume_hscrollbar").get_value()-1)
		elif hover == "effect":
			if event.button_index == BUTTON_WHEEL_UP:
				get_node("./margin/grid/effect_volume_hscrollbar").set_value(get_node("./margin/grid/effect_volume_hscrollbar").get_value()+1)
			if event.button_index == BUTTON_WHEEL_DOWN:
				get_node("./margin/grid/effect_volume_hscrollbar").set_value(get_node("./margin/grid/effect_volume_hscrollbar").get_value()-1)


func _exit_tree() -> void: # Saving the option file with new values
	var options_file = File.new() 
	options_file.open("user://data/options.dat", File.READ_WRITE)
	var current_line = parse_json(options_file.get_as_text())
	### We delete the old value
	var temp_co = current_line.first_connection
	current_line.erase("first_connection")
	current_line.erase("master_volume")
	current_line.erase("amb_volume")
	current_line.erase("music_volume")
	current_line.erase("effect_volume")
	current_line.erase("language")

	### And add the new ones
	current_line["first_connection"] = temp_co
	current_line["master_volume"] = get_node("margin/grid/master_volume_hscrollbar").get_value()
	current_line["amb_volume"] = get_node("margin/grid/ambiance_volume_hscrollbar").get_value()
	current_line["music_volume"] = get_node("margin/grid/music_volume_hscrollbar").get_value()
	current_line["effect_volume"] = get_node("margin/grid/effect_volume_hscrollbar").get_value()
	current_line["language"] = global.locale

	options_file.open("user://data/options.dat", File.WRITE)
	options_file.store_line(to_json(current_line))
	options_file.close()
