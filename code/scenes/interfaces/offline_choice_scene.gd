extends Control

const connection_scene = "res://scenes/interfaces/connection_screen.tscn"

# Called when the node enters the scene tree for the first time.
func _ready():
		_theme_selection()
		$CenterContainer/Panel/VBoxContainer/RichTextLabel.add_text(tr("Distant Dials's servers are offlines."))


func _theme_selection() -> void:
	var theme_
	
	if global.p_clan == "Voider":
		theme_ = load("res://ui/voiders_theme.tres") # Theme...	
	if global.p_clan == "Builder":
		theme_ = load("res://ui/builders_theme.tres") # Theme...	
	if global.p_clan == "Thinker":
		theme_ = load("res://ui/thinkers_theme.tres") # Theme...	

	if global.level == 4:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")

	get_node("CenterContainer/Panel").set_theme(theme_)
	get_node("CenterContainer/Panel/VBoxContainer/RichTextLabel").set_theme(theme_)
	get_node("CenterContainer/Panel/VBoxContainer/HBoxContainer/offline_mode").set_theme(theme_)
	get_node("CenterContainer/Panel/VBoxContainer/HBoxContainer/online_mode").set_theme(theme_)


func _on_offline_mode_button_down():
	global.offline_mode = true

	## Saving choice in option file
	var options_file = File.new()
	options_file.open("user://data/options.dat", File.READ)
	var current_line = parse_json(options_file.get_as_text())

	current_line.erase("offline_mode")
	current_line["offline_mode"] = true
	### Writing new datas
	options_file.open("user://data/options.dat", File.WRITE)
	options_file.store_line(to_json(current_line))
	options_file.close()

	get_tree().change_scene(connection_scene)


func _on_online_mode_button_down():
	global.offline_mode = false

	## Saving choice in option file
	var options_file = File.new()
	options_file.open("user://data/options.dat", File.READ)
	var current_line = parse_json(options_file.get_as_text())

	current_line.erase("offline_mode")
	current_line["offline_mode"] = true
	### Writing new datas
	options_file.open("user://data/options.dat", File.WRITE)
	options_file.store_line(to_json(current_line))
	options_file.close()

	get_tree().change_scene(connection_scene)
