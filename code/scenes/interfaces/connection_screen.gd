extends Node

const bg_scene = preload("res://scenes/interfaces/bgControl.tscn")
const menu = preload("res://scenes/interfaces/main_menu.tscn")
var bypass: bool = false
var main_menu: bool = false

func _ready():
	set_process(true)

	if get_tree().is_network_server() : # The server autoconnect
		global.p_buttons = [0]
		_main_menu()
	else: # Else player load the right theme 
		_theme_selection()
		get_node("enter_connection")._enter()

	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").connect("text_changed", self, "_pname_change")
	get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").connect("text_changed", self, "_pname_change")
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/quit").connect("pressed", self, "_quit")
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/replay_intro").connect("pressed", self, "_replay_intro")
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/replay_intro").set_disabled(false)
	get_node("/root/connection_back/gameControl/MarginContainer2").set_visible(false)

	global.current_scene = "connection"

	### Audio triggers
	if get_tree().is_network_server(): pass
	else:
		get_node("keys_connection")._key_change()
		get_node("buttons_connection")._scene_change()

	if global.offline_mode == true :
		$gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream.add_text(tr("Welcome in OffLine Mode.")+"\n"+tr("Please connect or create a new admin."))


func _theme_selection() -> void: ### Theme selection
	main_menu = false

	var bg = bg_scene.instance()
	add_child(bg)
	move_child(bg, 0)

	var bg_sprite
	var title_sprite
	var theme_

	if global.p_clan == "Voider":
		bg_sprite = load("res://ui/bg/voiders-landscape.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()

		title_sprite = load("res://ui/titles/voiders-title.png") # Title selection
		get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/distant_dials_title").set_texture(title_sprite)

		theme_ = load("res://ui/voiders_theme.tres") # Theme...
	if global.p_clan == "Builder":
		bg_sprite = load("res://ui/bg/builders-landscape.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()

		title_sprite = load("res://ui/titles/builders-title.png") # Title selection
		get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/distant_dials_title").set_texture(title_sprite)

		theme_ = load("res://ui/builders_theme.tres") # Theme...
	if global.p_clan == "Thinker":
		bg_sprite = load("res://ui/bg/thinkers-landscape.jpg") # Background selection
		get_node("bgControl/bg").set_texture(bg_sprite)
		get_node("bgControl")._window_resize()

		title_sprite = load("res://ui/titles/thinkers-title.png") # Title selection
		get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/distant_dials_title").set_texture(title_sprite)

		theme_ = load("res://ui/thinkers_theme.tres") # Theme...

	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/connection").set_theme(theme_)
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/admin_creation").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/settings").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/quit").set_theme(theme_)
	get_node("gameControl/MarginContainer/VBoxContainer/replay_intro").set_theme(theme_)
	get_node("gameControl/MarginContainer2/version_panel").set_theme(theme_)
	get_node("gameControl/MarginContainer2/version_panel/version_text").set_theme(theme_)


func _process(delta) -> void:
	### Management of buttons activation related to fields content
	if get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").get_text() != "" or get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").get_text() != "" :
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/admin_creation").set_disabled(false)
		if bypass == false :
			get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/connection").set_disabled(false)
		else :
			get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/connection").set_disabled(true)
	else :
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/admin_creation").set_disabled(true)
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/connection").set_disabled(true)


func _pname_change(new_text) -> void:
	if bypass == true :
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/admin_creation")._creation_reset()
		get_node("newadmin_verify")._bypass_pressed(false)


func _bypass_connection(by) -> void: # Bypass Connection until creation is validate
	bypass = by


func _main_menu() -> void: # Instancing the main_menu when the time comes...
	if main_menu == false :
		var main_menu_scene = menu.instance()
		add_child(main_menu_scene)
		main_menu = true
		if get_tree().is_network_server() == false :
			get_node("bgControl").queue_free()


func _deletion_notice(player_name) -> void:
	get_node("gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/live_stream").add_text("\n"+"\n"+tr("Admin ")+player_name+tr(" deleted..."))


func _quit() -> void:
	yield(get_tree().create_timer(0.2), "timeout")
	get_tree().quit()


func _replay_intro() -> void:
	if global.p_clan == "Builder":
		get_tree().change_scene("res://scenes/intro/intro_builders.tscn")
	if global.p_clan == "Thinker":
		get_tree().change_scene("res://scenes/intro/intro_thinkers.tscn")
	if global.p_clan == "Voider":
		get_tree().change_scene("res://scenes/intro/intro_voiders.tscn")


func _back_from_option() -> void:
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/settings").set_disabled(false)
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/quit").set_disabled(false)
	get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/replay_intro").set_disabled(false)
