extends LineEdit

const placeholder: String = ""
var text_lenght_old
var delete_or_not
var tex
var holder_typing: int = 0

func _ready() -> void:
	set_process(true)
	set_max_length(99)
	get_node(".")._desactivated()
	get_node(".").clear()
	get_node(".").set_editable(false)
	connect("text_changed", get_node("."), "_key_pressed")


func _process(delta) -> void:
	if global.focus == 1 :
		get_node(".").grab_focus()


func _activated() -> void:
	get_node(".").set_selecting_enabled(true)
	get_node(".").set_shortcut_keys_enabled(true)
	get_node(".").set_editable(true)
	get_node(".").grab_focus()
	get_node(".").set_default_cursor_shape(1)
	get_node(".").modulate = Color(1,1,1,1)


func _desactivated() -> void:
	get_node(".").set_selecting_enabled(false)
	get_node(".").set_shortcut_keys_enabled(false)
	get_node(".").set_editable(false)
	get_node(".").release_focus()
	get_node(".").set_default_cursor_shape(0)
	get_node("../valid_player_entry").set_focus_mode(true)
	get_node("../valid_player_entry").grab_focus()
	get_node(".").modulate = Color(0.5,0.5,0.5,0.5)


func _key_pressed(tex) -> void: # Func triggering visible dot when other player is typing
	if tex == "" : pass# We disable this if no text is enter
	else:
		if holder_typing >= 3:
			holder_typing = 0
		else:
			holder_typing += 1
		get_node("/root/connection_back/main_menu/loading/gameControl/dials")._key_pressed_on_dials(holder_typing)
