extends AudioStreamPlayer

var sound_dict: Dictionary
var maker_music
var previous_music
var once: bool = false
var vol

func _ready() -> void:
	set_process(true)

	if get_tree().is_network_server() :
		pass
	else:
		### Opening the json
		var sound_dict_to_load = File.new()
		sound_dict_to_load.open("res://json/sfx.json", sound_dict_to_load.READ)
		var sound_dict_load = sound_dict_to_load.get_as_text()
		sound_dict = parse_json(sound_dict_load)
		sound_dict_to_load.close()

		if global.level == 4 and get_node(".").get_name() == "music" :
			get_node(".").connect("finished", self, "_finished")
			_play_radio()
		else:
			if global.p_clan == "Voider" and global.level != 4:
				get_node(".").stream = load(sound_dict.voiders_amb[0])
				get_node(".").stream.loop = true
				get_node(".").play()

			elif global.p_clan == "Builder" and global.level != 4:
				get_node(".").stream = load(sound_dict.builders_amb[0])
				get_node(".").stream.loop = true
				get_node(".").play()

			elif global.p_clan == "Thinker" and global.level != 4:
				get_node(".").stream = load(sound_dict.thinkers_amb[0])
				get_node(".").stream.loop = true
				get_node(".").play()
			else:
				get_node(".").connect("finished", self, "_finished")
				_play_default()


func _process(delta) -> void:
	if get_node(".").get_name() == "music" :
		vol = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("music_bus"))
		if global.level == 4 :
			if global.current_scene == "credits" and once == false:
				_credits_song()
				once = true
			elif global.current_scene == "main_menu" and once == true:
				AudioServer.set_bus_bypass_effects(AudioServer.get_bus_index("music_bus"), false) # Bring effects back
				AudioServer.set_bus_volume_db(AudioServer.get_bus_index("music_bus"), vol)
				_play_radio()
				once = false
		else:
			if global.current_scene == "credits" and once == false:
				get_node(".").stream_paused = false
				_credits_song()
				once = true
			elif global.current_scene == "main_menu" and once == true:
				get_node(".").stream_paused = true
				AudioServer.set_bus_bypass_effects(AudioServer.get_bus_index("music_bus"), false) # Bring effects back
				once = false

	else:
		vol = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("amb_bus"))
		if global.current_scene == "credits" and once == false:
			get_node(".").stream_paused = true
			once = true
		elif global.current_scene == "main_menu" and once == true:
			once = false
			get_node(".").stream_paused = false
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("amb_bus"), vol)
			if global.p_clan == "Voider" and global.level != 4:
				get_node(".").stream = load(sound_dict.voiders_amb[0])
				get_node(".").stream.loop = true
				get_node(".").play()

			elif global.p_clan == "Builder" and global.level != 4 :
				get_node(".").stream = load(sound_dict.builders_amb[0])
				get_node(".").stream.loop = true
				get_node(".").play()

			elif global.p_clan == "Thinker" and global.level != 4 :
				get_node(".").stream = load(sound_dict.thinkers_amb[0])
				get_node(".").stream.loop = true
				get_node(".").play()
			else:
				_play_default()


func _credits_song() -> void:
	AudioServer.set_bus_bypass_effects(AudioServer.get_bus_index("music_bus"), true) # Bypass effects for credit song
	get_node(".").stream = load(sound_dict.credits[0])
	get_node(".").play()


func _play_radio() -> void:
	var rng = RandomNumberGenerator.new()
	rng.randomize()

	maker_music = [load(sound_dict.makerz_amb[0]), load(sound_dict.makerz_amb[1]), load(sound_dict.makerz_amb[2]), load(sound_dict.makerz_amb[3]), load(sound_dict.makerz_amb[4]), load(sound_dict.makerz_amb[5]), load(sound_dict.makerz_amb[6]), load(sound_dict.makerz_amb[7]), load(sound_dict.makerz_amb[8]), load(sound_dict.makerz_amb[9])]

	previous_music = rng.randi() % maker_music.size()
	get_node(".").stream = maker_music[previous_music]
	get_node(".").stream.loop = false
	get_node(".").play()


func _play_default() -> void:
	var rng = RandomNumberGenerator.new()
	rng.randomize()

	maker_music = [load(sound_dict.voiders_amb[0]), load(sound_dict.builders_amb[0]), load(sound_dict.thinkers_amb[0])]

	previous_music = rng.randi() % maker_music.size()
	get_node(".").stream = maker_music[previous_music]
	get_node(".").stream.loop = false
	get_node(".").play()


func _finished() -> void:
	var rng = RandomNumberGenerator.new()
	rng.randomize()

	var i = rng.randi() % maker_music.size()

	while i == previous_music :
		i = rng.randi() % maker_music.size()

	get_node(".").stream = maker_music[i]
	get_node(".").stream.loop = false
	previous_music = i
	get_node(".").play()
