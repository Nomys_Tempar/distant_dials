extends Node

# this script scale the 4K background node according to screen size

onready var viewport = get_viewport()
onready var bg = get_node("bg")

func _ready() -> void:
	# detect window and resize
	viewport.connect("size_changed", self, "_window_resize")


# here we try to avoid black borders by scaling the background. Ideally we would use a formula to have the minimun width or height to fit the window scale. Help welcome !
func _window_resize() -> void:
	var bg_size = bg.texture.get_size()
	var current_size = OS.get_window_size()

	var current_scale = Vector2(current_size.x/bg_size.x, current_size.y/bg_size.y)
	var scalefact_x: float = 0.46875
	var scalefact_y: float = 0.5

	var max_scale = max( current_scale.x, current_scale.y )
	var new_scale = Vector2(max_scale,max_scale)

	if current_scale.y < scalefact_y :
		new_scale = Vector2(max_scale/0.75,max_scale/0.75)
	if current_scale.y < 0.375 :
		new_scale = Vector2(max_scale/0.5,max_scale/0.5)
	if current_scale.y < 0.25 :
		new_scale = Vector2(max_scale/0.25,max_scale/0.25)
	if current_scale.x < scalefact_x:
		new_scale = Vector2(max_scale/0.75,max_scale/0.75)
	if current_scale.x < 0.375:
		new_scale = Vector2(max_scale/0.5,max_scale/0.5)
	if current_scale.x < 0.25:
		new_scale = Vector2(max_scale/0.3,max_scale/0.3)
	if current_scale.x < 0.15:
		new_scale = Vector2(max_scale/0.2,max_scale/0.2)
	if current_scale.x < 0.11:
		new_scale = Vector2(max_scale/0.15,max_scale/0.15)
	if current_scale.x < 0.08:
		new_scale = Vector2(max_scale/0.1,max_scale/0.1)

	bg.set_scale(new_scale)
