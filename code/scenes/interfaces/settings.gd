extends Button

const options = preload("res://scenes/interfaces/options.tscn")

func _ready() -> void:
	connect("pressed", self, "_on_pressed")


func _on_pressed() -> void:
	_load_options()


func _load_options() -> void: # Options screen
	var op_tions = options.instance()
	add_child(op_tions)
	if global.current_scene == "connection" :
		get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/settings").set_disabled(true)
		get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/quit").set_disabled(true)
		get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/replay_intro").set_disabled(true)
		global.current_scene = "options_conn"
	else :
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/dials").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/maker_section").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/admin").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/logs").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/makerz_charts").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer2/settings").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer2/replay_intro").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/credits").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer2/disconnect").set_disabled(true)
		get_node(".").get_owner().get_node("gameControl/MarginContainer/VBoxContainer2/quit").set_disabled(true)
		global.current_scene = "options_main"
