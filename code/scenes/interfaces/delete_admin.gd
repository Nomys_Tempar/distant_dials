extends Button

const delete_scene = preload("res://scenes/interfaces/delete_scene.tscn")

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	get_node(".").set_text(tr("Delete Admin"))
	connect("button_down", self, "_on_pressed")


func _on_pressed() -> void:
	var del = delete_scene.instance()
	get_node("../../..").add_child(del) # We create the admin deletion scene

	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/info_panel").set_text(tr("To permanently delete this admin and all datas,")+"\n"+tr("please enter admin's password and confirm.")+"\n"+tr("Be carreful! This action cannot be undone..."))
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_confirm").set_text(tr("Password confirmed! Admin will be deleted next shutdown."))
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_confirm").visible = false
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_to_delete").set_placeholder(tr("Admin Password"))
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/delete_valid").set_text(tr("Erase Admin"))
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/cancel").set_text(tr("Cancel"))

	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/delete_valid").connect("button_down", self, "_on_delete_pressed")
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/cancel").connect("button_down", self, "_on_cancel")

	_theme_selection()


func _theme_selection() -> void:
	var theme_

	if global.p_clan == "Voider":
		theme_ = load("res://ui/voiders_theme.tres") # Theme...	
	if global.p_clan == "Builder":
		theme_ = load("res://ui/builders_theme.tres") # Theme...	
	if global.p_clan == "Thinker":
		theme_ = load("res://ui/thinkers_theme.tres") # Theme...	

	if global.level == 4:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")

	get_node("../../../delete_scene/Panel").set_theme(theme_)
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/info_panel").set_theme(theme_)
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_confirm").set_theme(theme_)
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_to_delete").set_theme(theme_)
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/delete_valid").set_theme(theme_)
	get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/cancel").set_theme(theme_)


func _on_delete_pressed() -> void:
	var admin_pass = get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_to_delete").get_text()
	get_node("/root/connection_back/main_menu")._delete_admin_verify(admin_pass)


func _deletion_status(deletion_bool: int) -> void: # Player side, deletion_bool = 0 for wrong pass, 1 for deletion confirmed
	if deletion_bool == 0:
		get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_to_delete").clear()
		get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_to_delete").set_placeholder("Wrong Password!")
	else:
		get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/info_panel").visible = false
		get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_to_delete").visible = false
		get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/delete_valid").visible = false
		get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/pass_confirm").visible = true
		get_node("../../../delete_scene/Panel/CenterContainer/VBoxContainer/cancel").visible = false
		global.delete = true


func _on_cancel() -> void:
	get_node("../../../delete_scene").queue_free()
