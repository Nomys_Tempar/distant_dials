extends Node

onready var text_anim = get_node("gameControl/MarginContainer/credits/TextAnimationPlayer")

func _ready() -> void:
	global.current_scene = "credits"
	get_node("bgControl")._window_resize()
	get_node("gameControl/MarginContainer3/replay").connect("pressed", self, "_on_replay_pressed")
	get_node("gameControl/MarginContainer/credits").set_text(tr("credits text"))

	_theme_selection()


func _on_replay_pressed() -> void:
	text_anim.stop()
	text_anim.seek(2)
	text_anim.play("introtext")
	get_node("/root/connection_back/main_menu/music")._credits_song()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.level == 4:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
	else:
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_theme.tres") # Theme...
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_theme.tres") # Theme...
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_theme.tres") # Theme...

	get_node("gameControl/MarginContainer3/replay").set_theme(theme_)

