extends Node
 
var display_text: Array = [] # Text array 

func _ready() -> void:
	if get_tree().is_network_server() :
		pass
	else:
		_theme_selection()

	set_process(true)
	get_node("MarginContainer/display").clear()
	get_node("MarginContainer/display").set_scroll_follow(true)


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.p_clan == "Voider":
		theme_ = load("res://ui/voiders_maker_theme.tres")
	if global.p_clan == "Builder":
		theme_ = load("res://ui/builders_maker_theme.tres")
	if global.p_clan == "Thinker":
		theme_ = load("res://ui/thinkers_maker_theme.tres")
	get_node("MarginContainer/display").set_theme(theme_)
	get_node("HBoxContainer/VBoxContainer/makers_online").set_theme(theme_)
	get_node("HBoxContainer/VBoxContainer/maker_list").set_theme(theme_)


func _process(delta) -> void:
	if global.add_display_answer == true :
		_display_maker_text()


### Makers Func
func _display_maker_text() -> void:
	display_text.append(global.maker_entry)
	global.dials_logs = display_text
	get_node("MarginContainer/display").add_text(global.maker_entry)
	global.add_display_answer = false
