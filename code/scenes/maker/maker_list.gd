extends ItemList

### The server extracts datas...
func _get_init_makers_data(player_id: int, player_name: String, player_group) -> void: 
	# Player's group members
	var load_groups = File.new()
	load_groups.open("user://data/maker_groups.dat", load_groups.READ)
	var groups_to_load = load_groups.get_as_text()
	load_groups.close()
	var groups_loaded = parse_json(groups_to_load)

	### Displaying player's group on maker_list (offline stage)
	var group = groups_loaded[str(player_group)]
	var time_ = OS.get_time()
	_server_data_save("group n°"+str(player_group)+" initializing at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))
	yield(get_tree().create_timer(0.5), "timeout")
	rpc_id(player_id, "_maker_group_initializing", group) # Server send datas back to the player asking


remote func _maker_group_initializing(group) -> void: # Player receive datas and process them to initialise the itemlist
	### Processing default players list
	get_node(".").clear()

	for i in range(group.size()) :
		get_node(".").add_item(group[i])
		get_node(".").set_item_disabled(i, true)

		### Set avatars of makerz from pre-saved avatars
		if group[i] == global.player_name: # Current player
			set_item_icon(i, _load_png("user://data/"+global.player_name+"_avatar.png")) # We load the user avatar with the _load_png func and apply it to the TextureRect
		else:  # Other players
			if File.new().file_exists("user://data/"+group[i]+"_makerz_avatar.png"):
				set_item_icon(i, _load_png("user://data/"+group[i]+"_makerz_avatar.png")) # We load the others avatars with the _load_png func and apply it to the TextureRect
		i += 1


### This func is triggered by player_makerz.gd, a new maker is joining or quitting the chan
func _status_online() -> void:
	rpc_id(1, "_get_update_makers_datas", get_tree().get_network_unique_id(), global.player_group) # Asking the server to send new datas about online makers


remote func _get_update_makers_datas(id: int, player_group) -> void: # Server searching and processing for datas
	var temp_load_groups = File.new() # Loading player's temp_group, display online member of the group
	temp_load_groups.open("user://data/temp_groups.dat", temp_load_groups.READ)
	var temp_groups_to_load = temp_load_groups.get_as_text()
	temp_load_groups.close()
	var temp_groups_loaded = parse_json(temp_groups_to_load)	
	### Displaying player's group on maker_list (online members)
	var temp_online_group = temp_groups_loaded[player_group]

	var total_online = File.new() # Loading player's ingame datas
	total_online.open("user://data/online_player.dat", total_online.READ)
	var temp_total_online = total_online.get_as_text()
	total_online.close()
	var current_ingames = parse_json(temp_total_online)

	### We compare IDs in temp_online_group and in current_ingames, and if a match is found it goes into makers_in_makersection
	var makers_in_makersection = []
	for i in range(temp_online_group.size()) :
		var x = ((str(temp_online_group[i][0]))+"_name")
		if x in current_ingames:
			makers_in_makersection.append([temp_online_group[i][0], current_ingames[x]])
			i += 1
		else :
			i += 1
	rpc_id(id, "_updating_makers_presenses", makers_in_makersection, temp_online_group) # Server send back present players to the maker asking...


remote func _updating_makers_presenses(makers_in_makersection, temp_online_group) -> void: # The maker update his online list
	### Reset itemlist status
	for y in range(get_item_count()) :
		set_item_disabled(y, true)

	### Activating online players
	var count_players = 0 # Count online player to display a message if the player is alone

	for x in range(get_node(".").get_item_count()) :
		set_item_tooltip_enabled(x, true)
		if global.player_name == get_node(".").get_item_text(x):
			set_item_disabled(x, false)
			for i in temp_online_group.size():
				if temp_online_group[i][0] == get_tree().get_network_unique_id():
					set_item_tooltip(x, global.player_name+":"+"\n"+"\n"+temp_online_group[i][2])
					count_players += 1
		else:
			for i in range(makers_in_makersection.size()) :
				if makers_in_makersection[i][1] == get_node(".").get_item_text(x):
					_gen_maker_avatars(makers_in_makersection[i], temp_online_group) # Generating/updating makerz avatars
					yield(get_tree(), "idle_frame")
					yield(get_tree(), "idle_frame")
					set_item_icon(x, _load_png("user://data/"+makers_in_makersection[i][1]+"_avatar.png"))
					get_node(".").set_item_disabled(x, false)
					count_players += 1

					for z in temp_online_group.size():
						if temp_online_group[z][0] == makers_in_makersection[i][0]:
							set_item_tooltip(x, makers_in_makersection[i][1]+":"+"\n"+"\n"+temp_online_group[z][2])
				i += 1
		x += 1

	if count_players == 1:
		get_node(".").owner.get_node("./MarginContainer/display").add_text("\n"+"\n"+tr("You are alone for now, you should wait for someone to join you !"))


func _gen_maker_avatars(one_maker_in_makersection, temp_online_group) -> void: # Generating/updating makerz avatars
	for i in temp_online_group.size():
		if temp_online_group[i][0] == one_maker_in_makersection[0]:
			_gen_process(one_maker_in_makersection, temp_online_group[i])
		else:
			i += 1


func _gen_process(one_maker_in_makersection, online_maker) -> void: # Here we create the maker's avatar locally
	# Getting the parts
	var body = online_maker[1][0]
	var eyes = online_maker[1][1]
	var nose = online_maker[1][2]
	var mouth = online_maker[1][3]
	var hair = online_maker[1][4]
	var accessorie = online_maker[1][5]

	# Creating viewport
	var viewport = Viewport.new()
	viewport.size = Vector2(512, 512)
	viewport.render_target_update_mode =Viewport.UPDATE_ALWAYS
	add_child(viewport)
	viewport.set_vflip(true)

	# Create sprites
	var img_body = Sprite.new()
	viewport.add_child(img_body)
	img_body.set_centered(false)
	img_body.set_texture(load(body))

	var img_eyes = Sprite.new()
	viewport.add_child(img_eyes)
	img_eyes.set_centered(false)
	img_eyes.set_texture(load(eyes))

	var img_nose = Sprite.new()
	viewport.add_child(img_nose)
	img_nose.set_centered(false)
	img_nose.set_texture(load(nose))

	var img_mouth = Sprite.new()
	viewport.add_child(img_mouth)
	img_mouth.set_centered(false)
	img_mouth.set_texture(load(mouth))

	var img_hair = Sprite.new()
	viewport.add_child(img_hair)
	img_hair.set_centered(false)
	img_hair.set_texture(load(hair))

	var img_accessorie = Sprite.new()
	viewport.add_child(img_accessorie)
	img_accessorie.set_centered(false)
	img_accessorie.set_texture(load (accessorie))

	# Wait for content
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	# Fetch viewport content
	var texture = viewport.get_texture()
	var avatar = texture.get_data()

	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	### Saving viewport content
	avatar.save_png("user://data/"+one_maker_in_makersection[1]+"_makerz_avatar.png")
	### Player Avatar Creation

	viewport.free()


func _load_png(file): # Loading user external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex


func _server_data_save(server_log_entry) -> void:
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)

		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry)

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry)
		data_server.close()
