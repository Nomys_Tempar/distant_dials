extends Node

const chart_rect = preload("res://scenes/interfaces/charts_display_scene.tscn")
var charts_keys # Array of keys of the charts dictionary (global.charts)
var once = false

func _ready() -> void:
	global.current_scene = "makerz_charts"
	get_node("charts_buttons")._scene_change()

	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").connect("item_selected", self, "_item_selected_question")
	get_node("Panel/MarginContainer/MarginContainer/group_size").connect("item_selected", self, "_item_selected_group")
	get_node("Panel/MarginContainer/MarginContainer/group_size").set_disabled(true)
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").visible = true
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/display").set_text(tr("Select A Topic"))

	_theme_selection()
	_load_questions()
	_load_group_size()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.p_clan == "Voider":
		theme_ = load("res://ui/voiders_maker_theme.tres")
	if global.p_clan == "Builder":
		theme_ = load("res://ui/builders_maker_theme.tres")
	if global.p_clan == "Thinker":
		theme_ = load("res://ui/thinkers_maker_theme.tres")

	get_node("Panel").set_theme(theme_)
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/display").set_theme(theme_)
	get_node("Panel/MarginContainer/MarginContainer/group_size").set_theme(theme_)  
	get_node("Panel/MarginContainer/title_chart").set_theme(theme_)
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").set_theme(theme_)
	get_node("Panel/MarginContainer/return_button").set_theme(theme_) 


func _load_questions() -> void:
	# Adding dials items
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").add_item(tr("Dials Switches"))
	get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").add_item(tr("History Logs"))

	# Adding Makerz items
	if typeof(global.charts) == TYPE_DICTIONARY:
		charts_keys = global.charts.keys()
		for i in global.charts.size():
			get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").add_item(charts_keys[i])


func _load_group_size() -> void:
	get_node("Panel/MarginContainer/MarginContainer/group_size").add_item(tr("Total"))
	get_node("Panel/MarginContainer/MarginContainer/group_size").add_item(tr("Your Group"))

	if global.player_group == "0":
		get_node("Panel/MarginContainer/MarginContainer/group_size").set_item_disabled(1, true)


func _item_selected_question(question_selected) -> void:
	if has_node("Panel/MarginContainer/MarginContainer/charts_display") == true: # If a chart is displayed
			get_node("Panel/MarginContainer/MarginContainer/charts_display").free()

	if question_selected == 0 or question_selected == 1 : # Dials and logs charts
		get_node("Panel/MarginContainer/MarginContainer/group_size").set_disabled(true)

		var load_chart = chart_rect.instance()
		get_node("Panel/MarginContainer/MarginContainer/").add_child(load_chart)

		if question_selected == 0:
			get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/display").set_text(tr("Switches Unlock In Dials"))
			get_node("Panel/MarginContainer/MarginContainer/charts_display/chart_rect")._recover_dials(global.p_buttons, 0) # 0 for buttons unlock chart
		elif question_selected == 1:
			get_node("Panel/MarginContainer/MarginContainer/charts_display/chart_rect")._recover_dials(global.logs_unlock, 1) # 1 for history logs unlock chart
	else:  # Makerz charts
		get_node("Panel/MarginContainer/MarginContainer/group_size").set_disabled(false)
		once = true

		get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/display").set_text(get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").get_item_text(question_selected))

		var load_chart = chart_rect.instance()
		get_node("Panel/MarginContainer/MarginContainer/").add_child(load_chart)

		### Sending player_group, charts, question selected and group_size selected to the charts_display_scene
		get_node("Panel/MarginContainer/MarginContainer/charts_display/chart_rect")._recover_values(global.player_group, global.charts, get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").get_item_text(question_selected), get_node("Panel/MarginContainer/MarginContainer/group_size").get_selected_id())


func _item_selected_group(group) -> void:
	if once == true:
		if has_node("Panel/MarginContainer/MarginContainer/charts_display") == true:
			get_node("Panel/MarginContainer/MarginContainer/charts_display").free()

		var question_selected = get_node("./Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").get_selected_items()

		var load_chart = chart_rect.instance()
		get_node("Panel/MarginContainer/MarginContainer/").add_child(load_chart)

		### Sending player_group, charts, question selected and group_size selected to the charts_display_scene
		get_node("Panel/MarginContainer/MarginContainer/charts_display/chart_rect")._recover_values(global.player_group, global.charts, get_node("Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").get_item_text(question_selected[0]), get_node("Panel/MarginContainer/MarginContainer/group_size").get_selected_id())
