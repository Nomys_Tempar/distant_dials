extends Node

const maker_text_scene = preload("res://scenes/maker/maker_text_scene.tscn")
const maker_counter = preload("res://scenes/maker/maker_counter.tscn")
const maker_display = preload("res://scenes/maker/display_makerz.tscn")

var groups = global.player_id_in_group
var temp_timer_other # Temp timer to let the player catch on when the question change without him in game 
var player_pic # String with the path of the player's picture

func _ready() -> void:
	var display = maker_display.instance()
	get_node("MarginContainer").add_child(display)

	get_node("/root/connection_back/main_menu/loading/gameControl/display_scene/MarginContainer").visible = false

	global.current_scene = "makerz"

	if get_tree().get_network_unique_id() == 1 :
		var load_temp_groups = File.new() 
		if load_temp_groups.file_exists("user://data/temp_groups.dat"):
			load_temp_groups.open("user://data/temp_groups.dat", File.READ)
			var current_line = parse_json(load_temp_groups.get_as_text())
			current_line = ""
			load_temp_groups.open("user://data/temp_groups.dat", File.WRITE)
			load_temp_groups.store_line(to_json(current_line))
			load_temp_groups.close()
		var counter_topic = maker_counter.instance() # Adding counter scene
		add_child(counter_topic)
	else : 
		_theme_selection()
		var maker_text = maker_text_scene.instance()
		add_child(maker_text)
		if str(global.current_subject[1]) == "over":
			### Checking new subjects
			var questions = File.new() # Looking for new questions in the question's JSON
			questions.open("res://json/makerz_subjects_en.json", File.READ)
			var all_questions = parse_json(questions.get_as_text())
			if all_questions.size() >= global.current_subject[0]: # We load the new question if any
				var sub_num = global.current_subject[0]
				global.current_subject[0] = ("q"+str(sub_num))
				global.current_subject[1] = all_questions[global.current_subject[0]].question
				global.current_subject[2] = all_questions[global.current_subject[0]].type

		get_node("MarginContainer/title").set_text(tr("Welcome to the Makerz Section"))
		rpc_id(1, "_maker_to_server", get_tree().get_network_unique_id(), global.player_name, global.player_group, global.current_subject, global.avatar_links, global.player_description)

	set_process(true)
	get_node("buttons_player1")._scene_change() # Update audio button node


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.p_clan == "Voider":
		theme_ = load("res://ui/voiders_maker_theme.tres")
	if global.p_clan == "Builder":
		theme_ = load("res://ui/builders_maker_theme.tres")
	if global.p_clan == "Thinker":
		theme_ = load("res://ui/thinkers_maker_theme.tres")

	get_node("HBoxContainer/VBoxContainer/quit").set_theme(theme_)
	get_node("MarginContainer/title").set_theme(theme_) 

	get_node("HBoxContainer/VBoxContainer/CenterContainer/avatar").set_texture(_load_png("user://data/"+global.player_name+"_avatar.png")) # We load the user avatar with the _load_png func and apply it to the TextureRect
	get_node("HBoxContainer/VBoxContainer/CenterContainer/avatar_border").color = Color(global.player_color[0], global.player_color[1], global.player_color[2], global.player_color[3])


func _load_png(file): # Loading user external image func
	var png_file = File.new()
	png_file.open(file, File.READ)
	var bytes = png_file.get_buffer(png_file.get_len())
	var img = Image.new()
	var data = img.load_png_from_buffer(bytes)
	var imgtex = ImageTexture.new()
	imgtex.create_from_image(img)
	png_file.close()
	return imgtex
	
#### Maker Func
remote func _maker_to_server(player_id: int, player_name: String, player_group, player_subject, player_avatar, player_description) -> void: # Server receive player infos...
	var someone_else = false # If true request timer from another player not the server
	_server_data_save("Maker n°"+str(player_id)+" named "+player_name+" is member of the group n°"+player_group)

	# Checking temporary group existence...
	var load_temp_groups = File.new()
	if load_temp_groups.file_exists("user://data/temp_groups.dat") == false :
		var first_group: Dictionary = {
			str(player_group) : []
			}
		load_temp_groups.open("user://data/temp_groups.dat", File.WRITE)
		load_temp_groups.store_line(to_json(first_group))

	load_temp_groups.open("user://data/temp_groups.dat", File.READ)
	var current_line = parse_json(load_temp_groups.get_as_text())
	load_temp_groups.close()

	if current_line is String: pass
	elif str(player_group) in current_line:
		groups = current_line[str(player_group)]

	if groups != null : # If temporary groups are already created...
		load_temp_groups.open("user://data/temp_groups.dat", File.READ)
		current_line = parse_json(load_temp_groups.get_as_text())

		### Server checks if the player's group already exist
		if player_group in current_line : # If player group exist, we had the player's ID to the group
			if current_line[player_group].empty() == true: pass
			else:
				if current_line[player_group][0][0] != player_id:
					someone_else = true

			var player_ar = [player_id, player_avatar, player_description] # This is the array for temp player datas
			var group_array = current_line[player_group]
			group_array.append(player_ar)

			### We delete the old group from the dict and replace it by the new one
			current_line.erase(player_group) 
			current_line[player_group] = group_array

			load_temp_groups.open("user://data/temp_groups.dat", File.WRITE)
			load_temp_groups.store_line(to_json(current_line))
			load_temp_groups.close()

			_set_counter_data(player_id, player_group, player_subject, someone_else)
			yield(get_tree().create_timer(0.5), "timeout")
			_start_talking_to(player_id, player_name, current_line[player_group])

		else : # If player group doesn't exist, we create it and add the player's ID
			var player_ar = [player_id, player_avatar, player_description] # This is the array for temp player datas
			current_line[player_group] = [player_ar]

			load_temp_groups.open("user://data/temp_groups.dat", File.WRITE)
			load_temp_groups.store_line(to_json(current_line))
			load_temp_groups.close()

			_set_counter_data(player_id, player_group, player_subject, someone_else)
			yield(get_tree().create_timer(0.5), "timeout")
			_start_talking_to(player_id, player_name, current_line[player_group])

	else :  # If not, we create the group and add the player to it
		var player_ar: Array = [player_id, player_avatar, player_description] # This is the array for temp player datas
		groups = {
			str(player_group) : [player_ar]
			}

		load_temp_groups.open("user://data/temp_groups.dat", File.WRITE_READ)
		load_temp_groups.store_line(to_json(groups))
		current_line = parse_json(load_temp_groups.get_as_text())
		load_temp_groups.close()

		_set_counter_data(player_id, player_group, player_subject, someone_else)
		yield(get_tree().create_timer(0.5), "timeout")
		_start_talking_to(player_id, player_name, current_line[player_group])


func _set_counter_data(player_id: int, player_group, player_subject, someone_else) -> void: # Server sets maker counter and current question (current_subject)
	var player_is_late: bool = false # Control var for player voting late

	var group_data_quest = File.new() # Looking for the current group question & time left
	group_data_quest.open("user://data/maker_group_questions.dat", File.READ) # Once all verifications are done, we retrieve the values
	var current_question_status = parse_json(group_data_quest.get_as_text())
	var quest_datas = current_question_status[player_group]
	var current_timer = current_question_status[player_group][1]

	var questions = File.new() # Looking for the question in the question's JSON
	questions.open("res://json/makerz_subjects_"+global.locale+".json", File.READ)
	var all_questions = parse_json(questions.get_as_text())

	var current_quest
	if player_subject[0] is String : # If player_subject[0] is "null", or "qN"
		if player_subject[0] == "null": # Player is the first
			if quest_datas[0] <= all_questions.size(): # When a new player log in
				current_quest = ["q"+str(quest_datas[0]), all_questions["q"+str(quest_datas[0])].question, all_questions["q"+str(quest_datas[0])].type]
			else: # When a new player reach a "over" group
				current_quest = [quest_datas[0], "over", "over"]
		else: # "qN"/normal behaviour
			if player_subject[0] is String and player_subject[0] != ("q"+str(quest_datas[0])): # If not, the player didn't vote for his current question.
				player_is_late = true
			else : # Player is up to date
				if current_timer is String: # If a new question appears we reset current_timer
					current_timer = int(86400)
			current_quest = player_subject
	else: # If player_subject[0] is int (pass the last question and is last question+1)
		if player_subject[0] > all_questions.size(): # Confirmation
			current_quest = [player_subject[0], "over", "over"]
			current_timer = "over"

	### So we set the counter to 0 to launch the maker_topic_ended scene right away...
	if player_is_late == true:
		current_timer = 0

	var relaunch_counter: bool = false
	if someone_else == true and player_is_late == false: 
			var load_temp_groups = File.new()
			load_temp_groups.open("user://data/temp_groups.dat", File.READ)
			var current_line = parse_json(load_temp_groups.get_as_text())

			var group_array = current_line[player_group]
			rpc_id(int(group_array[0][0]), "_get_time", current_quest, current_timer, relaunch_counter, player_id)
	else:
		rpc_id(player_id, "_quest_data", current_quest, current_timer, relaunch_counter) # Sending question and timer to new player


### Grabbing time for newcomer
remote func _get_time(current_quest, current_timer, relaunch_counter, other_id) -> void:
	if str(current_quest[1]) != "over":
		current_timer = get_node("maker_counter/VBoxContainer/counter").get_text()
		rpc_id(other_id, "_quest_data", current_quest, current_timer, relaunch_counter)
	else:
		current_timer = "over"
		rpc_id(other_id, "_quest_data", current_quest, current_timer, relaunch_counter)
### Grabbing time for newcomer


func _start_talking_to(player_id, player_name, current_group) : # Server launch the player online notification (makerz side)
	for i in range (current_group.size()):
		rpc_id(int(current_group[i][0]), "_player_online_notif_and_receiving_name", player_name, current_group) # Dispatch player arrival to everyone
		i += 1


remote func _quest_data(current_quest, current_timer, relaunch_counter) -> void: # New player receive question and timer
	global.current_topic_time = current_timer
	global.current_subject = current_quest

	var counter_topic = maker_counter.instance() # Adding counter scene
	add_child(counter_topic)

	var over: bool
	if str(global.current_topic_time) == "over": # No new subject
		over = true
		get_node("maker_counter")._over_set_accum("None")
	else:
		over = false
		if relaunch_counter == true : # Checking time and relaunching if needed
			if typeof(global.current_topic_time) != TYPE_STRING:
				global.current_topic_time = temp_timer_other

		get_node("maker_counter")._start_counter(get_tree().get_network_unique_id())
		relaunch_counter = false
	if over == true:
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").add_text("\n"+tr("Current Room topic: No new topic, you can discuss freely!"))
	else:
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").add_text("\n"+tr("Current Room topic: ")+global.current_subject[1])


remote func _player_online_notif_and_receiving_name(new_player, current_group) -> void: # Every players in the group receive the player online notification
	global.player_id_in_group = current_group # Updating current temporary group
	while get_node("maker_counter/VBoxContainer/counter") == null:
		yield(get_tree().create_timer(0.5), "timeout")
	get_node("maker_counter")._start_counter(global.player_id_in_group.back()[0])
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").add_text("\n"+new_player+tr(" is Online..."))
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/HBoxContainer/VBoxContainer/maker_list")._status_online()


func _sending_answer_data(answer_data, player_group) -> void: # Function trigger by maker_topic_ended scene
	get_node("maker_counter").queue_free() # Killing counter
	rpc_id(1, "_makerz_data_responses_update", get_tree().get_network_unique_id(), answer_data, global.current_subject, player_group)


remote func _makerz_data_responses_update(player_id: int, answer_data, current_subject, player_group) -> void: # Server function to save maker's answer to topics and trigger the next topic
	var data_store = File.new()
	if data_store.file_exists("user://data/data_makerz_compilation_file.dat") == false  : # If answer compilation file doesn't exist
		data_store.open("user://data/data_makerz_compilation_file.dat", File.WRITE)

		var new_q_gr_dat = {str(player_group) : [answer_data]}
		var new_data_text: Dictionary = {
			current_subject[1] : new_q_gr_dat
			}

		data_store.open("user://data/data_makerz_compilation_file.dat", File.WRITE)
		data_store.store_line(to_json(new_data_text))
		data_store.close()

	else : # If answer compilation file exist
		data_store.open("user://data/data_makerz_compilation_file.dat", File.READ)
		var data_in_text = parse_json(data_store.get_as_text())

		var y = 0
		if data_in_text.has(current_subject[1]) != true : # We check if the topic exist
			var new_q_gr_dat: Dictionary = {
				str(player_group) : [answer_data]
				}
			data_in_text[current_subject[1]] = new_q_gr_dat

		else : # If the topic exist in the file
			if data_in_text[current_subject[1]].has(player_group) != true : # We check if the group exist
				var p_group = str(player_group)
				data_in_text[current_subject[1].p_group] = [answer_data]
			else : # If the group exist...
				var p_group = str(player_group)
				var topic = data_in_text[current_subject[1]]

				topic[p_group].append(answer_data)

		data_store.open("user://data/data_makerz_compilation_file.dat", File.WRITE)
		data_store.store_line(to_json(data_in_text))
		data_store.close()

	_change_topic(player_id, current_subject, player_group)


func _change_topic(player_id: int, current_subject, player_group) -> void: # Server maker function to update topic and counter
	var player_is_late: bool = false # Control var for player voting late

	### Updating maker_group_questions.data
	var group_data_quest = File.new() 
	group_data_quest.open("user://data/maker_group_questions.dat", File.READ)
	var current_question_status = parse_json(group_data_quest.get_as_text())

	var new_q = int(current_subject[0].substr(1))

	var p_group_status = current_question_status[str(player_group)]

	var questions = File.new() # Looking for the question in the question's JSON
	questions.open("res://json/makerz_subjects_en.json", File.READ)
	var all_questions = parse_json(questions.get_as_text())

	var over: bool
	if all_questions.size() == int(p_group_status[0]): # Here we verify if the group is in the last question available
		new_q += 1
		over = true
	else: # If not we check if the player is late
		over = false
		if p_group_status[0] == (new_q+1) or p_group_status[0] == new_q :
			new_q += 1 
		else :
			new_q = p_group_status[0]
			player_is_late = true

	p_group_status.remove(0)
	p_group_status.push_front(new_q)

	var current_timer
	if player_is_late == true :
		var load_temp_groups = File.new()
		load_temp_groups.open("user://data/temp_groups.dat", File.READ)
		var temp_group = parse_json(load_temp_groups.get_as_text())

		if temp_group.has(str(player_group)) == true :
			var time_contact = temp_group[str(player_group)]
			rpc_id(int(time_contact[0][0]), "_get_current_timer_topic", player_id)
			current_timer = "null"
		else :
			current_timer = p_group_status[1]
	elif over == true: pass
	else:
		p_group_status.remove(1)
		p_group_status.push_back(86400)
		current_timer = 86400

	group_data_quest.open("user://data/maker_group_questions.dat", File.WRITE)
	group_data_quest.store_line(to_json(current_question_status))
	group_data_quest.close()

	### Updating player current_subject
	var relaunch_counter: bool
	if all_questions.size() < new_q: # If all questions have been answer
		current_subject = [new_q,"over","over"]
		relaunch_counter = false
		current_timer = "over"
	else:
		current_subject[0] = ("q"+str(new_q))
		current_subject[1] = all_questions[current_subject[0]].question
		current_subject[2] = all_questions[current_subject[0]].type
		relaunch_counter = true

	rpc_id(player_id, "_quest_data" ,current_subject, current_timer, relaunch_counter) # Then server send it back to quest data to update the player


### Grabbing time when topic is changed
remote func _get_current_timer_topic(other_id: int) -> void: # Function to grab the current accum time (in maker_counter)
	var current_timer = get_node("maker_counter/VBoxContainer/counter").get_text()
	rpc_id(other_id, "_grab_current_time_topic", current_timer) # Sending it to the needy player


remote func _grab_current_time_topic(current_accum) -> void: # Needy player receive the current timer
	temp_timer_other = current_accum
### Grabbing time when topic is changed


func _exit_tree() -> void: # When player is quiting maker section, we advice the server
	if get_tree().get_network_unique_id() != 1 && global.maker_section == true:
		rpc_id(1, "_maker_exit", get_tree().get_network_unique_id(), global.player_name, global.player_group, global.current_topic_time)#, global.player_id_in_group)


remote func _maker_exit(id: int, player_name: String, player_group, new_current_timer) -> void: # The server delete the player from the temp_group array in the temp_group dictionary
	var temp_groups_obsolete = File.new()
	temp_groups_obsolete.open("user://data/temp_groups.dat", File.READ)
	var old_group = parse_json(temp_groups_obsolete.get_as_text())
	var temp_array = old_group[player_group]
#	var i = 0
	for i in range(temp_array.size()) :
		if temp_array[i][0] == id :
			var time_ = OS.get_time()
			_server_data_save(player_name+" remove from group at "+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second))

			var new_array = temp_array.duplicate(true)

			new_array.remove(i)

			var temp_groups_obsolete_change = File.new()
			temp_groups_obsolete_change.open("user://data/temp_groups.dat", File.READ)
			var new_group = parse_json(temp_groups_obsolete_change.get_as_text())
			new_group[player_group] = new_array

			temp_groups_obsolete_change.open("user://data/temp_groups.dat", File.WRITE)
			temp_groups_obsolete_change.store_line(to_json(new_group))
			temp_groups_obsolete_change.close()

			if new_array == []: # If the player leaving was the last, we save the counter as well

				var group_data_quest = File.new() # Looking for the current group question & time left
				group_data_quest.open("user://data/maker_group_questions.dat", File.READ)
				var current_question_status = parse_json(group_data_quest.get_as_text())

				current_question_status[player_group].remove(1)
				current_question_status[player_group].append(new_current_timer)

				group_data_quest.open("user://data/maker_group_questions.dat", File.WRITE)
				group_data_quest.store_line(to_json(current_question_status))
				group_data_quest.close()
		else:
			var time_ = OS.get_time()
			_server_data_save(player_name+"'s quit notification send at"+str(time_.hour)+":"+str(time_.minute)+":"+str(time_.second)+" to other players in the group")
			rpc_id(int(temp_array[i][0]), "_maker_quit_notif", player_name) # Notication for other players that someone is quiting maker session
		i += 1


remote func _maker_quit_notif(other_name: String) -> void: 
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").add_text("\n"+other_name+tr(" Quit..."))
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/HBoxContainer/VBoxContainer/maker_list")._status_online()


func _notification(quit) -> void:
	if quit == MainLoop.NOTIFICATION_WM_QUIT_REQUEST and get_tree().get_network_unique_id() != 1:
		rpc_id(1, "_maker_exit", get_tree().get_network_unique_id(), global.player_name, global.player_group, global.current_topic_time)#, global.player_id_in_group)
		get_tree().quit()


func _server_data_save(server_log_entry) -> void:
	var date = OS.get_date()
	var data_server = File.new()
	if data_server.file_exists("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat") == true  : 
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.READ)

		var server_logs_moredata = ((data_server.get_as_text())+server_log_entry)

		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_logs_moredata)
		data_server.close()
	else :
		data_server.open("user://data/data_server_"+str(date.day)+"_"+str(date.month)+"_"+str(date.year)+".dat", File.WRITE)
		data_server.store_line(server_log_entry)
		data_server.close()
