extends LineEdit

var m_entry
var maker_entry

func _ready() -> void:
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/maker_text_scene/MarginContainer/HBoxContainer/maker_valid_entry").connect("pressed", self, "_on_pressed")
	get_node(".").grab_focus()


func _on_pressed() -> void: ### Retrieving player's text
	if get_node(".").get_text() == "": pass
	else :
		m_entry = get_node(".").get_text()
		get_node(".").clear()
		maker_entry = str("\n"+global.player_name+" : "+m_entry)

		var y = 0
		for i in global.player_id_in_group.size() :
			if get_tree().get_network_unique_id() == (int(global.player_id_in_group[y][0])) :
				global.maker_entry = maker_entry
				global.add_display_answer = true
				i += 1
				y += 1
			else :
				rpc_id(int(global.player_id_in_group[y][0]), "_sending_maker_text", maker_entry) # We send the text to other players
				i += 1
				y += 1


remote func _sending_maker_text(maker_entry) -> void:
	global.maker_entry = maker_entry
	global.add_display_answer = true
