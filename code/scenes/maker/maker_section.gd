extends Button

func _ready() -> void:
	set_process(true)
	connect("pressed", self, "_on_pressed")


func _process(delta) -> void:
	if is_hovered() :
		get_node(".").owner.get_node("./gameControl/MarginContainer/CenterContainer/VBoxContainer/menu_logs").append_bbcode("[center]" + tr("Here, you can talk about the past society with other Makers.") + "[/center]\n")


func _on_pressed() -> void:
	global.maker_section = true
	get_node("/root/connection_back/main_menu")._load_dial()
