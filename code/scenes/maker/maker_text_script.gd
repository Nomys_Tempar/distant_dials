extends Node

const help_instance = preload("res://scenes/maker/maker_help.tscn")
var help

func _ready() -> void:
	get_node("MarginContainer2/VBoxContainer/what").connect("pressed", self, "_is_pressed")
	get_node("MarginContainer2/VBoxContainer/what/whatLabel").set_text(tr("What am I doing here?"))
	_theme_selection()


func _theme_selection() -> void: ### Theme selection
	var theme_
	var just_text_theme_ = load("res://ui/buttons/just_text_maker.tres")
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
		get_node("MarginContainer2/VBoxContainer/what").set_theme(theme_)
		get_node("MarginContainer/HBoxContainer/marker_entry").set_theme(theme_)
		get_node("MarginContainer/HBoxContainer/maker_valid_entry").set_theme(theme_) 


func _is_pressed() -> void:
	if global.dev == false: pass
	else :
		get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/maker_counter")._shorten_counter() # Testing func
	help = help_instance.instance()
	add_child(help)
