extends Control # Counter for the maker session

const topic_ended = preload("res://scenes/maker/maker_topic_ended.tscn")
var accum: int
var timer: bool = false
var stop_counter: bool = false 
var form_popup

func _ready() -> void:
	set_process(true)
	if get_tree().is_network_server(): pass
	else :
		if global.current_topic_time is int or global.current_topic_time is float :
			accum = int(global.current_topic_time)
		elif global.current_topic_time is String :
			accum = global.current_topic_time
		_theme_selection()


func _process(delta) -> void:
	if timer == false: pass
	elif stop_counter == false:
		if global.current_topic_time is int or global.current_topic_time is float :
			accum -= delta
			get_node("VBoxContainer/counter").set_text(str(int(accum)))

			if accum <= 0:
				_topic_ended()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
		get_node("VBoxContainer/counter").set_theme(theme_)
		get_node("VBoxContainer/topic_time_left").set_theme(theme_) 


remote func _new_maker(new_id: int, new_accum: int) -> void: # Server send the current time to the new player
	rpc_id(new_id,"_accum_set", new_accum)


remote func _accum_set(nowish_timer) -> void: # New player retrieve current time
	if nowish_timer is String:
		global.current_topic_time = nowish_timer
	else:
		global.current_topic_time = int(nowish_timer)
	accum = global.current_topic_time


func _start_counter(new_id: int) -> void:
	if timer == false :
		timer = true
	else: # Existing players send the current time to the server
		if accum != null:
			if global.current_topic_time is String:
				rpc_id(1, "_new_maker", new_id, "-")
			else:
				rpc_id(1, "_new_maker", new_id, accum)
		else: # In case the only available player is in the answering form
			_start_counter(new_id)


func _timer_set() -> void:
	accum = int(global.current_topic_time)


func _over_set_accum(new_time) -> void:
	if new_time == "-":
		get_node("VBoxContainer/counter").set_text(str(new_time))


func _topic_ended() -> void:
	stop_counter = true
	form_popup = topic_ended.instance()
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz").add_child(form_popup)


func _exit_tree() -> void:
	if get_tree().get_network_unique_id() != 1:
		global.current_topic_time = accum


func _shorten_counter() -> void: # For testing purpose, allow to reduce counter time (global.dev)
	accum = int(20)
