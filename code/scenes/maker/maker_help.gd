extends MarginContainer

func _ready() -> void:
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").visible = false

	### We set the help screen 
	# Text label
	get_node("MarginContainer/Panel/MarginContainer/MarginContainer/help_text").set_text(tr("Welcome into The Makerz Section!"))
	# Quit button
	get_node("MarginContainer/Panel/MarginContainer/return_button")._help()
	get_node("MarginContainer/Panel/MarginContainer/return_button")._set_text()

	_theme_selection()


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
		get_node("MarginContainer/Panel").set_theme(theme_)
		get_node("MarginContainer/Panel/MarginContainer/MarginContainer/help_text").set_theme(theme_)
		get_node("MarginContainer/Panel/MarginContainer/return_button").set_theme(theme_) 


func _exit_tree() -> void:
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").visible = true
