extends MarginContainer

var answer
var data_ans

func _ready() -> void:
	set_process(true)
	_theme_selection()

	get_node("topic_end_panel/MarginContainer/VBoxContainer/send").connect("pressed", self, "_is_pressed")
	get_node("topic_end_panel/MarginContainer/VBoxContainer/send").set_disabled(true)

	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").visible = false
	get_node("topic_end_panel/MarginContainer/VBoxContainer/explanations").visible = false

	get_node("topic_end_panel/MarginContainer/VBoxContainer/ended_text").set_text("\n"+str(global.player_name)+tr("! It's time to make up your mind")+"\n"+tr("The question was: ")+str(global.current_subject[1])+"\n"+"\n"+tr("What do you think?"))


func _theme_selection() -> void: ### Theme selection
	var theme_
	if global.maker_section == true : # Maker in maker section
		if global.p_clan == "Voider":
			theme_ = load("res://ui/voiders_maker_theme.tres")
		if global.p_clan == "Builder":
			theme_ = load("res://ui/builders_maker_theme.tres")
		if global.p_clan == "Thinker":
			theme_ = load("res://ui/thinkers_maker_theme.tres")
		get_node("topic_end_panel").set_theme(theme_)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/ended_text").set_theme(theme_)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_yes").set_theme(theme_)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_no").set_theme(theme_)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_complicated").set_theme(theme_)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/expla_title").set_theme(theme_)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/explanations").set_theme(theme_)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/send").set_theme(theme_)


func _process(delta) -> void:
	if get_node("topic_end_panel/MarginContainer/VBoxContainer/check_yes").get_draw_mode() == 1 :
		answer = "yes"
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_no").set_pressed(0)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_complicated").set_pressed(0)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/send").set_disabled(false)
	elif get_node("topic_end_panel/MarginContainer/VBoxContainer/check_no").get_draw_mode() == 1 :
		answer = "no"
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_yes").set_pressed(0)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_complicated").set_pressed(0)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/send").set_disabled(false)
	elif get_node("topic_end_panel/MarginContainer/VBoxContainer/check_complicated").get_draw_mode() == 1 :
		answer = "complicated"
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_yes").set_pressed(0)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/check_no").set_pressed(0)
		get_node("topic_end_panel/MarginContainer/VBoxContainer/send").set_disabled(false)
	else:
		get_node("topic_end_panel/MarginContainer/VBoxContainer/send").set_disabled(true)
	get_node("topic_end_panel/MarginContainer/VBoxContainer/explanations").visible = true


func _is_pressed() -> void:
	if get_node("topic_end_panel/MarginContainer/VBoxContainer/explanations").get_text() != "" :
		var text = get_node("topic_end_panel/MarginContainer/VBoxContainer/explanations").get_text()
		data_ans = [answer, text]
	else :
		data_ans = [answer]
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz")._sending_answer_data(data_ans, global.player_group) # No server access here so we activate a function in player1
	get_node(".").queue_free()


func _exit_tree() -> void:
	get_node("/root/connection_back/main_menu/loading/gameControl/dials/makerz/MarginContainer/display_makerz/MarginContainer/display").visible = true
