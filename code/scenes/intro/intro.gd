extends Node

const offline_choice_scene = "res://scenes/interfaces/offline_choice_scene.tscn"
onready var text_anim = get_node("gameControl/MarginContainer/Label/TextAnimationPlayer")

func _ready() -> void:
	get_node("bgControl")._window_resize()
	get_node("gameControl/MarginContainer/Label").set_text(tr("intro"))

	if global.current_scene == "main_menu":
		get_node("gameControl/MarginContainer2/skip").set_text(tr("Back To Menu"))
	else:
		get_node("gameControl/MarginContainer2/skip").set_text(tr("Skip Intro"))


func _on_replay_pressed() -> void:
	text_anim.stop()
	text_anim.seek(2)
	text_anim.play("introtext")


func _on_skip_pressed() -> void:
	if global.current_scene == "main_menu":
		queue_free()
	else:
		var options_file = File.new() 
		options_file.open("user://data/options.dat", File.READ)
		var current_line = parse_json(options_file.get_as_text())

		if current_line.first_connection == true:
			current_line.erase("first_connection")
			current_line["first_connection"] = false
		else: pass

		### Writing new datas
		options_file.open("user://data/options.dat", File.WRITE)
		options_file.store_line(to_json(current_line))
		options_file.close()

		get_tree().change_scene("res://scenes/interfaces/connection_screen.tscn")


func _on_TextAnimationPlayer_animation_finished(introtext) -> void:
	var options_file = File.new() 
	options_file.open("user://data/options.dat", File.READ)
	var current_line = parse_json(options_file.get_as_text())

	if current_line.first_connection == true:
		current_line.erase("first_connection")
		current_line["first_connection"] = false

	### Writing new datas
	options_file.open("user://data/options.dat", File.WRITE)
	options_file.store_line(to_json(current_line))
	options_file.close()

	if global.current_scene == "main_menu":
		queue_free()
	else:
		get_tree().change_scene(offline_choice_scene)
