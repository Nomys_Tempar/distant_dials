extends AudioStreamPlayer

var sound_dict: Dictionary

func _ready() -> void:
	### Opening the json
	var sound_dict_to_load = File.new()
	sound_dict_to_load.open("res://json/sfx.json", sound_dict_to_load.READ)
	var sound_dict_load = sound_dict_to_load.get_as_text()
	sound_dict = parse_json(sound_dict_load)
	sound_dict_to_load.close()

	if get_node("..").get_name() == "intro":
		get_node("../gameControl/MarginContainer2/skip").connect("pressed", self, "on_pressed")
		get_node("../gameControl/MarginContainer3/replay").connect("pressed", self, "on_pressed")


func _scene_change() -> void:
	
	if global.offline_mode == false:
		if get_tree().get_network_unique_id() == 1 :
			pass
		### Buttons connection in relation to global.current_scene
		else:
			_set_buttons()
	else:
		_set_buttons()


func _set_buttons():
	if get_node(".").get_name() == "button_back":
		pass
	else:
		if global.current_scene == "connection" :
				get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/admin_creation").connect("pressed", self, "on_pressed")
				get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/connection").connect("pressed", self, "on_pressed")
				get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/settings").connect("pressed", self, "on_pressed")
				get_node("/root/connection_back/gameControl/MarginContainer/VBoxContainer/quit").connect("pressed", self, "on_pressed")
		elif global.current_scene == "main_menu" :
			get_node("../gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/dials").connect("pressed", self, "on_pressed")
			get_node("../gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/admin").connect("pressed", self, "on_pressed")
			get_node("../gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer2/credits").connect("pressed", self, "on_pressed")
			get_node("../gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer/logs").connect("pressed", self, "on_pressed")
			get_node("../gameControl/MarginContainer/VBoxContainer2/settings").connect("pressed", self, "on_pressed")
			get_node("../gameControl/MarginContainer/VBoxContainer2/disconnect").connect("pressed", self, "on_pressed")
			get_node("../gameControl/MarginContainer/VBoxContainer2/quit").connect("pressed", self, "on_pressed")
			if global.level == 4 :
				get_node("../gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/makerz_charts").connect("pressed", self, "on_pressed")
				get_node("../gameControl/MarginContainer/VBoxContainer/HBoxContainer/VBoxContainer3/maker_section").connect("pressed", self, "on_pressed")
		elif global.current_scene == "player1" :
			get_node("../HBoxContainer/VBoxContainer/make_maker").connect("button_down", self, "on_pressed")
			get_node("../MarginContainer2/VBoxContainer/player_text/valid_player_entry").connect("pressed", self, "on_pressed")
		elif global.current_scene == "makerz" :
			get_node("../HBoxContainer/VBoxContainer/quit").connect("pressed", self, "on_pressed")
		elif global.current_scene == "logs" :
			get_node("../Panel/MarginContainer/HBoxContainer/VBoxContainer/history_logs").connect("pressed", self, "on_pressed")
			get_node("../Panel/MarginContainer/HBoxContainer/VBoxContainer/dials_logs").connect("pressed", self, "on_pressed")
			get_node("../Panel/MarginContainer/HBoxContainer/VBoxContainer/maker_logs").connect("pressed", self, "on_pressed")
		elif global.current_scene == "makerz_charts" :
			get_node("../Panel/MarginContainer/HBoxContainer/VBoxContainer/quest_list").connect("item_selected", self, "on_select")
			get_node("../Panel/MarginContainer/MarginContainer/group_size").connect("item_selected", self, "on_select")


### Pressing buttons getting sounds
func on_pressed() -> void:
	AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(1)
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	if global.p_clan == "Voider" :
		if get_node("..").get_name() == "intro":
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			get_node(".").stream = load(sound_dict.voiders_buttons_quit[0])
		elif get_node(".").get_name() == "button_back": # If the player is quiting something
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			get_node(".").stream = load(sound_dict.voiders_buttons_quit[0])
		elif global.current_scene == "logs" and get_node(".").get_name() != "button_back" :
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.1))
			get_node(".").stream = load(sound_dict.voiders_logs[0])
		else : # Other buttons
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -20)
			AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
			get_node(".").stream = load(sound_dict.voiders_buttons_main[0])

	elif global.p_clan == "Builder" :
		if get_node("..").get_name() == "intro":
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			get_node(".").stream = load(sound_dict.voiders_buttons_quit[0])
		elif get_node(".").get_name() == "button_back": # If the player is quiting something
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			get_node(".").stream = load(sound_dict.builders_buttons_quit[0])
		elif get_node(".").get_name() != "button_back" and global.current_scene == "logs" :
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
			get_node(".").stream = load(sound_dict.builders_logs[0])
		else : # Other buttons
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -20)
			AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
			get_node(".").stream = load(sound_dict.builders_buttons_main[0])

	elif global.p_clan == "Thinker" :
		if get_node("..").get_name() == "intro":
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			get_node(".").stream = load(sound_dict.voiders_buttons_quit[0])
		elif get_node(".").get_name() == "button_back": # If the player is quiting something
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			get_node(".").stream = load(sound_dict.thinkers_buttons_quit[0])
		elif get_node(".").get_name() != "button_back" and global.current_scene == "logs" :
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -30)
			AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
			get_node(".").stream = load(sound_dict.thinkers_logs[0])
		else : # Other buttons
			AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -20)
			AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
			get_node(".").stream = load(sound_dict.thinkers_buttons_main[0])
	get_node(".").play()


func on_select(item) -> void: # For charts screen 
	AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(1)
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	if global.p_clan == "Voider" :
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -20)
		AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
		get_node(".").stream = load(sound_dict.voiders_buttons_main[0])

	elif global.p_clan == "Builder" :
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -20)
		AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
		get_node(".").stream = load(sound_dict.builders_buttons_main[0])

	elif global.p_clan == "Thinker" :
		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("button_bus"), -20)
		AudioServer.get_bus_effect(AudioServer.get_bus_index("button_bus"),0).set_pitch_scale(rng.randf_range (0.7, 1.3))
		get_node(".").stream = load(sound_dict.thinkers_buttons_main[0])
	get_node(".").play()
