extends AudioStreamPlayer

var text: String
var sound_dict: Dictionary

func _ready() -> void:
	if get_tree().is_network_server() : pass # The server bypass sounds
	else:
		var sound_dict_to_load = File.new()
		sound_dict_to_load.open("res://json/sfx.json", sound_dict_to_load.READ)
		var sound_dict_load = sound_dict_to_load.get_as_text()
		sound_dict = parse_json(sound_dict_load)
		sound_dict_to_load.close()

		AudioServer.set_bus_volume_db(AudioServer.get_bus_index("key_bus"), -20)


func _key_change() -> void:
	if global.current_scene == "connection":
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_name").connect("text_changed", get_node("."), "_key_pressed")
		get_node("/root/connection_back/gameControl/MarginContainer/HBoxContainer/VBoxContainer/HBoxContainer/VBoxContainer/player_pass").connect("text_changed", get_node("."), "_key_pressed")
	elif global.current_scene == "player1":
		get_node("../entry_text").connect("text_changed", get_node("."), "_key_pressed")
	elif global.current_scene == "makerz":
		get_node("../maker_entry").connect("text_changed", get_node("."), "_key_pressed")


func _key_pressed(text: String) -> void:
	if global.current_scene == "player1" && get_node("../entry_text").get_text() == "": pass
	else:
		var rng = RandomNumberGenerator.new()
		rng.randomize()
		if global.p_clan == "Voider" :
			get_node(".").stream = load(sound_dict.voiders_keyboard[rng.randi() % sound_dict.voiders_keyboard.size()])
		if global.p_clan == "Builder" :
			get_node(".").stream = load(sound_dict.builders_keyboard[rng.randi() % sound_dict.builders_keyboard.size()])
		if global.p_clan == "Thinker" :
			get_node(".").stream = load(sound_dict.thinkers_keyboard[rng.randi() % sound_dict.thinkers_keyboard.size()])
		get_node(".").play()
